<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerProfile extends Model
{
    protected $fillable=['last_name','first_name','phone_number','mailing_address','user_id'];

}
