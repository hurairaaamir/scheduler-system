<?php

namespace App\Http\Controllers\API;

use App\Mail\PhoneVerification;
use App\Mail\RequestPassword;
use Illuminate\Http\Request;
use App\Http\Controllers\API\ResponseController as ResponseController;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class AuthController extends ResponseController
{
    //create user
    public function signup(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|',
            'email' => 'required|string|email|unique:users',
            'password' => 'required',
        ]);

        if($validator->fails()){
            return $this->sendError($validator->errors());
        }

        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);
        if($user){
            $success['user'] = $user;
            $success['token'] =  $user->createToken('token')->accessToken;
            $success['message'] = "Registration successfull..";

            return $this->sendResponse($success);
        }
        else{
            $error = "Sorry! Registration is not successfull.";
            return $this->sendError($error, 401);
        }

    }

    //login
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email',
            'password' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError($validator->errors());
        }

        $credentials = request(['email', 'password']);
        if(!Auth::attempt($credentials)){
            $error = "Unauthorized";
            return $this->sendError($error, 401);
        }
        $user = $request->user();
        $success['token'] =  $user->createToken('token')->accessToken;
        return $this->sendResponse($success);
    }

    //logout
    public function logout(Request $request)
    {

        $isUser = $request->user()->token()->revoke();
        if($isUser){
            $success['message'] = "Successfully logged out.";
            return $this->sendResponse($success);
        }
        else{
            $error = "Something went wrong.";
            return $this->sendResponse($error);
        }


    }

    //getuser
    public function getUser(Request $request)
    {
        //$id = $request->user()->id;
        $user = $request->user();
        if($user){
            return $this->sendResponse($user);
        }
        else{
            $error = "user not found";
            return $this->sendResponse($error);
        }
    }

    public function requestPassword(Request $request){
        $user = User::where('email' , $request->email)->first();
        if ($user){
            $user->update([
                'password_reset_token' => sha1(time())
            ]);

            Mail::to($user->email)->send(new RequestPassword($user));
            return response()->json(['status' => true , 'message' => 'Email Sent'] , 200);
        }else{
            return response()->json(['status' => false , 'message' => 'Email Not Found'] , 200);
        }
    }

    public function resetPassword(Request $request){
        $user = User::where('password_reset_token' , $request->token)->first();
        if ($user){
            $user->update([
                'password' => bcrypt($request->password)
            ]);
            return response()->json(['status' => true , 'message' => 'Password Reset Successfully'] , 200);
        }
        else{
            return response()->json(['status' => false , 'message' => 'User Not Found'] , 200);
        }
    }

    public function registerUser(Request $request){


        $validator = Validator::make($request->all(), [
            'phone_number' => 'required|string|',
        ]);

        if($validator->fails()){
            return $this->sendError($validator->errors());
        }


    }

    public function verifyCode(Request $request){
        $user = User::where('phone_verification_code' , $request->code)->first();

        if ($user){
            return response()->json([
                'status' => true,
                'user' => $user,
                'token' => $user->createToken('token')->accessToken
            ]);

        } else{
            return response()->json([
                'status' => false,
                'message' => 'Invalid Code'
            ]);
        }
    }
}