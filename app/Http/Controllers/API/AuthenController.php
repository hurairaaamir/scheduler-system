<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\Hash;
use GuzzleHttp\Client;
use App\Events\MailEvent;
use App\User;
use App\CustomerProfile;


class AuthenController extends Controller
{
    public function login(Request $request)
    {
        $request->validate([
            "email"=>"required",
            "password"=>"required"
        ]);

        $http = new \GuzzleHttp\Client;


        try {
            $response = $http->post($this->getUrl().'/oauth/token', [
                'form_params' => [
                    'grant_type' => 'password',
                    'client_id' => '2',
                    'client_secret' => config('services.passport.key'),
                    'username' => $request->email,
                    'password' => $request->password,
                ]
            ]);
            return $response->getBody();

        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            if ($e->getCode() === 400) {
                return response()->json('Invalid Request. Please enter a username or a password.', $e->getCode());
            } else if ($e->getCode() === 401) {
                return response()->json('Your credentials are incorrect. Please try again', $e->getCode());
            }

            return response()->json('Something went wrong on the server.', $e->getCode());
        }
    }

    public function register(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);

        $user=User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'role'=> 'driver'
        ]);

        return response()->json([
            "data"=>$user
        ]);
    }

    public function logout()
    {
        auth()->guard('api')->user()->tokens->each(function ($token, $key) {
            $token->delete();
        });

        return response()->json('Logged out successfully', 200);
    }

    private function getUrl(){
        return url("/")=="http://127.0.0.1:8000"? "http://127.0.0.1:8001" : url("/");
    }

    private function random($size)
    {
        $permitted_chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        return substr(str_shuffle($permitted_chars), 0, $size);
    }

    public function forgetPassword(Request $request){
        
        $request->validate([
            "email"=>"required|email"
        ]); 

        $user=User::where('email',$request->email)->first();
        

        if($user){
            $token=($user->token)?$user->token:$this->random(20);
            
            $url=$this->getUrl()."/#/reset-password/".$user->email."/".$token;

            $user->update([
                "token"=>$token
            ]);
            // event(new MailEvent($user->email,$url));

            return response()->json([
                "data"=>"email is sent"
            ]);

        }else{
            return response()->json([
                "errors"=>["onEmail"=>"Email not found"]
            ],405);
        }
                
    }

    public function authUser(Request $request){
        $user=auth()->user();
        $profile=null;
        // $role=[];
        
        if($user->role=='customer'){
            $user->customer_profile;        
        }
        
        return response()->json([
            "data"=>$user,
        ]);
    }

    public function storeCustomerProfile(Request $request){
        $request->validate([
            "last_name"=>"required|min:3",
            "first_name"=>"required|min:3",
            "phone_number"=>"required|min:8",
            "user_id"=>"required"
        ]);
        $profile=CustomerProfile::where('user_id',$request->user_id)->first();
        if($profile){
            $profile->update([
                "last_name"=>$request->last_name,
                "first_name"=>$request->first_name,
                "phone_number"=>$request->phone_number,
                "mailing_address"=>$request->mailing_address,
            ]);
        }else{
            $profile=CustomerProfile::create([
                "last_name"=>$request->last_name,
                "first_name"=>$request->first_name,
                "phone_number"=>$request->phone_number,
                "mailing_address"=>$request->mailing_address,
                "user_id"=>$request->user_id
            ]);
        }

        return response()->json([
            "data"=>$profile
        ]);
    }

    public function updateProfile(Request $request , $id){
        $request->validate([
            "last_name"=>"required|min:3",
            "first_name"=>"required|min:3",
            "phone_number"=>"required|min:8",
            "mailing_address"=>"required"
        ]);
        $profile=CustomerProfile::findOrFail($id);
        $profile->update([
            "last_name"=>$request->last_name,
            "first_name"=>$request->first_name,
            "phone_number"=>$request->phone_number,
            "mailing_address"=>$request->mailing_address,
        ]);
        return response()->json([
            "data"=>$profile
        ]);
    }

    public function updateUser(Request $request , $id){
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
            'password' => 'required|string|min:6',
        ]);
        $user=User::findOrFail($id);
        if(Hash::check($request->old_password,$user->password)){
            $user->update([
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password),
            ]);

            return response()->json([
                "data"=>$user
            ]);
        }else{
            return response()->json([
                "errors"=>["old_password"=>["old password is incorrect"]]
            ],405);
        }

    }

}
