<?php

namespace App\Http\Controllers\API;

use App\Booking;
use App\Mail\PhoneVerification;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class BookingController extends Controller
{
    public function reserve_booking(Request $request){
        $digits = 4;
        $rand_number = rand(pow(10, $digits-1), pow(10, $digits)-1);
        $user = User::where('phone_number' , $request->phone_number)->orWhere('email' , $request->email)->first();

        if ($user){


            $user->phone_verification_code = $rand_number;
            $user->save();

            Mail::to($request->email)->send(new PhoneVerification($user));

        }
        else{

            $user = new User();
            $user->name = $request->name;
            $user->email = $request->email;
            $user->phone_number = $request->phone_number;
            $user->phone_verification_code = $rand_number;
            $user->save();

            Mail::to($request->email)->send(new PhoneVerification($user));

        }


        $booking = new Booking();
        $booking->user_id = $user->id;
        $booking->package_id = $request->packageId;
        $booking->category_id = $request->categoryId;
        $booking->origin_lat = $request->orgin['lat'];
        $booking->origin_lang = $request->orgin['lng'];
        $booking->destination_lat = $request->destination['lat'];
        $booking->destination_lang = $request->destination['lng'];
        $booking->delivery_days = $request->deliveryDays;
        $booking->pickup_days = $request->pickupDays;
        $booking->time = $request->time;
        $booking->date = $request->date;
        $booking->price = $request->price;
        $booking->description = $request->description;
        $booking->save();

        return response()->json([
           'status' => true,
            'message' => 'Booking Added Successfully'
        ]);
    }
}
