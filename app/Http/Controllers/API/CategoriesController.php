<?php

namespace App\Http\Controllers\API;

use App\Categories;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoriesController extends Controller
{
    public function get_categories(){
        $categories = Categories::all();

        return response()->json([
            'data' => $categories,
            'message' => 'All Categories'
        ]);

    }
}
