<?php

namespace App\Http\Controllers\API;

use App\Item;
use App\Package;
use App\Rule;
use App\Zone;
use App\Categories;
use Illuminate\Http\Request;
use App\Http\Resources\PackageResource;
use App\Http\Resources\RuleResource;
use App\Http\Controllers\Controller;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class PackageController extends Controller
{

    public function get_packages(Request $request){
        $package = Package::where('category_id' , $request->category_id)->with(['package_items'])->get();

        foreach ($package as $value){
            $value->charges = ($value->per_mile_price * $request->distance) + $value->distance_charge;
            foreach ($value->package_items as $val){
                $val->name = Item::find($val->item_id)->name;
            }

        }


        return response()->json([
            'status' => true,
            'message' => 'Package By Category',
            'data' => $package
        ]);
    }

    public function get_package_by_id(Request $request){
        $package = Package::where('id' , $request->id)->with(['package_items'])->first();
        // $package->cat=Categories::findOrFail($package->category_id);
        $package->charges = ($package->per_mile_price * $request->distance) + $package->distance_charge;
        foreach ($package->package_items as $val){
            $val->name = Item::find($val->item_id)->name;
        }

        return response()->json([
            'status' => true,
            'message' => 'Package By Id',
            'data' => $package
        ]);
    }



    private function getOfficeDistance($pickup,$lat,$lng){
        $http = new \GuzzleHttp\Client;
        try {
            $response = $http->get('https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins='.$lat.','.$lng.'&destinations='.$pickup->lat.','.$pickup->lng.'&key=AIzaSyAhN31rb1mb4ejPcZiAqqjtrjOly3l960g');
            $data=json_decode($response->getBody());

            return $data;

        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            return $e;
        }
    }



    private function selectOffice($zone,$pickup,$dropoff){
        $officeLocations=$zone->office_location;
        $officeDistance="";
        $selectedOffice="";
        $returnDistance="";
        $officeDuration="";

        foreach($officeLocations as $office){
            $data=$this->getOfficeDistance($pickup,$office->lat,$office->lng);
            $tempDistance=$data->rows[0]->elements[0]->distance->value;

            if(!$officeDistance){
                $officeDistance=$tempDistance;
                $officeDuration=$data->rows[0]->elements[0]->duration->value;
                $selectedOffice=$office;
            }else if($tempDistance<$officeDistance){
                $officeDistance=$tempDistance;
                $officeDuration=$data->rows[0]->elements[0]->duration->value;
                $selectedOffice=$office;
            }
        }

        if($selectedOffice){
            $returnDistance=$this->getOfficeDistance($dropoff,$selectedOffice->lat,$selectedOffice->lng)->rows[0]->elements[0]->distance->value;
        }

        return[
            "officeDistance"=>$officeDistance,
            "returnDistance"=>$returnDistance,
            "selectedOffice"=>$selectedOffice,
            "officeDuration"=>$officeDuration
        ];


    }

    public function fetchPackages(Request $request){

        $pickup=json_decode($request->pickup);
        $dropoff=json_decode($request->dropoff);

        $zone=Zone::findOrFail($request->pickup_zone_id);
        $officeData=$this->selectOffice($zone,$pickup,$dropoff);
        // dd($officeData["officeDistance"]);
        $data=$this->getTimeAndDistance($pickup,$dropoff);

        // lat,lng,address,zipcode,
        $distance=$data->rows[0]->elements[0]->distance;
        $duration=$data->rows[0]->elements[0]->duration;


        $packages= new PackageResource($zone->packages->where('category_id',$request->category_id),$officeData,$distance,$duration);

        return response()->json([
            "data"=>$packages
        ]);
    }

    public function fetchRules($package_id,$type){
        $rules=RuleResource::collection(Package::findOrFail($package_id)->rules->where('rule_type',$type));
        $dayz=[];
        foreach($rules as $rule){
            $days=$rule->days;
            $days=explode(',',$days);
            foreach($days as $day){
                array_push($dayz,$day);
            }
        }

        return response()->json([
            "data"=>$dayz
        ]);
    }

    public function getTimeAndDistance($pickup,$dropoff){
        $http = new \GuzzleHttp\Client;
        try {
            $response = $http->get('https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins='.$pickup->lat.','.$pickup->lng.'&destinations='.$dropoff->lat.','.$dropoff->lng.'&key=AIzaSyAhN31rb1mb4ejPcZiAqqjtrjOly3l960g');
            $data=json_decode($response->getBody());
            return $data;

        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            return $e;
        }
    }

    public function show($id){
        $package=Package::findOrFail($id);
        return response()->json([
            "data"=>$package
        ]);
    }
}
