<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Reservation;
use App\UnitTruck;
use App\Schedule;
use App\User;
use App\Categories;
use App\Package;
use App\Payment;
use App\UnitTimeBlock;
use App\ReservationScheduleStatus;
use App\ReservationSchedule;
use Stripe\Stripe;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\DB;
use App\Http\Resources\ReservationScheduleCollection;
use App\Http\Resources\UnitTruckCollection;
use App\Http\Resources\ScheduleCollection;


class ReservationController extends Controller
{

    protected $lastReservation=NULL;

    public function index(Request $request){
        $datas=auth()->guard('api')->user()->reservation_schedules->pluck(['id']);
        $ids=[];
        foreach($datas as $data){
            if(!in_array($data,$ids)){
                array_push($ids , $data);
            }
        }
        $datas=ReservationSchedule::select('date',DB::raw('count(*) as total'))->whereIn('id',$ids)->groupBy('date')->get();

        return response()->json([
            "data"=>$datas
        ]);        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // ->orderBy('campaigns.created_at','desc')

    public function getReservation(Request $request){

        if(auth()->guard('api')->user()->role=='driver'){
            $datas=auth()->guard('api')->user()->reservation_schedules->where('date',$request->date);

            $ids=[];
            $unit_id="";
            foreach($datas as $data){
                $unit_id=$data->unit_truck_id;
                    array_push($ids , $data->id);
            }        
                
            $this->filterSchedules($unit_id,$request->date);


            $reservations= ReservationScheduleCollection::collection(ReservationSchedule::whereIn('id',$ids)->orderBy('ordering','asc')->get());
            
            


            return response()->json([ "data"=>$reservations]);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    private function filterSchedules($unit_id,$date){
        
        
        $unitTimeBlock=UnitTimeBlock::where('unit_truck_id',$unit_id)->where('date',$date)->first();
        $last_id=NULL;
        $currentReservation=NULL;

        
        if((!$unitTimeBlock->_6am_9am)&&$unitTimeBlock->_6am_9am_schedule_id){
            $id=$unitTimeBlock->_6am_9am_schedule_id;
            $last_id=$this->updateSchedule($last_id,$id);
        }
        if((!$unitTimeBlock->_9am_12pm)&&$unitTimeBlock->_9am_12pm_schedule_id){
            $id=$unitTimeBlock->_9am_12pm_schedule_id;
            $last_id=$this->updateSchedule($last_id,$id);
        } 
        if((!$unitTimeBlock->_12pm_3pm)&&$unitTimeBlock->_12pm_3pm_schedule_id){
            $id=$unitTimeBlock->_12pm_3pm_schedule_id;
            $last_id=$this->updateSchedule($last_id,$id);
        }
        if((!$unitTimeBlock->_3pm_6pm)&&$unitTimeBlock->_3pm_6pm_schedule_id){
            $id=$unitTimeBlock->_3pm_6pm_schedule_id;
            $last_id=$this->updateSchedule($last_id,$id);
        }            
        if((!$unitTimeBlock->_6pm_9pm)&&$unitTimeBlock->_6pm_9pm_schedule_id){
            $id=$unitTimeBlock->_6pm_9pm_schedule_id;
            $last_id=$this->updateSchedule($last_id,$id);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    private function updateSchedule($last_id,$id){
        $reservation=ReservationSchedule::findOrFail($id);
        
        if($last_id){
            if(($last_id!=$reservation->id)){
                $this->lastReservation=$this->updateOne($this->lastReservation,$reservation->id);
                return $this->lastReservation->id;
            }
        }else{
            if($reservation->type=='pickup'){
                $dropoffReservation=ReservationSchedule::where('reservation_id',$reservation->reservation_id)->where('type','dropoff')->first();
                $this->lastReservation=$dropoffReservation;
                return $reservation->id;
            }else{
                $this->lastReservation=$reservation;
                return $reservation->id;
            }            
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    private function timeToMin($time){
        if(strpos($time,'Hr')){
            $timeExplode=explode('Hr',$time);
            $travel_time=(int)trim($timeExplode[0])*60 + (int)trim(explode('Min',$timeExplode[1])[0]);
            return $travel_time;
        }else{
            return (int)trim(explode('Min',$time)[0]);
        }
    }
    
    private function updateNow($currentReservation,$lastReservation){

        $travel_time_min=(int)(round(((float)$this->getTravelTime($currentReservation,$lastReservation))/60));
        if($travel_time_min>60){
            $travel_time=intdiv($travel_time_min, 60).' Hr'.' '. ($travel_time_min % 60)." Min";
        }else{
            $travel_time=$travel_time_min." Min";
        }
        $temp=strtotime("+".$travel_time_min." minutes", strtotime($lastReservation->est_time_complete));
        $est_time_arrival=date('h:i A',$temp);

        // service time
        $service_time_min=$this->timeToMin($currentReservation->service_time);
        $temp=(strtotime("+".$service_time_min." minutes", strtotime($est_time_arrival)));
        $est_time_complete=date('h:i A', $temp);

        $currentReservation=ReservationSchedule::findOrFail($currentReservation->id);
        $currentReservation->update([
            "travel_time"=>$travel_time,
            "est_time_arrival"=>$est_time_arrival,
            "est_time_complete"=>$est_time_complete
        ]);
        
        return $currentReservation;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    private function updateOne($lastReservation,$current_id){
        $currentReservation=ReservationSchedule::findOrFail($current_id);
        
        if($currentReservation->type=='pickup'){
            
            $currentReservation=$this->updateNow($currentReservation,$lastReservation);   
            $dropoffReservation=ReservationSchedule::where('reservation_id',$currentReservation->reservation_id)->where('type','dropoff')->first();
            $dropoffReservation=$this->updateNow($dropoffReservation,$currentReservation);

            // Max Leave Time
            $post_time=explode('-',$dropoffReservation->time_block)[1];
            $duration=$this->timeToMin($dropoffReservation->travel_time);
            $endTime = strtotime("-".$duration." minutes", strtotime($post_time));
            $max_leave=date('h:i A', $endTime);
    

            $this->lastReservation->update([
                "max_leave_time"=>$max_leave
            ]);

            return $dropoffReservation;
        }else{
            $currentReservation=$this->updateNow($currentReservation,$lastReservation);
            // Max Leave Time
            $post_time=explode('-',$currentReservation->time_block)[1];
            $duration=$this->timeToMin($currentReservation->travel_time);
            $endTime = strtotime("-".$duration." minutes", strtotime($post_time));
            $max_leave=date('h:i A', $endTime);
    
            $this->lastReservation->update([
                "max_leave_time"=>$max_leave
            ]);

            return $currentReservation;
        }
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function customerReservations(Request $request){
        $reservation=Reservation::select('packages.name','reservations.id','reservations.date','reservations.status')->join('packages','packages.id','reservations.package_id')->where('reservations.user_id',auth()->guard('api')->user()->id)->get();

        return response()->json(["data"=>$reservation]);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function customerReservation(Request $request){
        $reservation=Reservation::findOrFail($request->reservation_id);
        $pickup_schedule=$reservation->reservation_schedule->where('type','pickup')->first();
        $dropoff_schedule=$reservation->reservation_schedule->where('type','dropoff')->first();
        $delivery_schedule=$reservation->reservation_schedule->where('type','delivery')->first();

        return response()->json([
            "data"=>[
                "reservation"=>$reservation,
                "pickup_schedule"=>$pickup_schedule,
                "delivery_schedule"=>$delivery_schedule,
                "dropoff_schedule"=>$dropoff_schedule
            ]
        ]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getTimeAndDistance(Request $request){
        $http = new \GuzzleHttp\Client;
        

        try {
            $response = $http->get('https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins='
            .$request->pickup['address'].'&destinations='
            .$request->dropoff["address"].'&key=AIzaSyAhN31rb1mb4ejPcZiAqqjtrjOly3l960g');    
            $data=json_decode($response->getBody());
            return response()->json([
                "data"=>$data
            ]);

        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            return response()->json([
                "data"=>$e
            ]);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    private function getTravelTime($currentReservation,$lastReservation){
        $http = new \GuzzleHttp\Client;
        try {
            $response = $http->get('https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins='.$lastReservation->address_lat.','.$lastReservation->address_lng.'&destinations='.$currentReservation->address_lat.','.$currentReservation->address_lng.'&key=AIzaSyAhN31rb1mb4ejPcZiAqqjtrjOly3l960g');
            $data=json_decode($response->getBody());

            return $data->rows[0]->elements[0]->duration->value;

        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            return $e;
        }
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCategories(){
        $categories=Categories::all();
        return response()->json(["data"=>$categories]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function storeReservation(Request $request){
        
        $package=Package::findOrFail($request->package_id);
        $user=User::findOrFail($request->user_id);
        

        $stripe=new \Stripe\StripeClient('sk_test_51HOjzsC1zYIo0nPVtGcQectayrFTWlHpuLrEq2a9k556QtlIYMOJksjiPXZDr5lDFFbezTywBwWobnXakLRvcw5a00wkFxG1U5');
        $pay=(int)((float)$package->deposit*(float)$request->charge);

        $charge=$stripe->charges->create([
            'amount'               => $pay,
            'currency'             => 'USD',
            'source'=>$request->token,
            "description"=>"this is test description",
            'receipt_email'=>$user->email,
        ]);

        $stripe_id=$charge->id;
        $pickup_zone_id=$request->pickup_zone_id;
        $dropoff_zone_id=$request->dropoff_zone_id;
        $note=$request->note;
        $time_blocks=['6am-9am','9am-12pm',"12pm-3pm","3pm-6pm","6pm-9pm"];
        $travel_data=json_decode($request->travel_data);
        $dropoff=json_decode($request->dropoff);
        $pickup=json_decode($request->pickup);
        $schedule_data=json_decode($request->schedule_data);
        $bulky_items_list=$request->bulky_items;
        if($request->type=='delivery'){
            $time_data=$this->getDeliveryTimeDate($schedule_data,$travel_data,$package);
        }else{
            $time_data=$this->getTimeData($schedule_data,$travel_data,$package);
        }
            

        $reservation=Reservation::create([
            "date"=>str_replace('-','/',$schedule_data->date),
            'bulky_items'=>$package->moving_item,
            "quote_amount"=>$travel_data->charge,
            "deposit_amount"=>$package->deposit,
            "note"=>$note,
            "package_id"=>$request->package_id,
            "user_id"=>$user->id,
            "office_id"=>$travel_data->office_id,
            "bulky_items_list"=>serialize($bulky_items_list),
            "status"=>"active"
        ]);
        
        $block_no=$time_data["block_no"];

        // DELIVERY
        $reservationScheduleDelivery=ReservationSchedule::create([
            "address"=>$pickup->address,
            "address_lat"=>$pickup->lat,
            "address_lng"=>$pickup->lng,
            "block_no"=>$block_no,
            "time_block"=>$schedule_data->time_block,
            "distance"=>$time_data["distance"],
            "travel_time"=>$time_data['travel_time'],
            "est_time_arrival"=>$time_data['est_time_arrival'],
            "service_time"=>$time_data['service_time'],
            "est_time_complete"=>$time_data['est_time_complete'],
            "type"=>"delivery",
            "active"=>"true",
            "ordering"=>(float)array_search($schedule_data->time_block,$time_blocks)+1,
            "status"=>"scheduled",
            "zone_id"=>$pickup_zone_id,
            "date"=>str_replace('-','/',$schedule_data->date),
            "unit_truck_id"=>$schedule_data->unit_id,
            "reservation_id"=>$reservation->id
        ]);

        $this->attachDriver(str_replace('-','/',$schedule_data->date),$schedule_data->unit_id,$reservationScheduleDelivery);


        // PICK UP
        $reservationSchedulePickup=ReservationSchedule::create([
            "address"=>$pickup->address,
            "address_lat"=>$pickup->lat,
            "address_lng"=>$pickup->lng,
            "duration"=>$travel_data->duration,
            "distance"=>$travel_data->distance,
            "office_duration"=>$travel_data->office_duration,
            "office_distance"=>$travel_data->office_distance,
            "type"=>"pickup",
            "status"=>"scheduled",
            "zone_id"=>$pickup_zone_id,
            "reservation_id"=>$reservation->id
        ]);

        // DROPOFF
        $reservationSchedule=ReservationSchedule::create([
            "address"=>$dropoff->address,
            "address_lat"=>$dropoff->lat,
            "address_lng"=>$dropoff->lng,
            "distance"=>$travel_data->distance,
            "duration"=>$travel_data->duration,
            "office_duration"=>$travel_data->office_duration,
            "office_distance"=>$travel_data->office_distance,
            "type"=>"dropoff",
            "status"=>"scheduled",
            "zone_id"=>$dropoff_zone_id,
            "reservation_id"=>$reservation->id
        ]);
        


        Payment::create([
            "amount"=>$reservation->deposit_amount,
             "stripe_id"=>$stripe_id,   
            "reservation_id"=>$reservation->id
        ]);


        $this->disableBlocks($schedule_data->unit_id, $schedule_data->time_block, $block_no, str_replace('-','/',$schedule_data->date), $reservationScheduleDelivery->id);
        


        return response()->json([
            "data"=>$reservation->id
        ]);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private function attachDriver($date,$unit_id,$reservationSchedule)
    {
        $schedule=Schedule::where("unit_truck_id",$unit_id)->where('date',$date)->first();   
        foreach($schedule->users as $user){
            $reservationSchedule->users()->attach([$user->id]);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     private function disableBlocks($unit_id,$time_block,$block_no,$date,$schedule_id){
        $unit=UnitTruck::findOrFail($unit_id);
        $unitTimeBlock=UnitTimeBlock::where('unit_truck_id',$unit_id)->where('date',$date)->first();
        
        
        $time_blocks=['6am-9am','9am-12pm',"12pm-3pm","3pm-6pm","6pm-9pm"];
        $index=array_search($time_block,$time_blocks);

        for($i=0;$i<$block_no;$i++){
            if($index<count($time_blocks)){
                switch($time_blocks[$index]){
                    case "6am-9am":
                        $unitTimeBlock->_6am_9am=FALSE;
                        $unitTimeBlock->_6am_9am_schedule_id=$schedule_id;
                    break;
                    case "9am-12pm":
                        $unitTimeBlock->_9am_12pm=FALSE;
                        $unitTimeBlock->_9am_12pm_schedule_id=$schedule_id;
                    break;
                    case "12pm-3pm":
                        $unitTimeBlock->_12pm_3pm=FALSE;
                        $unitTimeBlock->_12pm_3pm_schedule_id=$schedule_id;
                    break;
                    case "3pm-6pm":
                        $unitTimeBlock->_3pm_6pm=FALSE;
                        $unitTimeBlock->_3pm_6pm_schedule_id=$schedule_id;
                    break;
                    case "6pm-9pm":
                        $unitTimeBlock->_6pm_9pm=FALSE;
                        $unitTimeBlock->_6pm_9pm_schedule_id=$schedule_id;
                    break;
                }
            }
            $index++;
        }
        

        $unitTimeBlock->save();
     }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private function setMaxLeaveTime($schedule_data)
    {
        $schedule=ReservationSchedule::find($schedule_data->pre_schedule_id);

        $post_time=explode('-',$schedule_data->time_block)[1];

        $duration=$schedule_data->new_duration;
        $duration=(int)(((float)($duration))/60);
        $endTime = strtotime("-".$duration." minutes", strtotime($post_time));
        $max_leave=date('h:i A', $endTime);

        $schedule->update([
            "max_leave_time"=>$max_leave
        ]);

        return $schedule->est_time_complete;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private function checkLastReservation($schedule_data)
    {   
        $date=$schedule_data->date;
        $unit_id=$schedule_data->unit_id;

        $schedules=ReservationSchedule::where('date',$date)
                ->where('unit_tuck_id',$unit_id)
                ->where('type','dropoff')->get();

        foreach($schedules as $schedule){
            $time_block=explode('-',$schedule->time_block);
            

        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private function getDeliveryTimeDate($schedule_data,$travel_data,$package){
        // travel time
        $distance=NULL;
        $duration=NULL;
        $last_est_time_complete=NULL;

        $office_travel_time=NULL;
        if($schedule_data->new_distance&&$schedule_data->new_duration){
            $distance=$schedule_data->new_distance;
            $duration=$schedule_data->new_duration;
            $last_est_time_complete=$this->setMaxLeaveTime($schedule_data);
        }else{
            $office_travel_time=TRUE;
            $duration=$travel_data->office_duration;
            $distance=$travel_data->office_distance;
        }
               
        $travel_time_min=(int)(round(((float)$duration)/60));
        if($travel_time_min>60){
            $travel_time=intdiv($travel_time_min, 60).' Hr'.' '. ($travel_time_min % 60)." Min";
        }else{
            $travel_time=$travel_time_min." Min";
        }


        // est Time Arrival
        if($last_est_time_complete){
            $time=strtotime("+".$travel_time_min." minutes", strtotime($last_est_time_complete));
        }else{
            $time=strtotime(explode('-',$schedule_data->time_block)[0]);
        }        
        $est_time_arrival=date('h:i A',$time);

        
        // service time
        $service_time_min=15;
        if($service_time_min>60){
            $service_time=intdiv($service_time_min, 60).' Hr'.' '.($service_time_min % 60)." Min";
        }else{
            $service_time=$service_time_min." Min";
        }

        // Est Time Complete
        $temp=(strtotime("+".$service_time_min." minutes", strtotime($est_time_arrival)));
        $est_time_complete=date('h:i A', $temp);

        // BLOCK NO
        $block_no=$this->calculateTimeBlockNumber(explode('-',$schedule_data->time_block)[1],$est_time_complete);


        $delivery_data=[
            "travel_time"=>$travel_time,
            "distance"=>$distance,
            "est_time_arrival"=>$est_time_arrival,
            "service_time"=>$service_time,
            "est_time_complete"=>$est_time_complete,
            "block_no"=>$block_no
        ];

        return $delivery_data;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function update(Request $request){

        $pickup_schedule=ReservationSchedule::findOrFail($request->pickup_schedule_id);
        $dropoff_schedule=ReservationSchedule::findOrFail($request->dropoff_schedule_id);
        $package=Package::findOrFail($request->package_id);
        $unit_id=$request->schedule_data["unit_id"];
        $time_block=$request->time_block;
        $time_data=$this->getTimeData($pickup_schedule,$dropoff_schedule,$package,$time_block);
        $block_no=$time_data['block_no'];
        $time_blocks=['6am-9am','9am-12pm',"12pm-3pm","3pm-6pm","6pm-9pm"];
        // PICK UP
        $pickup_schedule->update([
            "block_no"=>$time_data['block_no'],
            "time_block"=>$time_block,
            "travel_time"=>$time_data["pickup"]['travel_time'],
            "est_time_arrival"=>$time_data["pickup"]['est_time_arrival'],
            "service_time"=>$time_data["pickup"]['service_time'],
            "est_time_complete"=>$time_data["pickup"]['est_time_complete'],
            "active"=>"true",
            "ordering"=>(float)array_search($time_block,$time_blocks)+1-0.1,
            "unit_truck_id"=>$unit_id,
            "date"=>$request->date
        ]);

        $this->attachDriver($request->date,$unit_id,$pickup_schedule);

        // DROPOFF
        $dropoff_schedule->update([
            "block_no"=>$time_data['block_no'],
            "time_block"=>$time_block,
            "travel_time"=>$time_data["dropoff"]['travel_time'],
            "est_time_arrival"=>$time_data["dropoff"]['est_time_arrival'],
            "service_time"=>$time_data["dropoff"]['service_time'],
            "est_time_complete"=>$time_data["dropoff"]['est_time_complete'],
            "active"=>"true",
            "ordering"=>(float)array_search($time_block,$time_blocks)+1 +0.5,
            "unit_truck_id"=>$unit_id,
            "date"=>$request->date
        ]);

        $this->attachDriver($request->date,$unit_id,$dropoff_schedule);


        $this->disableBlocks($unit_id, $time_block, $block_no, $request->date, $pickup_schedule->id);


        return response()->json([
            "data"=>$pickup_schedule->reservation->id
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
    */
    private function getTimeData($pickup_schedule ,$dropoff_schedule, $package, $time_block)
    {
        // travel time
        $distance=NULL;
        $duration=NULL;
        $last_est_time_complete=NULL;

        $office_travel_time=NULL;
        
        $travel_time_min=(int)(round(((float)$pickup_schedule->office_duration)/60));
        if($travel_time_min>60){
            $travel_time=intdiv($travel_time_min, 60).' Hr'.' '. ($travel_time_min % 60)." Min";
        }else{
            $travel_time=$travel_time_min." Min";
        }
        
        $time=strtotime(explode('-',$time_block)[0]);
        $est_time_arrival=date('h:i A',$time);

        
        // service time
        $service_time_min=(int)(((float)$package->service_time)/2);
        if($service_time_min>60){
            $service_time=intdiv($service_time_min, 60).' Hr'.' '.($service_time_min % 60)." Min";
        }else{
            $service_time=$service_time_min." Min";
        }

        // Est Time Complete
        $temp=(strtotime("+".$service_time_min." minutes", strtotime($est_time_arrival)));
        $est_time_complete=date('h:i A', $temp);


        $pickup_data=[
            "travel_time"=>$travel_time,
            "distance"=>$distance,
            "est_time_arrival"=>$est_time_arrival,
            "service_time"=>$service_time,
            "est_time_complete"=>$est_time_complete
        ];



        // Travel Time
        $travel_time=(round(((float)$pickup_schedule->duration)/60));
        $travel_time_min=(int)(round(((float)$pickup_schedule->duration)/60));
        if($travel_time>60){
            $travel_time=intdiv($travel_time, 60).' Hr'.' '.($travel_time % 60)." Min";
        }else{
            $travel_time=$travel_time." Min";
        }

        // est time Arrival
        $temp=strtotime("+".$travel_time_min." minutes", strtotime($pickup_data['est_time_complete']));
        $est_time_arrival=date('h:i A',$temp);
        

        // est Time Complete
        $temp=(strtotime("+".$service_time_min." minutes", strtotime($est_time_arrival)));
        $est_time_complete=date('h:i A', $temp);

        $block_no=$this->calculateTimeBlockNumber(explode('-',$time_block)[1],$est_time_complete);

        $dropoff_data=[
            "travel_time"=>$travel_time,
            "service_time"=>$service_time,
            "est_time_arrival"=>$est_time_arrival,
            "est_time_complete"=>$est_time_complete
        ];
        return [
            "dropoff"=>$dropoff_data,
            "pickup"=>$pickup_data,
            "block_no"=>$block_no
        ];
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    private function calculateTimeBlockNumber($lower_time,$upper_time)
    {
        $time=strtotime($upper_time)-strtotime($lower_time);

        return 1<($time/60)?2:1;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    private function tohour($min)
    {
        return (float($min))/60;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private function formateTime($time){
        $temp=strtotime("-60 minutes", strtotime($time));
        return date('h:i A',$temp);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function changeStatus(Request $request)
    {
        $id=$request->id;
        $schedule=ReservationSchedule::findOrFail($id);
        $schedule->update([
            "status"=>$request->status
        ]);

        $user=auth()->guard('api')->user();

        $status=ReservationScheduleStatus::create([
            'driver_name'=>$user->name,
            'date'=>date('d/m/Y'),
            'status'=>$request->status,
            'time'=>$this->formateTime(date('h:i A')),
            'note'=>$request->note,
            'reservation_schedule_id'=>$schedule->id
        ]);

        return response()->json([
            "data"=>$status
        ]);

    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function fetchPayments(Request $request){
        $reservations=Reservation::join('payments','payments.reservation_id','reservations.id')->where('user_id',auth()->guard('api')->user()->id)->get();        
        return response()->json([
            "data"=>$reservations
        ]);
    }
    
}








// 'https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=Kamra Kalan, Attock, Punjab, Pakistan&destinations=Attock, Punjab, Pakistan&key=AIzaSyAhN31rb1mb4ejPcZiAqqjtrjOly3l960g'


// http://api.geonames.org/findNearbyPostalCodesJSON?lat=40.6655101&lng=-73.89188969999998&username=cosmos00999















































































// // PICK UP
// $reservationSchedule=ReservationSchedule::create([
//     "address"=>$pickup->address,
//     "address_lat"=>$pickup->lat,
//     "address_lng"=>$pickup->lng,
//     "block_no"=>$block_no,
//     "time_block"=>$schedule_data->time_block,
//     "distance"=>$time_data["pickup"]["distance"],
//     "travel_time"=>$time_data["pickup"]['travel_time'],
//     "est_time_arrival"=>$time_data["pickup"]['est_time_arrival'],
//     "service_time"=>$time_data["pickup"]['service_time'],
//     "est_time_complete"=>$time_data["pickup"]['est_time_complete'],
//     "type"=>"pickup",
//     "status"=>"scheduled",
//     "zone_id"=>$pickup_zone_id,
//     "date"=>str_replace('-','/',$schedule_data->date),
//     "unit_truck_id"=>$schedule_data->unit_id,
//     "reservation_id"=>$reservation->id
// ]);

// $this->attachDriver(str_replace('-','/',$schedule_data->date),$schedule_data->unit_id,$reservationSchedule);

// // DROPOFF
// $reservationSchedule=ReservationSchedule::create([
//     "address"=>$dropoff->address,
//     "address_lat"=>$dropoff->lat,
//     "address_lng"=>$dropoff->lng,
//     "block_no"=>$block_no,
//     "time_block"=>$schedule_data->time_block,
//     "distance"=>$travel_data->distance,
//     "travel_time"=>$time_data["dropoff"]['travel_time'],
//     "est_time_arrival"=>$time_data["dropoff"]['est_time_arrival'],
//     "service_time"=>$time_data["dropoff"]['service_time'],
//     "est_time_complete"=>$time_data["dropoff"]['est_time_complete'],
//     "type"=>"dropoff",
//     "status"=>"scheduled",
//     "zone_id"=>$dropoff_zone_id,
//     "date"=>str_replace('-','/',$schedule_data->date),
//     "unit_truck_id"=>$schedule_data->unit_id,
//     "reservation_id"=>$reservation->id
// ]);

// $this->attachDriver(str_replace('-','/',$schedule_data->date),$schedule_data->unit_id,$reservationSchedule);











































// private function getTimeData($pickup_schedule,$dropoff_schedule,$package)
//     {
//         // travel time
//         $distance=NULL;
//         $duration=NULL;
//         $last_est_time_complete=NULL;

//         $office_travel_time=NULL;
        
//         if($schedule_data->new_distance&&$schedule_data->new_duration){
//             $distance=$schedule_data->new_distance;
//             $duration=$schedule_data->new_duration;
//             $last_est_time_complete=$this->setMaxLeaveTime($schedule_data);
//         }else{
//             $office_travel_time=true;
//             $duration=$travel_data->office_duration;
//             $distance=$travel_data->office_distance;
//         }
               
//         $travel_time_min=(int)(round(((float)$duration)/60));
//         if($travel_time_min>60){
//             $travel_time=intdiv($travel_time_min, 60).' Hr'.' '. ($travel_time_min % 60)." Min";
//         }else{
//             $travel_time=$travel_time_min." Min";
//         }


//         // est Time Arrival
//         if($last_est_time_complete){
//             $time=strtotime("+".$travel_time_min." minutes", strtotime($last_est_time_complete));
//         }else{
//             $time=strtotime(explode('-',$schedule_data->time_block)[0]);
//         }        
//         $est_time_arrival=date('h:i A',$time);

        
//         // service time
//         $service_time_min=(int)(((float)$package->service_time)/2);
//         if($service_time_min>60){
//             $service_time=intdiv($service_time_min, 60).' Hr'.' '.($service_time_min % 60)." Min";
//         }else{
//             $service_time=$service_time_min." Min";
//         }

//         // Est Time Complete
//         $temp=(strtotime("+".$service_time_min." minutes", strtotime($est_time_arrival)));
//         $est_time_complete=date('h:i A', $temp);


//         $pickup_data=[
//             "travel_time"=>$travel_time,
//             "distance"=>$distance,
//             "est_time_arrival"=>$est_time_arrival,
//             "service_time"=>$service_time,
//             "est_time_complete"=>$est_time_complete
//         ];

//         // Travel Time
//         $travel_time=(round(((float)$travel_data->duration)/60));
//         $travel_time_min=(int)(round(((float)$travel_data->duration)/60));
//         if($travel_time>60){
//             $travel_time=intdiv($travel_time, 60).' Hr'.' '.($travel_time % 60)." Min";
//         }else{
//             $travel_time=$travel_time." Min";
//         }

//         // est time Arrival
//         $temp=strtotime("+".$travel_time_min." minutes", strtotime($pickup_data['est_time_complete']));
//         $est_time_arrival=date('h:i A',$temp);
        

//         // est Time Complete
//         $temp=(strtotime("+".$service_time_min." minutes", strtotime($est_time_arrival)));
//         $est_time_complete=date('h:i A', $temp);

//         $block_no=$this->calculateTimeBlockNumber(explode('-',$schedule_data->time_block)[1],$est_time_complete);

//         $dropoff_data=[
//             "travel_time"=>$travel_time,
//             "service_time"=>$service_time,
//             "est_time_arrival"=>$est_time_arrival,
//             "est_time_complete"=>$est_time_complete
//         ];
//         return [
//             "dropoff"=>$dropoff_data,
//             "pickup"=>$pickup_data,
//             "block_no"=>$block_no
//         ];
//     }