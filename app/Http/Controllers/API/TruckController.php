<?php

namespace App\Http\Controllers\API;

use App\LuggerPrice;
use App\TruckType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TruckController extends Controller
{
    public function get_trucks(Request $request){
        $trucks = TruckType::all();
        $per_lugger_cost = LuggerPrice::first()->per_lugger_cost;

        foreach ($trucks as $value){
            $value->price = ($request->kilometers * $value->per_kilomoeter_price) + $value->luggers * $per_lugger_cost;
        }
        return $trucks;
    }
}
