<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use App\UnitTruck;  
use App\OfficeLocation;
use App\Schedule;
use App\ReservationSchedule;
use App\UnitTimeBlock;
use App\Package;
class UnitTruckController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function fetchTimeBlock(Request $request){
        $date=$request->date;
        $office=OfficeLocation::findOrFail($request->office_id);
        $time_blocks=["_6am_9am"=>FALSE,"_9am_12pm"=>FALSE,"_12pm_3pm"=>FALSE,"_3pm_6pm"=>FALSE,"_6pm_9pm"=>FALSE];
        $type=$request->type;
        $unit_trucks=$this->getUnits($type,$office);
                
        foreach($unit_trucks as $unit_truck){
            $unitSchedule=Schedule::where('date',$date)->where('unit_truck_id',$unit_truck->id)->first();
            if(!$unitSchedule){
                continue;
            }
            $unitTimeBlock=UnitTimeBlock::where('unit_truck_id',$unit_truck->id)->where('date',$date)->first();
            
            if(!$unitTimeBlock){
                $unitTimeBlock=UnitTimeBlock::create([
                    'date'=>$date,
                    "unit_truck_id"=>$unit_truck->id,
                    "schedule_time"=>$unitSchedule->start_time."-".$unitSchedule->end_time
                ]);
                $this->disableTimeBlockBySchedule($unitSchedule,$unitTimeBlock);
            }


            $time_blocks=$this->checkAvalibility($unitTimeBlock,$time_blocks);
        }
        return response()->json([
            "data"=>$time_blocks
        ]);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private function getUnits($type,$office){
        if($type=='pickup'){
            $units=$office->unit_trucks->where('service_type_pickUP','pickup');
        }else if($type=='delivery'){
            $units=$office->unit_trucks->where('service_type_delivery','delivery');
        }
        
        return $units;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private function checkAvalibility($unitTimeBlock,$time_blocks){

        if($unitTimeBlock->_6am_9am){
            $time_blocks['_6am_9am']=TRUE;
        }
        if($unitTimeBlock->_9am_12pm){
            $time_blocks['_9am_12pm']=TRUE;
        }
        if($unitTimeBlock->_12pm_3pm){
            $time_blocks['_12pm_3pm']=TRUE;
        }
        if($unitTimeBlock->_3pm_6pm){
            $time_blocks['_3pm_6pm']=TRUE;
        }
        if($unitTimeBlock->_6pm_9pm){
            $time_blocks['_6pm_9pm']=TRUE;
        }
        
        return $time_blocks;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private function disableTimeBlockBySchedule($schedule,$unitTimeBlock){
        $times=["6am","9am","12pm","3pm","6pm","9pm"];
        $startIndex=NULL;
        $endIndex=NULL;
        for($i=0;$i<count($times);$i++){
            if((strtotime($schedule->start_time) <= strtotime($times[$i]))&&(!isset($startIndex))){
                $startIndex=$i;
            }
            if((strtotime($schedule->end_time) <= strtotime($times[$i]))&&(!$endIndex)){
                $endIndex=$i;
            }
            if($startIndex&&$endIndex){
                break;
            }
        }
        $blocks=[];
        
        for($i=$startIndex;$i<$endIndex;$i++){
            $b="_".$times[$i].'_'.$times[$i+1];
            array_push($blocks,$b);
        }

        if(!in_array('_6am_9am',$blocks)){
            $unitTimeBlock->_6am_9am=(bool)$unitTimeBlock->_6am_9am||FALSE;
        }else{
            $unitTimeBlock->_6am_9am=(bool)$unitTimeBlock->_6am_9am||TRUE;
        }
        if(!in_array('_9am_12pm',$blocks)){
            $unitTimeBlock->_9am_12pm=(bool)$unitTimeBlock->_9am_12pm||FALSE;
        }else{
            $unitTimeBlock->_9am_12pm=(bool)$unitTimeBlock->_9am_12pm||TRUE;
        }
        if(!in_array('_12pm_3pm',$blocks)){
            $unitTimeBlock->_12pm_3pm=(bool)$unitTimeBlock->_12pm_3pm||FALSE;
        }else{
            $unitTimeBlock->_12pm_3pm=(bool)$unitTimeBlock->_12pm_3pm||TRUE;
        }
        if(!in_array('_3pm_6pm',$blocks)){
            $unitTimeBlock->_3pm_6pm=(bool)$unitTimeBlock->_3pm_6pm||FALSE;
        }else{
            $unitTimeBlock->_3pm_6pm=(bool)$unitTimeBlock->_3pm_6pm||TRUE;
        }   
        if(!in_array('_6pm_9pm',$blocks)){
            $unitTimeBlock->_6pm_9pm=(bool)$unitTimeBlock->_6pm_9pm||FALSE;
        }else{
            $unitTimeBlock->_6pm_9pm=(bool)$unitTimeBlock->_6pm_9pm||TRUE;
        }
        $unitTimeBlock->save();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function checkTimeBlockAvalibility(Request $request){
        $time=$this->calculateTotalTime($request);
        $time_block=explode("-",$request->timeblock);
        $time_block='_'.$time_block[0]."_".$time_block[1];
        $office=OfficeLocation::findOrFail($request->office_id);
        $type=$request->type;
        $units=$this->getUnits($type,$office);

        foreach($units as $unit){
            $unitTimeBlock=UnitTimeBlock::where('unit_truck_id',$unit->id)->where('date',$request->date)->where($time_block,TRUE)->first();
            if($unitTimeBlock){
                $time=(int)(((float)$time)/(60*60));
                if($time<6){
                    $data=$this->checkLastReservation($unitTimeBlock->unit_truck_id,$request);
                    return response()->json($data);
                }
            }            
        }

        return response()->json([
            "data"=>"no unit Avalible for this time block"
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private function calculateTotalTime($request){
        $package=Package::findOrFail($request->package_id);
        $time=(float)$request->travel_data['duration']+(float)$package->service_time;
        return $time;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private function checkLastReservation($unit_id,$schedule_data){
        $date=$schedule_data->date;
        $next_reservation=[
            "new_distance"=>NULL,
            "new_duration"=>NULL
        ];

        $pre_schedule_id=NULL;
        $next_schedule_id=NULL;

        $schedules=ReservationSchedule::where('date',$date)
                ->where('unit_truck_id',$unit_id)
                ->whereIn('type',['dropoff','delivery'])->get();
        $new_time_block=$this->blockToTime($schedule_data->timeblock);
        foreach($schedules as $schedule){
            $old_time_block=$this->blockToTime($schedule->time_block);

            $hours_difference=($old_time_block-$new_time_block)/3600;

            if($hours_difference==(-3*$schedule->block_no)){
                $data=$this->getDistanceTime($schedule_data->dropoff_lat,$schedule_data->dropoff_lng,$schedule->address_lat,$schedule->address_lng);

                $distance=$data->rows[0]->elements[0]->distance->value;
                $duration=$data->rows[0]->elements[0]->duration->value;
                $pre_schedule_id=$schedule->id;
                $next_reservation["new_distance"]=$distance;
                $next_reservation["new_duration"]=$duration;
                
            }else if($hours_difference==(3*$schedule->block_no)){
                $next_schedule_id=$schedule->id;
            }
        }

        return [
            "pre_schedule_id"=>$pre_schedule_id,
            "next_schedule_id"=>$next_schedule_id,
            "new_distance"=>$next_reservation["new_distance"],
            "new_duration"=>$next_reservation['new_duration'],
            "unit_id"=>$unit_id
        ];
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private function getDistanceTime($dropoff_lat,$dropoff_lng,$lat,$lng){

        $http = new \GuzzleHttp\Client;
        try {
            $response = $http->get('https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins='.$lat.','.$lng.'&destinations='.$dropoff_lat.','.$dropoff_lng.'&key=AIzaSyAhN31rb1mb4ejPcZiAqqjtrjOly3l960g');
            $data=json_decode($response->getBody());

            return $data;

        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            return $e;
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private function blockToTime($time_block){
        $time_block=explode('-',$time_block);
        $time_block=(strtotime($time_block[0]) + strtotime($time_block[1]) )/2;
        return $time_block;
    }
}
