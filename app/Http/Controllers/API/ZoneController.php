<?php

namespace App\Http\Controllers\API;

use App\Rule;
use App\RuleType;
use App\RuleZone;
use App\ZoneZipCode;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ZoneController extends Controller
{
    public function get_zone(Request $request){

        $zone = ZoneZipCode::where('zip_code' , $request->zip_code)->first();
        if ($zone){

            $zone_rule = RuleZone::where('zone_id' , $zone->zone_id)->pluck('rule_id');

            if (count($zone_rule) > 0){
                $type = RuleType::where('type' , $request->type)->first();
                $rule = Rule::whereIn('id' , $zone_rule)->where('rule_type_id' , $type->id)->first();


                if ($rule->rule_type_id == $type->id){
                    $rule->days = explode(',' , $rule->days);

                    $rule->type = RuleType::where('id' , $rule->rule_type_id)->first();
                    return response()->json([
                        'status' => true,
                        'message' => 'Zone Found',
                        'data' => $rule
                    ]);
                } else {
                    return response()->json([
                        'status' => false,
                        'message' => 'This zone is not for this type'
                    ]);
                }

            }

            return response()->json([
                'status' => false,
                'message' => 'No Rule Exist against this zipcode'
            ]);

//
        } else{
            return response()->json([
                'status' => false,
                'message' => 'No Zone Exist'
            ]);
        }
    }

    public function confirmZipCode(Request $request){
        $pickup_zipcode=$request->pickup_zipcode;
        $dropoff_zipcode=$request->dropoff_zipcode;

        $pickup_confirm=FALSE;
        $dropoff_confirm=FALSE;
        $pickup_zone_id=NULL;
        $dropoff_zone_id=NULL;

        $zipCodes=ZoneZipCode::all();
        foreach($zipCodes as $zip){
            if($pickup_zipcode==$zip->zip_code){
                $pickup_confirm=TRUE;
                $pickup_zone_id=$zip->zone->id;
            }
            if($dropoff_zipcode==$zip->zip_code){
                $dropoff_confirm=TRUE;
                $dropoff_zone_id=$zip->zone->id;
            }
        }
        return response()->json([
            "data"=>[
                'pickup_confirm'=>$pickup_confirm,
                "pickup_zone_id"=>$pickup_zone_id,
                "dropoff_confirm"=>$dropoff_confirm,
                "dropoff_zone_id"=>$dropoff_zone_id
            ]
        ]);
    }

}
