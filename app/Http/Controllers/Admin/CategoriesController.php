<?php

namespace App\Http\Controllers\Admin;

use App\Categories;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoriesController extends Controller
{
    public function index(){
        $categories = Categories::all();
         return view('categories.index' , compact('categories'));
    }

    public function add(){
        return view('categories.add');
    }

    public function store(Request $request){
        $request->validate([
            "icon"=>'required|image|mimes:jpg,png,jpeg,svg|max:4000',
            'name' => "required",
            'description' => "required"
        ]);

        if($request->hasFile('icon')){
            $allowedfileExtension=['jpg', 'jpeg' , 'JPG' ,'png', 'PNG'];

            $file = $request->file('icon');

            $filename = $file->getClientOriginalName();

            $newname = str_replace(' ', '', $filename);

            $extension = $file->getClientOriginalExtension();

            $check=in_array($extension,$allowedfileExtension);

            if($check)
            {
                $finalname = time().$newname;

                $file->move('images/package', $finalname);

                $path = '/images/package'."/". $finalname;
            }
            Categories::create([
                'name' => $request->name,
                'icon' => $path,
                'description' => $request->description
            ]);
        }


        return redirect('admin/categories');
    }

    public function edit($id){
        $category = Categories::find($id);

        return view('categories.edit' , compact('category'));
    }

    public function update(Request $request , $id){

        $category = Categories::find($id);
        $path = $category->icon;
        $request->validate([
            'name' => "required",
            'description' => "required"
        ]);

        if($request->hasFile('icon')){

            $request->required([
                "icon"=>'required|image|mimes:jpg,png,jpeg,svg|max:4000',
            ]);

            $allowedfileExtension=['jpg', 'jpeg' , 'JPG' ,'png', 'PNG'];

            $file = $request->file('icon');

            $filename = $file->getClientOriginalName();

            $newname = str_replace(' ', '', $filename);

            $extension = $file->getClientOriginalExtension();

            $check=in_array($extension,$allowedfileExtension);

            if($check)
            {
                $finalname = time().$newname;

                $file->move('images/package', $finalname);

                $path = '/images/package'."/". $finalname;
            }
        }


        $category->name = $request->name;
        $category->icon = $path;
        $category->description = $request->description;
        $category->save();

        return redirect('admin/categories');
    }

    public function delete($id){
        $category = Categories::find($id);
        $category->delete();

        return back();


    }
}
