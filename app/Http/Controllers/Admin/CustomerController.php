<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;

class CustomerController extends Controller
{
    public function index(){
        $customers=User::where('role','customer')->get();

        return view('customers.index',["customers"=>$customers]);
    }

    public function impersonateAsCustomer(Request $request){
        $user=User::findOrFail($request->user_id);

        $token = $user->createToken('Token Name')->accessToken;


        return response()->json([
            "token"=>$token
        ]);
    }
    

}
