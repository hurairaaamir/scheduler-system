<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\User;


class DriverController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $drivers=User::where('role','driver')->get();
        return view('drivers.index',['drivers'=>$drivers]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('drivers.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'status'=>['required']
        ]);
        User::create([
            "name"=>$request->name,
            "email"=>$request->email,
            'role'=>"driver",
            'status'=>$request->status,
            "password"=>Hash::make($request->password)
        ]);

        return redirect('admin/drivers')->with('success','driver created successfully');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit($id)
    {
        $driver=User::findOrFail($id);
        return view('drivers.edit',['driver'=>$driver]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'status'=>['required']
        ]);

        $User=User::findOrFail($id);
        $User->update([
            "name"=>$request->name,
            "email"=>$request->email,
            'role'=>"driver",
            'status'=>$request->status,
            "password"=>Hash::make($request->password)
        ]);

        return redirect('admin/drivers')->with('success','driver updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Users=User::findOrFail($id);

        $Users->delete();

        return redirect('admin/drivers')->with('info','driver deleted successfully');
    }    
}
