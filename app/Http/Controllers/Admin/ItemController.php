<?php

namespace App\Http\Controllers\Admin;

use App\Item;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ItemController extends Controller
{
    public function index(){
        $items = Item::all();
        return view('item.index' , compact('items'));
    }

    public function add(){
        return view('item.add');
    }

    public function store(Request $request){

        $item = new Item();
        $item->name = $request->name;
        $item->save();

        return redirect('admin/item');
    }

    public function edit($id){

        $item = Item::find($id);
        return view('item.edit' , compact('item'));
    }

    public function update(Request $request , $id){

        $item = Item::find($id);
        $item->name = $request->name;
        $item->save();


        return redirect('admin/item');
    }

    public function delete($id){
        $item = Item::find($id);
        $item->delete();
        return back();
    }
}
