<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LuggersController extends Controller
{
    public function index(){
        $luggers = User::where('role' , 'lugger')->get();

        return view('luggers.index', compact('luggers'));
    }
}
