<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\OfficeLocation;
use App\Zone;


class OfficeLocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $offices=OfficeLocation::all();
        return view('office-location.index',['data'=>$offices]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $zones=Zone::all();
        return view('office-location.add',compact('zones'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(["name"=>"required","address"=>"required"]);
        
        $officelocation=OfficeLocation::create([
            "address"=>$request->address,
            "lat"=>$request->lat,
            "lng"=>$request->lng,
            "name"=>$request->name
        ]);

        $officelocation->zones()->attach($request->zones);

        return redirect('admin/office-location')->with('success','Office location created successfully');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data=OfficeLocation::findOrFail($id);
        $zones=Zone::all();
        return view('office-location.edit',['data'=>$data,'zones'=>$zones]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate(["name"=>"required","address"=>"required"]);
        $officeLocation=OfficeLocation::findOrFail($id);

        $officeLocation->update([
            "address"=>$request->address,
            "lat"=>$request->lat,
            "lng"=>$request->lng,
            "name"=>$request->name
        ]);

        $officeLocation->zones()->detach();
        $officeLocation->zones()->attach($request->zones);

        return redirect('admin/office-location')->with('success','Office location updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $officeLocation=OfficeLocation::findOrFail($id);

        $officeLocation->delete();

        return redirect('admin/office-location')->with('info','Office location deleted successfully');
    }
}
