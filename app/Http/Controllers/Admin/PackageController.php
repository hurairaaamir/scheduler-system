<?php

namespace App\Http\Controllers\Admin;

use App\Categories;
use App\Package;
use App\Rule;
use App\Zone;
use App\Item;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PackageController extends Controller
{
    public function index(){
        $packages = Package::all();

        foreach ($packages as $value){
            $value->category = Categories::where('id' , $value->category_id)->first();
            $value->rule = Rule::where('id' , $value->rule_id)->first();
        }

        return view('package.index' , compact('packages'));
    }

    public function add(){

        $categories = Categories::all();

        $rules = Rule::all();

        $zones=Zone::all();

        $items=Item::all();

        return view('package.add' , ['categories'=>$categories,'rules'=>$rules,'zones'=>$zones,'items'=>$items]);
    }

    public function store(Request $request){
        // dd($request->all());
        $request->validate([
            "image"=>'required|image|mimes:jpg,png,jpeg,svg|max:4000',
            "name"=>"required",
            "category"=>"required",
            "rules"=>"required",
            "deposit"=>"required",
            "distance_charge"=>"required"
        ]);

        
            
        $img_path=$this->upload_img($request->image);

        $package = new Package();
        $package->name  = $request->name;
        $package->category_id  = $request->category;
        $package->image  = $img_path;


        $package->deposit=$request->deposit;
        $package->distance_charge=$request->distance_charge;
        $package->origin_fee=$request->origin_fee;
        $package->origin_fee_limit=$request->origin_fee_limit;
        $package->return_fee=$request->return_fee;
        $package->return_fee_limit=$request->return_fee_limit;
        $package->moving_charge=$request->moving_charge;
        $package->moving_item=$request->moving_item;
        $package->padding_time=$request->padding_time;
        $package->service_time=$request->service_time;
        $package->save();
        
        $package->rules()->attach($request->rules);
        
        $zones=[];
        $rules=$package->rules;
        foreach($rules as $rule){
            $zone_ids=$rule->zones->pluck('id');
            foreach($zone_ids as $id){
                if(!in_array($id,$zones)){
                    array_push($zones,$id);
                }
            }
        }
        foreach($zones as $zone){
            $package->zones()->attach($zone);
        }



        $item_ids = array_filter($request->item_ids);
        $index=0;
        
        foreach ($request->value as $key => $value){
            if($value){
                ($item_ids[$index])?$package->items()->attach([$item_ids[$index]=>['value' => $value]]):'';
                $index++;
            }
        }

        return redirect('admin/package')->with('success','Package created successfully');
    }

    public function edit($id){
        
        $package=Package::findOrFail($id);
        $categories = Categories::all();
        $rules = Rule::all();
        $zones=Zone::all();
        $items=Item::all();
        $selected_items=[];
        foreach($package->items as $i){
            array_push($selected_items,["id"=>$i->id,"name"=>$i->name,"value"=>$i->pivot->value]);
        }
        
        return view('package.edit',[
            'categories'=>$categories ,
            'rules'=>$rules ,
            'package'=>$package,
            'zones'=>$zones,
            'items'=>$items,
            'selected_items'=>$selected_items
        ]);
    }

    public function update(Request $request , $id){
        $request->validate([
            "image"=>'image|mimes:jpg,png,jpeg,svg|max:4000',
            "name"=>"required",
            "category"=>"required",
            "deposit"=>"required",
            "distance_charge"=>"required"
        ]);

        $package = Package::findOrFail($id);

        if($request->image){
            $this->delete_img($request->image);
            $img_path=$this->upload_img($request->image);
            $package->image=$img_path;
        }

        $package->name  = $request->name;
        $package->category_id  = $request->category;
        $package->deposit=$request->deposit;
        $package->distance_charge=$request->distance_charge;
        $package->origin_fee=$request->origin_fee;
        $package->origin_fee_limit=$request->origin_fee_limit;
        $package->return_fee=$request->return_fee;
        $package->return_fee_limit=$request->return_fee_limit;
        $package->moving_charge=$request->moving_charge;
        $package->moving_item=$request->moving_item;
        $package->padding_time=$request->padding_time;
        $package->service_time=$request->service_time;
        $package->save();
        $package->rules()->detach();
        $package->items()->detach();
        $package->zones()->detach();

        $package->rules()->attach($request->rules);

        $zones=[];
        $rules=$package->rules;
        foreach($rules as $rule){
            $zone_ids=$rule->zones->pluck('id');
            foreach($zone_ids as $id){
                if(!in_array($id,$zones)){
                    array_push($zones,$id);
                }
            }
        }
        foreach($zones as $zone){
            $package->zones()->attach($zone);
        }
        
        $item_ids = array_filter($request->item_ids);
        $index=0;
        
        foreach ($request->value as $key => $value){
            if($value){
                (isset($item_ids[$index]))?$package->items()->attach([$item_ids[$index]=>['value' => $value]]):'';
                $index++;
            }else if(isset($item_ids[$index])){
                (isset($item_ids[$index]))?$package->items()->attach([$item_ids[$index]=>['value' => $value]]):'';
                $index++;
            }
            
        }

        return redirect('admin/package')->with('success','Package updated successfully');
    }
    private function upload_img($image){
        $image_name = uniqid().'.'.$image->getClientOriginalExtension();
        $destinationPath=public_path().'/images/package/';
        $image->move($destinationPath, $image_name);
        $path='/images/package/'.$image_name;

        return $path;
    }
    private function delete_img($img_path){
        $path=public_path($img_path);            

        @unlink($path);
    }

    public function delete($id){
        $package=Package::findOrFail($id);
        $this->delete_img($package->image);
        $package->delete();

        return back()->with('info','Rule deleted successfully');
    }
}
