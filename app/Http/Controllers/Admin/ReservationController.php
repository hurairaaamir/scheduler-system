<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Reservation;
use App\UnitTruck;
use App\User;
use App\Package;
use App\Payment;
use App\ReservationScheduleStatus;
use App\ReservationSchedule;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class ReservationController extends Controller
{
    public function index(){
        $reservations = Reservation::all();
        return view('reservation.index' , compact('reservations'));
    }

    public function create(){
        $trucks=UnitTruck::all();
        $packages=Package::all();
        return view('reservation.add',['trucks'=>$trucks,'packages'=>$packages]);
    }

    public function store(Request $request){
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'phone_number'=>['required'],
            'package'=>['required'],
            'bulky_items'=>['required'],
            'unit'=>['required'],
            "note"=>['min:10'],
            'pickup_address'=>['required'],
            'pickup_time_block'=>['required'],
            'Dropoff_address'=>['required'],
            'Dropoff_time_block'=>['required'],
        ]);

        $customer=User::create([
            "name"=>$request->name,
            "email"=>$request->email,
            "password"=>Hash::make($request->password),
            "role"=>"customer",
            "phone_number"=>$request->phone_number
        ]);
        
        $reservation=Reservation::create([
            "date"=>date("m/d/Y"),
            'bulky_items'=>$request->bulky_items,
            "quote_amount"=>342,
            "deposit_amount"=>20,
            "note"=>$request->note,
            "package_id"=>$request->package,
            "note"=>$request->note,
            "user_id"=>$customer->id,
            "status"=>"active"
        ]);
        
        ReservationSchedule::create([
            "address"=>$request->pickup_address,
            "time_block"=>$request->pickup_time_block,
            "type"=>"delivery",
            "status"=>"scheduled",
            "date"=>date("m/d/Y"),
            "service_time"=>1,
            "unit_truck_id"=>$request->unit,
            "reservation_id"=>$reservation->id
        ]);

        ReservationSchedule::create([
            "address"=>$request->pickup_address,
            "time_block"=>$request->pickup_time_block,
            "type"=>"pickup",
            "status"=>"scheduled",
            "date"=>date("m/d/Y"),
            "service_time"=>1,
            "unit_truck_id"=>$request->unit,
            "reservation_id"=>$reservation->id
        ]);

        ReservationSchedule::create([
            "address"=>$request->Dropoff_address,
            "time_block"=>$request->Dropoff_time_block,
            "type"=>"dropoff",
            "status"=>"scheduled",
            "date"=>date("m/d/Y"),
            "service_time"=>1,
            "unit_truck_id"=>$request->unit,
            "reservation_id"=>$reservation->id
        ]);

        Payment::create([
            "amount"=>$reservation->deposit_amount,
             "stripe_id"=>"38023-4325-423454-49",   
            "reservation_id"=>$reservation->id
        ]);

        return redirect('admin/reservation')->with('success','Reservation updated successfully');
    }

    public function show($id)
    {
        $reservation = Reservation::find($id);

        return view('reservation.show' , compact('reservation'));
    }

    public function update(Request $request , $id){

        $reservation = Reservation::find($id);

        return redirect('admin/reservation');
    }

    public function updateStatus(Request $request , $id){
        $reservation = Reservation::find($id);
        $reservation->update([
            "status"=>$request->status
        ]);

        return back()->with('success','status changed successfully');;
    }

    public function delete($id){
        $reservation = Reservation::find($id);
        $reservation->delete();

        return back()->with('info','Reservation deleted successfully');
    }

    public function dropoffSchedule(Request $request){
        $request->validate([
            "type"=>"required",
            "reservation_schedule_id"=>"required",
            "status"=>"required",
            "time"=>"required"
        ]);
        
        $reservationSchedule=ReservationSchedule::findOrFail($request->reservation_schedule_id);
        $status=ReservationScheduleStatus::create([
            "driver_name"=>auth()->user()->name,
            "date"=>date("m/d/Y"),
            "status"=>$request->status,
            "time"=>$request->time,
            "note"=>$request->note,
            "reservation_schedule_id"=>$reservationSchedule->id
        ]);

        $data=view('renders.statusRow',["status"=>$status])->render();
        return response()->json(["data"=>$data,"type"=>$request->type]);

    }
    public function customerUpdate(Request $request){
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', ],
            'password' => ['required', 'string', 'min:8'],
            'phone_number'=>['required'],
        ]);

        $user=User::findOrFail($request->user_id);
        if($user->email!=$request->email){
            $request->validate([
                "email"=>'unique:users'
            ]);
        }
        $user->update([
            "name"=>$request->name,
            "email"=>$request->email,
            "password"=>Hash::make($request->password),
            'phone_number'=>$request->phone_number
        ]);
        return response()->json([ "data"=>$user]);
    }

    public function test(){
        $http = new \GuzzleHttp\Client;


        try {
            $response = $http->get('https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=40.6655101,-73.89188969999998&destinations=40.6905615,-73.9976592&key=AIzaSyAhN31rb1mb4ejPcZiAqqjtrjOly3l960g');
            $data=json_decode($response->getBody());
            return response()->json([
                "data"=>$data
            ]);

        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            return response()->json([
                "data"=>$e
            ]);
        }
    }

    
}
