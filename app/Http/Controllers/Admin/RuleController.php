<?php

namespace App\Http\Controllers\Admin;

use App\Rule;
use App\Zone;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RuleController extends Controller
{
    public function index(){
        $rules = Rule::all();

        return view('rule.index' , compact('rules'));
    }

    public function add(){
        $zones = Zone::all();

        return view('rule.add' , compact('zones'));
    }

    public function store(Request $request){
        dd($request->all());
        $request->validate([
            "name"=>"required|unique:rules",
            "rule_type"=>"required",
            "days"=>"required",
            "zones"=>"required"
        ]);
        
        $rule = new Rule();
        $rule->name = $request->name;
        $rule->rule_type = $request->rule_type;
        $rule->days = implode(',' , $request->days);
        $rule->save();

        $rule->zones()->attach($request->zones);

        return redirect('admin/rule')->with('success','Rules created successfully');
    }

    public function edit($id){
        $rule=Rule::findOrFail($id);
        $zones = Zone::all();
        return view('rule.edit',['rule'=>$rule,"zones"=>$zones]);
    }

    public function update(Request $request , $id){
        $request->validate([
            "name"=>"required",
            "rule_type"=>"required",
            "days"=>"required",
            "zones"=>"required"
        ]);
        
        $rule=Rule::find($id);
        $rule->name = $request->name;
        $rule->rule_type = $request->rule_type;
        $rule->days = implode(',' , $request->days);
        $rule->save();
        return redirect('admin/rule')->with('success','Unit updated successfully');
    }

    public function delete($id){
        $rule = Rule::find($id);
        $rule->delete();
        return back()->with('info','Rule deleted successfully');
    }
}
