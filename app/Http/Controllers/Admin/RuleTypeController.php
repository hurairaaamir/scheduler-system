<?php

namespace App\Http\Controllers\Admin;

use App\Rule;
use App\RuleType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RuleTypeController extends Controller
{
    public function index(){
        $rule_types = RuleType::all();
        return view('rule-type.index' , compact('rule_types'));
    }

    public function add(){
        return view('rule-type.add');
    }

    public function store(Request $request){


        $rule_type = new RuleType();
        $rule_type->name = $request->name;
        $rule_type->type = $request->type;
        $rule_type->save();

        return redirect('admin/rule-type');
    }

    public function edit($id){

        $rule_type = RuleType::find($id);

        return view('rule-type.edit' , compact('rule_type'));
    }

    public function update(Request $request , $id){

        $rule_type = RuleType::find($id);
        $rule_type->name = $request->name;
        $rule_type->type = $request->type;
        $rule_type->save();

        return redirect('admin/rule-type');
    }

    public function delete($id){
        $rule_type = RuleType::find($id);
        $rule_type->delete();

        return back();


    }
}
