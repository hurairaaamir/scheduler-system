<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\ScheduleImport;
use App\Schedule;
use App\User;
use App\UnitTruck;
use App\UnitTimeBlock;

class ScheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Schedules=Schedule::all();
        return view('schedule.index',['data'=>$Schedules]);
    }
/**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data=UnitTruck::all();
        return view('schedule.add',['data'=>$data]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'date'=>"required",
            'start_time'=>"required",
            'end_time'=>"required",
        ]);

        $unitTrucks=UnitTruck::whereIn('id',$request->unit)->get();

        $date=explode('/',$request->date);
        $date=$date[1].'/'.$date[0].'/'.$date[2];
        
        
        foreach($unitTrucks as $unitTruck){

            $timeBlock=UnitTimeBlock::where('unit_truck_id',$unitTruck->id)->where('date',$date)->first();

            if($timeBlock){
                if($this->checkBlock($timeBlock)){
                    $timeBlock->delete();
                }else{
                    return ;
                }
            }

            $schedule=Schedule::create([
                "unit_truck_id"=>$unitTruck->id,
                "date"=>$date,
                "start_time"=>$request->start_time,
                "end_time"=>$request->end_time
            ]);            

            $schedule->users()->attach($request->users);
        }

        return redirect('admin/schedule')->with('success','Units created successfully');
    }
    /**
     * Uploading CSV
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function csvUpload(Request $request){
        $data=Excel::import(new ScheduleImport,request()->file('csv'));
        return redirect('admin/schedule')->with('success','csv uploaded successfully');
    }

    public function csv(){
        return view('schedule.csv');
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $schedule=Schedule::findOrFail($id);
        $users=User::where('role','driver')->get();
        $data=UnitTruck::all();
        
        return view('schedule.edit',['schedule'=>$schedule,"data"=>$data,"id"=>$id,"users"=>$users]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    private function checkBlock($timeBlock){
        return  (!$timeBlock->_6am_9am_schedule_id)&&(!$timeBlock->_9am_12pm_schedule_id)&&(!$timeBlock->_12pm_3pm_schedule_id)&&(!$timeBlock->_3pm_6pm_schedule_id)&&(!$timeBlock->_6pm_9pm_schedule_id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)
    {
        $request->validate([
            'date'=>"required",
            'start_time'=>"required",
            'end_time'=>"required",
        ]);

        $schedule=Schedule::findOrFail($id);
        
        $unitTrucks=UnitTruck::whereIn('id',$request->unit)->get();
        
        $schedule->delete();

        $date=explode('/',$request->date);
        $date=$date[1].'/'.$date[0].'/'.$date[2];
        foreach($unitTrucks as $unitTruck){
            
            $timeBlock=UnitTimeBlock::where('unit_truck_id',$unitTruck->id)->where('date',$date)->first();

            if($timeBlock){
                if($this->checkBlock($timeBlock)){
                    $timeBlock->delete();
                }else{
                    return ;
                }
            }
            
            $schedule=Schedule::create([
                "unit_truck_id"=>$unitTruck->id,
                "date"=>$date,
                "start_time"=>$request->start_time,
                "end_time"=>$request->end_time
            ]);            
            
            $schedule->users()->detach();
            $schedule->users()->attach($request->users);
        }
        

        return redirect('admin/schedule')->with('success','Unit updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Schedule=Schedule::findOrFail($id);

        $timeBlock=UnitTimeBlock::where('unit_truck_id',$Schedule->unit_truck_id)->where('date',$Schedule->date)->first();

        if($timeBlock){
            if($this->checkBlock($timeBlock)){
                $timeBlock->delete();
            }else{
                return ;
            }
        }

        $Schedule->delete();

        return redirect('admin/schedule')->with('info','Unit deleted successfully');
    }


    public function assignDriverView($schedule_id,$unit_truck_id)
    {
        $drivers=User::where('role','driver')->get();

        $schedule=Schedule::findOrFail($schedule_id);

        // dd($schedule);
        // dump($schedule_id);
        // dd($unit_truck_id);

        return view('schedule.assign_driver' , [
            "drivers"=>$drivers,
            "schedule"=>$schedule
        ]);
    }

    public function assignDriver(Request $request){
        $request->validate([
            "users"=>"required"
        ]);

        $schedule=Schedule::findOrFail($request->schedule_id);
        $schedule->users()->detach();
        $schedule->users()->attach($request->users);
        
        
        return redirect('admin/schedule')->with('success','Driver assgin successfully');
    }
}

