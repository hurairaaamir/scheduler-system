<?php

namespace App\Http\Controllers\Admin;

use App\TruckType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TruckController extends Controller
{
    public function index(){
        $trucks = TruckType::all();
        return view('truck.index' , compact('trucks'));
    }

    public function add(){
        return view('truck.add');
    }

    public function store(Request $request){

        if($request->hasFile('icon')){

            $allowedfileExtension=['jpg', 'jpeg' , 'JPG' ,'png', 'PNG'];

            $file = $request->file('icon');

            $filename = $file->getClientOriginalName();

            $newname = str_replace(' ', '', $filename);

            $extension = $file->getClientOriginalExtension();

            $check=in_array($extension,$allowedfileExtension);

            if($check)
            {
                $finalname = time().$newname;

                $file->move('uploads/'.'images', $finalname);

                $path = 'uploads/'.'images'."/". $finalname;


                TruckType::create([
                    'name' => $request->name,
                    'icon' => url($path),
                    'luggers' => $request->luggers,
                    'per_kilomoeter_price' => $request->per_kilomoeter_price,
                    'description' => $request->description
                ]);

            }

        }

        return redirect('admin/trucks');
    }

    public function edit($id){
        $truck = TruckType::find($id);


        return view('truck.edit' , compact('truck'));
    }



    public function update(Request $request , $id){
        $truck = TruckType::find($id);

        $path = $truck->icon;

        if($request->hasFile('icon')){

            $allowedfileExtension=['jpg', 'jpeg' , 'JPG' ,'png', 'PNG'];

            $file = $request->file('icon');

            $filename = $file->getClientOriginalName();

            $newname = str_replace(' ', '', $filename);

            $extension = $file->getClientOriginalExtension();

            $check=in_array($extension,$allowedfileExtension);

            if($check)
            {
                $finalname = time().$newname;

                $file->move('uploads/'.'images', $finalname);

                $path = url('uploads/'.'images'."/". $finalname);


            }

        }

        $truck->name = $request->name;
        $truck->icon = $path;
        $truck->luggers = $request->luggers;
        $truck->per_kilomoeter_price = $request->per_kilomoeter_price;
        $truck->description = $request->description;

        $truck->save();

        return redirect('admin/trucks');
    }


    public function delete($id){

    }
}
