<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\UnitTruck;
use App\OfficeLocation;

class UnitTruckController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $unitTrucks=UnitTruck::all();
        return view('units.index',['data'=>$unitTrucks]);
    }
/**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $locations=OfficeLocation::all();
        return view('units.add',['locations'=>$locations]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'=>"required",
            'office_location'=>"required",
            'max_bulky_items'=>"required",
        ]);

        UnitTruck::create([
            'name'=>$request->name,
            'office_location_id'=>$request->office_location,
            'max_bulky_items'=>$request->max_bulky_items,
            'service_type_pickUP'=>$request->service_type_pickUP,
            'service_type_delivery'=>$request->service_type_delivery
        ]);

        return redirect('admin/unit-truck')->with('success','Unit created successfully');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $locations=OfficeLocation::all();
        $data=UnitTruck::findOrFail($id);

        return view('units.edit',['data'=>$data,'locations'=>$locations]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'=>"required",
            'office_location'=>"required",
            'max_bulky_items'=>"required",
        ]);

        $unitTrucks=UnitTruck::findOrFail($id);

        $unitTrucks->update([
            'name'=>$request->name,
            'office_location_id'=>$request->office_location,
            'max_bulky_items'=>$request->max_bulky_items,
            'service_type_pickUP'=>$request->service_type_pickUP,
            'service_type_delivery'=>$request->service_type_delivery
        ]);

        return redirect('admin/unit-truck')->with('success','Unit updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $unitTrucks=UnitTruck::findOrFail($id);

        $unitTrucks->delete();

        return redirect('admin/unit-truck')->with('info','Unit deleted successfully');
    }
}
