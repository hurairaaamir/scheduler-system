<?php

namespace App\Http\Controllers\Admin;

use App\Zone;
use App\ZoneZipCode;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ZoneController extends Controller
{
    public function index(){
        $zones = Zone::all();
        foreach ($zones as $value){
            $value->zip_code = ZoneZipCode::where('zone_id' , $value->id)->pluck('zip_code')->toArray();
        }

//        dd($zones);
        return view('zone.index' , compact('zones'));
    }

    public function add(){
        return view('zone.add');
    }

    public function store(Request $request){
        $arr = explode(',' , $request->zip_codes);
        $postal_code = ZoneZipCode::whereIn('zip_code' , $arr)->get();
        if (count($postal_code) > 0){
            return back()->with('message' , 'ZipCode Already Exist');
        }
        $zone = new Zone();
        $zone->name = $request->name;
        $zone->save();



        foreach ($arr as $value){
            $zip_code = new ZoneZipCode();
            $zip_code->zone_id = $zone->id;
            $zip_code->zip_code = $value;
            $zip_code->save();
        }
        return redirect('admin/zone');
    }

    public function edit($id){

        $zone = Zone::find($id);

        $zone_zipcode = ZoneZipCode::where('zone_id' , $zone->id)->get()->pluck('zip_code')->toArray();
        $zone_zipcode = implode(",",$zone_zipcode);

        return view('zone.edit' , compact('zone' , 'zone_zipcode'));
    }

    public function update(Request $request , $id){

        ZoneZipCode::where('zone_id' , $id)->delete();

        $zone = Zone::find($id);
        $zone->name = $request->name;
        $zone->save();

        $arr = explode(',' , $request->zip_codes);
        foreach ($arr as $value){
            $zip_code = new ZoneZipCode();
            $zip_code->zone_id = $zone->id;
            $zip_code->zip_code = $value;
            $zip_code->save();
        }
        return redirect('admin/zone');
    }

    public function delete($id){

        ZoneZipCode::where('zone_id' , $id)->delete();

        $zone = Zone::find($id);
        $zone->delete();
        return back();


    }
}
