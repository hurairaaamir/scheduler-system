<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Http\Resources\PackageResourceCollection;

class PackageResource extends ResourceCollection
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected $distance;
    protected $duration;
    protected $officeData;

    public function __construct($resource,$officeData=null,$distance=null,$duration=null){
        $this->distance=$distance;
        $this->duration=$duration;
        $this->officeData=$officeData;
        parent::__construct($resource);
    }

    public function toArray($request)
    {


        return [
            'data' => $this->collection->map(function($n){
                return [
                    "office_id"=>$this->officeData['selectedOffice']->id,
                    "office_distance"=>$this->officeData['officeDistance'],
                    "office_duration"=>$this->officeData['officeDuration'],
                    "distance"=>$this->distance->value,
                    "duration"=>$this->duration->value,
                    "category_name"=>$n->category->name,
                    'id'=>$n->id,
                    "name"=>$n->name,
                    "image"=>$n->image,
                    "description"=>$n->description,
                    "luggers"=>$n->luggers,
                    "deposit"=>$n->deposit,
                    "charge"=>$this->generatePrice($n->distance_charge,$n->origin_fee,$n->origin_fee_limit,$n->retrun_fee,$n->return_fee_limit,$n->deposit),
                    "distance_charge"=>$n->distance_charge,
                    "origin_fee"=>$n->origin_fee,
                    "origin_fee_limit"=>$n->origin_fee_limit,
                    "return_fee"=>$n->return_fee,
                    "return_fee_limit"=>$n->return_fee_limit,
                    "moving_charge"=>$n->moving_charge,
                    "moving_item"=>$n->moving_item,
                    "padding_time"=>$n->padding_time,
                    "service_time"=>$n->service_time,
                    "category_id"=>$n->category_id,
                    "extras"=>$n->items
                ];
            })
        ];
    }
    private function generatePrice($distance_charge, $origin_fee, $origin_fee_limit, $retrun_fee, $return_fee_limit,$deposit){
        $distance=$this->getMiles($this->distance->value);
        $officeDistance=$this->getMiles($this->officeData['officeDistance']);
        $returnDistance=$this->getMiles($this->officeData['returnDistance']);

        $charge=($distance*(float)$distance_charge)*$deposit;
        if($officeDistance > $origin_fee_limit){
            $charge=$charge+ ($origin_fee * $officeDistance);
        }
        if($returnDistance>$return_fee_limit){
            $charge=$charge + ($retrun_fee*$returnDistance);
        }

        return $charge;
    }
    private function getMiles($m) {
        return ((float)$m)*0.000621371192;
   }
    
}
