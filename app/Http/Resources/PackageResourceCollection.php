<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class PackageResourceCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected $distance;
    protected $duration;
    protected $officeData;
    
    public function setValue($officeData,$distance,$duration){
        $this->distance=$distance;
        $this->duration=$duration;
        $this->officeData=$officeData;
        return $this;
    }

    public function toArray($request)
    {
        // return parent::toArray($request);
        return $this->collection->map(function(PackageResource $resource) use($request){
            return $resource->setValue($this->officeData,$this->distance,$this->duration)->toArray($request);
        })->all();
        
    }
}
