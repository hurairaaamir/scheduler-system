<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Helpers\Helper;

class ReservationScheduleCollection extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);

        return [
            "reservation_id"=>"Gpq".Helper::styleId($this->reservation->id),
            "date"=>$this->date,
            "customer_name"=>$this->reservation->user->name,
            "phone_no"=>$this->reservation->user->customer_profile->phone_number,
            "time_block"=>($this->type=='dropoff')?'x':$this->time_block,
            "service_type"=>$this->type,
            "status"=>$this->status,
            "address"=>$this->address,
            "travel_time"=>$this->travel_time,
            "est_time_arrival"=>$this->est_time_arrival,
            "service_time"=>$this->service_time,
            "est_time_complete"=>$this->est_time_complete,
            "max_leave_time"=>($this->max_leave_time??"x"),
            "note"=>$this->reservation->note,
            "id"=>$this->id ,
            "schedule_status"=>$this->reservation_schedule_status   
        ];
    }
}
