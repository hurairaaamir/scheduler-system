<?php

namespace App\Imports;

use App\Schedule;
use Maatwebsite\Excel\Concerns\ToModel;
use App\UnitTimeBlock;

class ScheduleImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        // dd($row);
        $timeBlock=UnitTimeBlock::where('unit_truck_id',$row[0])->where('date',$row[3])->first();

        if($timeBlock){
            if($this->checkBlock($timeBlock)){
                $timeBlock->delete();
            }else{
                return ;
            }
        }
        return new Schedule([
            "unit_truck_id"=>$row[0],
            "start_time"=>strtolower($row[1]),
            "end_time"=>strtolower($row[2]),
            "date"=>$row[3]
        ]);
    }
}
