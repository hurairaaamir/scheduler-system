<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $fillable=['name'];

    public function packages(){
        return $this->belongsToMany(Package::class);
    }

    // public function pack_items(){
    //     return $this->hasMany(PackageItem::class , 'item_id' , 'id');
    // }
}
