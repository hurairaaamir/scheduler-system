<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OfficeLocation extends Model
{
    protected $fillable=['address','name','lat','lng'];

    public function zones(){
        return $this->belongsToMany(Zone::class);
    }

    public function unit_trucks(){
        return $this->hasMany(UnitTruck::class);
    }
}
