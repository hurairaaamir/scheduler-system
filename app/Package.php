<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    protected $fillable=[
        'name',
        'image',
        'deposit',
        'distance_charge',
        'origin_fee',
        'origin_fee_limit',
        'return_fee',
        'return_fee_limit',
        'moving_charge',
        'moving_item',
        "luggers",
        'padding_time',
        'service_time',
        'category_id',
        'rule_id',
        'zone_id'
    ];

    public function rules(){
        return $this->belongsToMany(Rule::class);
    }

    public function zones(){
        return $this->belongsToMany(Zone::class);
    }

    public function category(){
        return $this->belongsTo(Categories::class);
    }

    public function items(){
        return $this->belongsToMany(Item::class)->withPivot(['value']);
    }
}
