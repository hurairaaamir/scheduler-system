<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackageItem extends Model
{
    public function package(){
        return $this->belongsTo(Package::class);
    }

    public function items(){
        return $this->belongsTo(Item::class);
    }
}
