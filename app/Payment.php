<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable=['amount','stripe_id','reservation_id'];

    public function reservation(){
        return $this->belongsTo(Reservation::class);
    }
}
