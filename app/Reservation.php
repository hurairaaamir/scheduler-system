<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    protected $fillable=['date','bulky_items','quote_amount','deposit_amount','note','status','package_id','user_id','office_id','bulky_items_list'];


    public function package(){
        return $this->belongsTo(Package::class);
    }
    public function payment(){
        return $this->hasOne(Payment::class);
    }
    public function reservation_schedule(){
        return $this->hasMany(ReservationSchedule::class);
    }
    public function user(){
        return $this->belongsTo(User::class);
    }
    public function unit_truck(){
        return $this->belongsTo(UnitTruck::class);
    }
}
