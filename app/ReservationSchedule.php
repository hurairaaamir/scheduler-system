<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReservationSchedule extends Model
{
    protected $fillable=[
        'address',
        'date',
        'time_block',
        'status',
        'type',
        'service_time',
        "block_no",
        'unit_truck_id',
        'reservation_id',
        'travel_time',
        'est_time_arrival',
        'est_time_complete',
        'max_leave_time',
        'distance',
        "address_lat",
        "zone_id",
        "address_lng",
        'active',
        "duration",
        "office_duration",
        'ordering',
        "office_distance"
    ];

    public function unit_truck(){
        return $this->belongsTo(UnitTruck::class);
    }
    

    public function reservation(){
        return $this->belongsTo(Reservation::class);
    }
    public function reservation_schedule_status(){
        return $this->hasMany(ReservationScheduleStatus::class);
    }

    public function users(){
        return $this->belongsToMany(User::class);
    }
}
