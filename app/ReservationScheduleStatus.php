<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReservationScheduleStatus extends Model
{
    protected $fillable=['driver_name','date','status','time','note','reservation_schedule_id'];

    public function reservation_schedule(){
        return $this->belongsTo(ReservationSchedule::class);
    }
}
