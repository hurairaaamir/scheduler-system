<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rule extends Model
{
    protected $fillable=[
        'name',
        'rule_type',
        'days',
    ];
    
    public function packages(){
        return $this->belongsToMany(Package::class);
    }

    public function zones(){
        return $this->belongsToMany(Zone::class);
    }
}
