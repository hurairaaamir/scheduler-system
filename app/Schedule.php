<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    protected $fillable=['unit_truck_id','user_id','date','start_time','end_time'];

    public function unit_truck(){
        return $this->belongsTo(UnitTruck::class);
    }

    public function users(){
        return $this->belongsToMany(User::class);
    }
}
