<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TruckType extends Model
{
    protected $fillable = [
        'name' , 'luggers', 'per_kilomoeter_price' , 'icon' , 'description'
    ];
}
