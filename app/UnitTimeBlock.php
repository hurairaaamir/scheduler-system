<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UnitTimeBlock extends Model
{
    protected $fillable=[
        '6am_9am',
        '_6am_9am_schedule_id',
        '9am_12pm',
        '_9am_12pm_schedule_id',
        '12pm_3pm',
        '_12pm_3pm_schedule_id',
        '3pm_6pm',
        '_3pm_6pm_schedule_id',
        '6pm_9pm',
        '_6pm_9pm_schedule_id',
        "schedule_time",
        'date',
        'unit_truck_id',

    ];
}
