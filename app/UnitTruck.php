<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UnitTruck extends Model
{
    protected $fillable =['name','max_bulky_items','office_location_id','service_type_pickUP','service_type_delivery'];

    public function office_location(){
        return $this->belongsTo(OfficeLocation::class);
    }

    public function schedules(){
        return $this->hasMany(Schedule::class);
    }

    public function reservation_schedule_status(){
        return $this->hasMany(ReservationSchedule::class);
    }
}
