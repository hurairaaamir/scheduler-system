<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Zone extends Model
{
    protected $fillable=['name'];
    public function zone_rule(){
        return $this->hasMany(RuleZone::class);
    }
    public function rules(){
        return $this->hasMany(Rule::class);
    }

    public function packages(){
        return $this->belongsToMany(Package::class);
    }
    
    public function office_location(){
        return $this->belongsToMany(OfficeLocation::class);
    }
}
