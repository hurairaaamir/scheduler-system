<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ZoneZipCode extends Model
{
    protected $fillable=['zip_code','zone_id'];

    public function zone(){
        return $this->belongsTo(Zone::class);
    }
}
