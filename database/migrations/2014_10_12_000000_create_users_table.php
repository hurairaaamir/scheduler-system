<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('provider_id')->nullable();
            $table->string('provider')->nullable();
            $table->string('phone_number')->nullable();
            $table->string('avatar')->nullable();
            $table->string('email')->unique();
            $table->string('dob')->nullable();
            $table->string('area')->nullable();
            $table->string('phone_service_provider')->nullable();
            $table->boolean('is_lift_upto_100_lbs')->default(0);
            $table->boolean('own_truck')->default(0);
            $table->boolean('work_over_weekend')->default(0);
            $table->string('refered_by')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password')->nullable();
            $table->enum('status',['inactive','active'])->default('active');
            $table->enum('role', ['admin', 'customer', 'lugger','driver'])->default('customer');
            $table->enum('gender', ['male', 'female', 'other'])->default('other');
            $table->string('email_verification_token')->nullable();
            $table->string('password_reset_token')->nullable();
            $table->string('address_line_one')->nullable();
            $table->string('address_line_two')->nullable();
            $table->boolean('is_phone_verfied')->default(0);
            $table->string('phone_verification_code')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
