<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('image');
            $table->text('description')->nullable();
            $table->string("luggers")->nullable();
            $table->float('deposit');
            $table->float('distance_charge');
            $table->float('origin_fee')->nullable();
            $table->float('origin_fee_limit')->nullable();
            $table->float('return_fee')->nullable();
            $table->float('return_fee_limit')->nullable();
            $table->float('moving_charge')->nullable();
            $table->float('moving_item')->nullable();
            $table->float('padding_time')->nullable();
            $table->float('service_time')->nullable();

            $table->unsignedBigInteger('category_id')->nullable();
            $table->foreign('category_id')
                ->references('id')->on('categories')
                ->onDelete('cascade');

            // $table->unsignedBigInteger('rule_id')->nullable();
            // $table->foreign('rule_id')
            //     ->references('id')->on('rules')
            //     ->onDelete('cascade');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('packages');
    }
}
