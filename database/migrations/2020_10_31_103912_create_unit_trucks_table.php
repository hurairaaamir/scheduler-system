<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnitTrucksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('unit_trucks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('max_bulky_items');
            $table->string('service_type_delivery')->nullable();
            $table->string('service_type_pickUP')->nullable();

            $table->unsignedBigInteger('office_location_id');
            $table->foreign('office_location_id')
                ->references('id')->on('office_locations')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('unit_trucks');
    }
}
