<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservation_schedules', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('address');
            $table->string('address_lat')->nullable();
            $table->string('address_lng')->nullable();
            $table->string('distance')->nullable();
            $table->string('duration')->nullable();
            $table->string('office_distance')->nullable();
            $table->string('office_duration')->nullable();
            
            $table->string('date')->nullable();
            $table->string('time_block')->nullable();
            $table->string('block_no')->nullable();
            $table->string('status')->nullable();
            $table->string('active')->nullable();
            $table->string('ordering')->nullable();
            $table->enum('type',['delivery','pickup','dropoff']);
        
            
            $table->string('travel_time')->nullable();
            $table->string('est_time_arrival')->nullable();
            $table->string('service_time')->nullable();
            $table->string('est_time_complete')->nullable();
            $table->string('max_leave_time')->nullable();



            $table->unsignedBigInteger('unit_truck_id')->nullable();
            $table->foreign('unit_truck_id')
                ->references('id')->on('unit_trucks')
                ->onDelete('cascade');
            $table->unsignedBigInteger('zone_id')->nullable();
            $table->foreign('zone_id')
                ->references('id')->on('zones')
                ->onDelete('cascade');
            $table->unsignedBigInteger('reservation_id')->nullable();
            $table->foreign('reservation_id')
                ->references('id')->on('reservations')
                ->onDelete('cascade');
                
            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservation_schedules');
    }
}
