<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationScheduleStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservation_schedule_statuses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('driver_name');
            $table->string('date')->nullable();
            $table->string('status');
            $table->string('time');
            $table->text('note');

            $table->unsignedBigInteger('reservation_schedule_id')->nullable();
            $table->foreign('reservation_schedule_id')
                ->references('id')->on('reservation_schedules')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservation_schedule_statuses');
    }
}
