<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUnitTimeBlocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('unit_time_blocks', function (Blueprint $table) {
            $table->id();
            $table->boolean('_6am_9am')->default(TRUE);
            $table->string('_6am_9am_schedule_id')->nullable();

            $table->boolean('_9am_12pm')->default(TRUE);
            $table->string('_9am_12pm_schedule_id')->nullable();

            $table->boolean('_12pm_3pm')->default(TRUE);
            $table->string('_12pm_3pm_schedule_id')->nullable();

            $table->boolean('_3pm_6pm')->default(TRUE);
            $table->string('_3pm_6pm_schedule_id')->nullable();

            $table->boolean('_6pm_9pm')->default(TRUE);
            $table->string('_6pm_9pm_schedule_id')->nullable();

            $table->string('date');
            $table->string('schedule_time');

            $table->unsignedBigInteger('unit_truck_id');
            $table->foreign('unit_truck_id')
                ->references('id')->on('unit_trucks')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('unit_time_blocks');
    }
}
