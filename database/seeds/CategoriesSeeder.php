<?php

use Illuminate\Database\Seeder;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories=[
            [
                "id"=>1,
                "name"=>"House Move",
                "description"=>"Donec vitae sapien ut libero",
                "icon"=>'/vectors/categories/houseMove.svg',
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42',
            ],
            [
                "id"=>2,
                "name"=>"Storage Move",
                "description"=>"Donec vitae sapien ut libero",
                "icon"=>'/vectors/categories/storageMove.svg',
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42',
            ],
            [
                "id"=>3,
                "name"=>"Store Move",
                "description"=>"Donec vitae sapien ut libero",
                "icon"=>'/vectors/categories/storeMove.svg',
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42',
            ],
            [
                "id"=>4,
                "name"=>"Donation Pickup",
                "description"=>"Donec vitae sapien ut libero",
                "icon"=>'/vectors/categories/9.svg',
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42',
            ],
            [
                "id"=>5,
                "name"=>"Junk Removal",
                "description"=>"Donec vitae sapien ut libero",
                "icon"=>'/vectors/categories/11.svg',
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42',
            ],
            [
                "id"=>6,
                "name"=>"Small Move",
                "description"=>"Donec vitae sapien ut libero",
                "icon"=>'/vectors/categories/1.svg',
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42',
            ]
        ];
        \App\Categories::insert($categories);
    }
}
