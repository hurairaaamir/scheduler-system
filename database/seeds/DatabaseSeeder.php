<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UserSeeder::class,
            CategoriesSeeder::class,
            ZoneSeeder::class,
            RuleSeeder::class,
            UnitTruckSeeder::class,
            // Datebase\Seeds\PackageSeeder::class
            PackageSeeder::class,
        ]);

    }
}
    