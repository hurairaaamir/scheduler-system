<?php

use Illuminate\Database\Seeder;

class RuleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rules=[
            [
                "id"=>1,
                "name"=>"NC Rule",
                "rule_type"=>"pickup",
                "days"=>"Monday,Tuesday,Wednesday,Thursday,Friday,Saturday",
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42',
            ],
            [
                "id"=>2,
                "name"=>"NC Rule",
                "rule_type"=>"delivery",
                "days"=>"Monday,Tuesday,Wednesday,Thursday,Friday,Saturday",
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42',
            ],
            [
                "id"=>3,
                "name"=>"NC Rule",
                "rule_type"=>"dropoff",
                "days"=>"Monday,Tuesday,Wednesday,Thursday,Friday,Saturday",
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42',
            ],


            [
                "id"=>4,
                "name"=>"SC Rule",
                "rule_type"=>"pickup",
                "days"=>"Monday,Tuesday,Wednesday,Thursday,Friday,Saturday",
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42',
            ],
            [
                "id"=>5,
                "name"=>"SC Rule",
                "rule_type"=>"delivery",
                "days"=>"Monday,Tuesday,Wednesday,Thursday,Friday,Saturday",
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42',
            ],
            [
                "id"=>6,
                "name"=>"SC Rule",
                "rule_type"=>"dropoff",
                "days"=>"Monday,Tuesday,Wednesday,Thursday,Friday,Saturday",
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42',
            ]
        ];

        \App\Rule::insert($rules);

        $rules=\App\Rule::all();

        foreach($rules as $rule){
            $rule->zones()->attach([1,2]);
        }
    }
}
