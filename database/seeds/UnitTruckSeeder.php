<?php

use Illuminate\Database\Seeder;

class UnitTruckSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $unit_trucks=[
            [
                'id'=>1,
                'name'=>"Ford Ranger",
                "office_location_id"=>1,
                "max_bulky_items"=>'20',
                "service_type_pickUP"=>"pickup",
                "service_type_delivery"=>"delivery",
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42',
            ],
            [
                'id'=>2,
                'name'=>"Toyota Tundra",
                "office_location_id"=>1,
                "max_bulky_items"=>'20',
                "service_type_pickUP"=>"pickup",
                "service_type_delivery"=>"delivery",
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42',
            ],
            [
                'id'=>3,
                'name'=>"Ram 1500",
                "office_location_id"=>1,
                "max_bulky_items"=>'20',
                "service_type_pickUP"=>"pickup",
                "service_type_delivery"=>"delivery",
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42',
            ],
            [
                'id'=>4,
                'name'=>"Ford Ranger",
                "office_location_id"=>2,
                "max_bulky_items"=>'20',
                "service_type_pickUP"=>"pickup",
                "service_type_delivery"=>"delivery",
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42',
            ],
            [
                'id'=>5,
                'name'=>"Toyota Tundra",
                "office_location_id"=>2,
                "max_bulky_items"=>'20',
                "service_type_pickUP"=>"pickup",
                "service_type_delivery"=>"delivery",
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42',
            ],
            [
                'id'=>6,
                'name'=>"Ram 1500",
                "office_location_id"=>2,
                "max_bulky_items"=>'20',
                "service_type_pickUP"=>"pickup",
                "service_type_delivery"=>"delivery",
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42',
            ]
        ];
        \App\UnitTruck::insert($unit_trucks);

        
        for($i=0;$i<5;$i++){
            $date=date('d/m/Y', strtotime(' +'.$i.' day'));

            $schedules=[
                // [
                //     "unit_truck_id"=>1,
                //     "date"=>$date,
                //     "start_time"=>"6am",
                //     "end_time"=>"9pm",
                //     'created_at' => '2019-04-15 19:14:42',
                //     'updated_at' => '2019-04-15 19:14:42',
                // ],
                // [
                //     "unit_truck_id"=>2,
                //     "date"=>$date,
                //     "start_time"=>"6am",
                //     "end_time"=>"9pm",
                //     'created_at' => '2019-04-15 19:14:42',
                //     'updated_at' => '2019-04-15 19:14:42',
                // ],
                // [
                //     "unit_truck_id"=>3,
                //     "date"=>$date,
                //     "start_time"=>"6am",
                //     "end_time"=>"9pm",
                //     'created_at' => '2019-04-15 19:14:42',
                //     'updated_at' => '2019-04-15 19:14:42',
                // ],
                [
                    "unit_truck_id"=>4,
                    "date"=>$date,
                    "start_time"=>"6am",
                    "end_time"=>"9pm",
                    'created_at' => '2019-04-15 19:14:42',
                    'updated_at' => '2019-04-15 19:14:42',
                ],
                // [
                //     "unit_truck_id"=>5,
                //     "date"=>$date,
                //     "start_time"=>"6am",
                //     "end_time"=>"9pm",
                //     'created_at' => '2019-04-15 19:14:42',
                //     'updated_at' => '2019-04-15 19:14:42',
                // ],
                // [
                //     "unit_truck_id"=>6,
                //     "date"=>$date,
                //     "start_time"=>"6am",
                //     "end_time"=>"9pm",
                //     'created_at' => '2019-04-15 19:14:42',
                //     'updated_at' => '2019-04-15 19:14:42',
                // ]
            ];
            \App\Schedule::insert($schedules);
        }

        $schedules=\App\Schedule::where('unit_truck_id',4)->get();
        $length=count($schedules);
        foreach($schedules as $index=> $schedule){
                $schedule->users()->attach([3]);
        }

        // $schedules=\App\Schedule::where('unit_truck_id',5)->get();
        // $length=count($schedules);
        // foreach($schedules as $index=> $schedule){
        //         $schedule->users()->attach([4]);
        // }
    
        $packages=[
            [
                "id"=>1,
                "name"=>"Pickup",
                "image"=>"/images/package/pickup1.png",
                "luggers"=>"1",
                "deposit"=>"10",
                "distance_charge"=>"1",
                "origin_fee"=>'1',
                "origin_fee_limit"=>"10",
                "return_fee"=>"1",
                "return_fee_limit"=>"15",
                "moving_charge"=>"5",
                "moving_item"=>'3',
                "padding_time"=>'60',
                "service_time"=>"90",
                "category_id"=>1,
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42',
            ],
            [
                "id"=>2,
                "name"=>"Pickup",
                "image"=>"/images/package/pickup2.png",
                "luggers"=>"2",
                "deposit"=>"15",
                "distance_charge"=>"2",
                "origin_fee"=>'2',
                "origin_fee_limit"=>"10",
                "return_fee"=>"2",
                "return_fee_limit"=>"15",
                "moving_charge"=>"6",
                "moving_item"=>'5',
                "padding_time"=>'90',
                "service_time"=>"120",
                "category_id"=>1,
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42',
            ],
            [
                "id"=>3,
                "name"=>"Van",
                "image"=>"/images/package/van.png",
                "luggers"=>"2",
                "deposit"=>"20",
                "distance_charge"=>"3.5",
                "origin_fee"=>'3.5',
                "origin_fee_limit"=>"15",
                "return_fee"=>"3.5",
                "return_fee_limit"=>"15",
                "moving_charge"=>"7",
                "moving_item"=>'5',
                "padding_time"=>'90',
                "service_time"=>"120",
                "category_id"=>1,
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42',
            ],
            [
                "id"=>4,
                "name"=>"XL",
                "image"=>"/images/package/xl.png",
                "luggers"=>"2",
                "deposit"=>"25",
                "distance_charge"=>"4",
                "origin_fee"=>'2',
                "origin_fee_limit"=>"10",
                "return_fee"=>"2",
                "return_fee_limit"=>"15",
                "moving_charge"=>"1.85",
                "moving_item"=>'4',
                "padding_time"=>'90',
                "service_time"=>"120",
                "category_id"=>1,
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42',
            ],

            



















            [
                "id"=>5,
                "name"=>"Pickup",
                "image"=>"/images/package/pickup1.png",
                "luggers"=>"1",
                "deposit"=>"50",
                "distance_charge"=>"10",
                "origin_fee"=>'8.5',
                "origin_fee_limit"=>"10",
                "return_fee"=>"5",
                "return_fee_limit"=>"15",
                "moving_charge"=>"0.85",
                "moving_item"=>'7',
                "padding_time"=>'60',
                "service_time"=>"90",
                "category_id"=>2,
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42',
            ],
            [
                "id"=>6,
                "name"=>"Pickup",
                "image"=>"/images/package/pickup2.png",
                "luggers"=>"2",
                "deposit"=>"70",
                "distance_charge"=>"12",
                "origin_fee"=>'9',
                "origin_fee_limit"=>"10",
                "return_fee"=>"7",
                "return_fee_limit"=>"15",
                "moving_charge"=>"2",
                "moving_item"=>'10',
                "padding_time"=>'90',
                "service_time"=>"120",
                "category_id"=>2,
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42',
            ],
            [
                "id"=>7,
                "name"=>"Van",
                "image"=>"/images/package/van.png",
                "luggers"=>"2",
                "deposit"=>"100",
                "distance_cxharge"=>"15",
                "origin_fee"=>'10',
                "origin_fee_limit"=>"15",
                "return_fee"=>"9",
                "return_fee_limit"=>"15",
                "moving_charge"=>"0.85",
                "moving_item"=>'10',
                "padding_time"=>'90',
                "service_time"=>"120",
                "category_id"=>2,
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42',
            ],
            [
                "id"=>8,
                "name"=>"XL",
                "image"=>"/images/package/xl.png",
                "luggers"=>"2",
                "deposit"=>"120",
                "distance_charge"=>"20",
                "origin_fee"=>'9',
                "origin_fee_limit"=>"10",
                "return_fee"=>"7",
                "return_fee_limit"=>"15",
                "moving_charge"=>"1.85",
                "moving_item"=>'10',
                "padding_time"=>'90',
                "service_time"=>"120",
                "category_id"=>2,
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42',
            ],




            







            [
                "id"=>9,
                "name"=>"Pickup",
                "image"=>"/images/package/pickup1.png",
                "luggers"=>"1",
                "deposit"=>"50",
                "distance_charge"=>"10",
                "origin_fee"=>'8.5',
                "origin_fee_limit"=>"10",
                "return_fee"=>"5",
                "return_fee_limit"=>"15",
                "moving_charge"=>"0.85",
                "moving_item"=>'7',
                "padding_time"=>'60',
                "service_time"=>"90",
                "category_id"=>3,
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42',
            ],
            [
                "id"=>10,
                "name"=>"Pickup",
                "image"=>"/images/package/pickup2.png",
                "luggers"=>"2",
                "deposit"=>"70",
                "distance_charge"=>"12",
                "origin_fee"=>'9',
                "origin_fee_limit"=>"10",
                "return_fee"=>"7",
                "return_fee_limit"=>"15",
                "moving_charge"=>"2",
                "moving_item"=>'10',
                "padding_time"=>'90',
                "service_time"=>"120",
                "category_id"=>3,
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42',
            ],
            [
                "id"=>11,
                "name"=>"Van",
                "image"=>"/images/package/van.png",
                "luggers"=>"2",
                "deposit"=>"100",
                "distance_cxharge"=>"15",
                "origin_fee"=>'10',
                "origin_fee_limit"=>"15",
                "return_fee"=>"9",
                "return_fee_limit"=>"15",
                "moving_charge"=>"0.85",
                "moving_item"=>'10',
                "padding_time"=>'90',
                "service_time"=>"120",
                "category_id"=>3,
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42',
            ],
            [
                "id"=>12,
                "name"=>"XL",
                "image"=>"/images/package/xl.png",
                "luggers"=>"2",
                "deposit"=>"120",
                "distance_charge"=>"20",
                "origin_fee"=>'9',
                "origin_fee_limit"=>"10",
                "return_fee"=>"7",
                "return_fee_limit"=>"15",
                "moving_charge"=>"1.85",
                "moving_item"=>'10',
                "padding_time"=>'90',
                "service_time"=>"120",
                "category_id"=>3,
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42',
            ],
















            [
                "id"=>13,
                "name"=>"Pickup",
                "image"=>"/images/package/pickup1.png",
                "luggers"=>"1",
                "deposit"=>"50",
                "distance_charge"=>"10",
                "origin_fee"=>'8.5',
                "origin_fee_limit"=>"10",
                "return_fee"=>"5",
                "return_fee_limit"=>"15",
                "moving_charge"=>"0.85",
                "moving_item"=>'7',
                "padding_time"=>'60',
                "service_time"=>"90",
                "category_id"=>4,
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42',
            ],
            [
                "id"=>14,
                "name"=>"Pickup",
                "image"=>"/images/package/pickup2.png",
                "luggers"=>"2",
                "deposit"=>"70",
                "distance_charge"=>"12",
                "origin_fee"=>'9',
                "origin_fee_limit"=>"10",
                "return_fee"=>"7",
                "return_fee_limit"=>"15",
                "moving_charge"=>"2",
                "moving_item"=>'10',
                "padding_time"=>'90',
                "service_time"=>"120",
                "category_id"=>4,
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42',
            ],
            [
                "id"=>15,
                "name"=>"Van",
                "image"=>"/images/package/van.png",
                "luggers"=>"2",
                "deposit"=>"100",
                "distance_cxharge"=>"15",
                "origin_fee"=>'10',
                "origin_fee_limit"=>"15",
                "return_fee"=>"9",
                "return_fee_limit"=>"15",
                "moving_charge"=>"0.85",
                "moving_item"=>'10',
                "padding_time"=>'90',
                "service_time"=>"120",
                "category_id"=>4,
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42',
            ],
            [
                "id"=>16,
                "name"=>"XL",
                "image"=>"/images/package/xl.png",
                "luggers"=>"2",
                "deposit"=>"120",
                "distance_charge"=>"20",
                "origin_fee"=>'9',
                "origin_fee_limit"=>"10",
                "return_fee"=>"7",
                "return_fee_limit"=>"15",
                "moving_charge"=>"1.85",
                "moving_item"=>'10',
                "padding_time"=>'90',
                "service_time"=>"120",
                "category_id"=>4,
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42',
            ],
        ];

        \App\Package::insert($packages);
        $packages=\App\Package::all();

        foreach($packages as $package){
            $package->rules()->attach([1,2]);
            $package->zones()->attach([1,2]);
        }

    }
}
