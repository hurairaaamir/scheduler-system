<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = new \App\User();
        $admin->name = 'Admin';
        $admin->email = 'admin@admin.com';
        $admin->password = bcrypt('password');
        $admin->role = 'admin';
        $admin->save();

        User::create([
            "name"=>"Customer",
            "email"=>"customer@customer.com",
            "password"=> bcrypt('password'),
            'role'=>'customer'
        ]);
        User::create([
            "name"=>"Driver1",
            "email"=>"driver1@driver.com",
            "password"=> bcrypt('password'),
            'role'=>'driver'
        ]);
        
        User::create([
            "name"=>"Driver2",
            "email"=>"driver2@driver.com",
            "password"=> bcrypt('password'),
            'role'=>'driver'
        ]);
        User::create([
            "name"=>"Driver3",
            "email"=>"driver3@driver.com",
            "password"=> bcrypt('password'),
            'role'=>'driver'
        ]);
        DB::table('oauth_clients')->insert([
            "id"=>1,
            "user_id"=>null,
            'name'=>"Laravel Personal Access Client",
            "secret"=>"15ZB7Uz2aSROflGgPp7XPhAfsFwtZGVLcrc3RZ9i",
            // "provider"=>null,
            "redirect"=>"http://localhost",
            "personal_access_client"=>1,
            "password_client"=>0,
            "revoked"=>0
        ]);

        DB::table('oauth_clients')->insert([
            "id"=>2,
            "user_id"=>null,
            'name'=>"Laravel Password Grant Client",
            "secret"=>"IDTea6vPLR3FhFZ6Ek66fb5IFowrhgBsot2qiMEa",
            // "provider"=>"users",
            "redirect"=>"http://localhost",
            "personal_access_client"=>0,
            "password_client"=>1,
            "revoked"=>0
        ]);

        DB::table('oauth_personal_access_clients')->insert([
            "id"=>1,
            "client_id"=>1
        ]);

    }
}