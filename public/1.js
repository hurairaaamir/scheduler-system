(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[1],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Hero/Navbar2.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Hero/Navbar2.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  created: function created() {
    this.getUser();
  },
  methods: {
    getUser: function getUser() {
      this.$store.dispatch('getUser');
    }
  },
  computed: {
    user: function user() {
      return this.$store.state.user;
    },
    isDriver: function isDriver() {
      return this.$store.state.user.role == 'driver';
    },
    isCustomer: function isCustomer() {
      return this.$store.state.user.role == 'customer';
    },
    isLoggedIn: function isLoggedIn() {
      return localStorage.getItem('access_token') != null;
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Hero/Navbar2.vue?vue&type=style&index=0&id=7684b350&lang=scss&scoped=true&":
/*!**********************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Hero/Navbar2.vue?vue&type=style&index=0&id=7684b350&lang=scss&scoped=true& ***!
  \**********************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".nav_header[data-v-7684b350] {\n  box-shadow: 0px 4px 11px -5px rgba(0, 0, 0, 0.25);\n}\na[data-v-7684b350] {\n  text-decoration: none;\n  font-family: \"Nunito\", sans-serif;\n  color: #000;\n}\n.navBar[data-v-7684b350] {\n  margin-bottom: 100px;\n}\n.navBar header[data-v-7684b350] {\n  background-color: transparent !important;\n}\n.v-btn.book_btn[data-v-7684b350] {\n  background-color: #f27f0b !important;\n  color: #fff !important;\n}", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Hero/Navbar2.vue?vue&type=style&index=0&id=7684b350&lang=scss&scoped=true&":
/*!**************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Hero/Navbar2.vue?vue&type=style&index=0&id=7684b350&lang=scss&scoped=true& ***!
  \**************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--7-2!../../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../../node_modules/vue-loader/lib??vue-loader-options!./Navbar2.vue?vue&type=style&index=0&id=7684b350&lang=scss&scoped=true& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Hero/Navbar2.vue?vue&type=style&index=0&id=7684b350&lang=scss&scoped=true&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Hero/Navbar2.vue?vue&type=template&id=7684b350&scoped=true&":
/*!***************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Hero/Navbar2.vue?vue&type=template&id=7684b350&scoped=true& ***!
  \***************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "nav_header" },
    [
      _c(
        "v-container",
        [
          _c(
            "v-toolbar",
            { staticClass: "hidden-xs-and-down topBar", attrs: { flat: "" } },
            [
              _c("v-toolbar-title", [
                _c(
                  "span",
                  [
                    _c("router-link", { attrs: { to: "/" } }, [
                      _c(
                        "svg",
                        {
                          attrs: {
                            width: "155",
                            height: "50",
                            viewBox: "0 0 155 50",
                            fill: "none",
                            xmlns: "http://www.w3.org/2000/svg",
                            "xmlns:xlink": "http://www.w3.org/1999/xlink"
                          }
                        },
                        [
                          _c("rect", {
                            attrs: {
                              width: "155",
                              height: "50",
                              fill: "url(#pattern0)"
                            }
                          }),
                          _vm._v(" "),
                          _c("defs", [
                            _c(
                              "pattern",
                              {
                                attrs: {
                                  id: "pattern0",
                                  patternContentUnits: "objectBoundingBox",
                                  width: "1",
                                  height: "1"
                                }
                              },
                              [
                                _c("use", {
                                  attrs: {
                                    "xlink:href": "#image0",
                                    transform:
                                      "translate(0 -0.00116667) scale(0.00333333 0.0103333)"
                                  }
                                })
                              ]
                            ),
                            _vm._v(" "),
                            _c("image", {
                              attrs: {
                                id: "image0",
                                width: "300",
                                height: "97",
                                "xlink:href": "/images/general/seredo.png"
                              }
                            })
                          ])
                        ]
                      )
                    ])
                  ],
                  1
                )
              ]),
              _vm._v(" "),
              _c("v-spacer"),
              _vm._v(" "),
              _c(
                "v-toolbar-items",
                [
                  !_vm.isLoggedIn
                    ? _c("v-btn", { attrs: { text: "", to: "/auth/login" } }, [
                        _vm._v("\n          Login\n        ")
                      ])
                    : _vm._e(),
                  _vm._v(" "),
                  !_vm.isLoggedIn
                    ? _c(
                        "v-btn",
                        { attrs: { text: "", to: "/auth/register" } },
                        [_vm._v("\n          Register\n        ")]
                      )
                    : _vm._e(),
                  _vm._v(" "),
                  _vm.isLoggedIn
                    ? _c("v-btn", { attrs: { text: "", to: "/auth/logout" } }, [
                        _vm._v("\n          Logout\n        ")
                      ])
                    : _vm._e(),
                  _vm._v(" "),
                  _vm.isLoggedIn
                    ? _c(
                        "v-btn",
                        { attrs: { text: "" } },
                        [
                          _vm.isCustomer && _vm.isLoggedIn
                            ? _c(
                                "router-link",
                                {
                                  attrs: {
                                    to: "/dashboard/customer/my-reservation"
                                  }
                                },
                                [
                                  _c(
                                    "v-btn",
                                    {
                                      staticClass: "book_btn",
                                      attrs: { depressed: "" }
                                    },
                                    [_vm._v(" Dashboard ")]
                                  )
                                ],
                                1
                              )
                            : _vm._e(),
                          _vm._v(" "),
                          _vm.isDriver && _vm.isLoggedIn
                            ? _c(
                                "router-link",
                                { attrs: { to: "/dashboard/driver/index" } },
                                [
                                  _c(
                                    "v-btn",
                                    {
                                      staticClass: "book_btn",
                                      attrs: { depressed: "" }
                                    },
                                    [_vm._v(" Dashboard ")]
                                  )
                                ],
                                1
                              )
                            : _vm._e()
                        ],
                        1
                      )
                    : _vm._e()
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/Hero/Navbar2.vue":
/*!**************************************************!*\
  !*** ./resources/js/components/Hero/Navbar2.vue ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Navbar2_vue_vue_type_template_id_7684b350_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Navbar2.vue?vue&type=template&id=7684b350&scoped=true& */ "./resources/js/components/Hero/Navbar2.vue?vue&type=template&id=7684b350&scoped=true&");
/* harmony import */ var _Navbar2_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Navbar2.vue?vue&type=script&lang=js& */ "./resources/js/components/Hero/Navbar2.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _Navbar2_vue_vue_type_style_index_0_id_7684b350_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Navbar2.vue?vue&type=style&index=0&id=7684b350&lang=scss&scoped=true& */ "./resources/js/components/Hero/Navbar2.vue?vue&type=style&index=0&id=7684b350&lang=scss&scoped=true&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _Navbar2_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Navbar2_vue_vue_type_template_id_7684b350_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Navbar2_vue_vue_type_template_id_7684b350_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "7684b350",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Hero/Navbar2.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Hero/Navbar2.vue?vue&type=script&lang=js&":
/*!***************************************************************************!*\
  !*** ./resources/js/components/Hero/Navbar2.vue?vue&type=script&lang=js& ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Navbar2_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Navbar2.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Hero/Navbar2.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Navbar2_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Hero/Navbar2.vue?vue&type=style&index=0&id=7684b350&lang=scss&scoped=true&":
/*!************************************************************************************************************!*\
  !*** ./resources/js/components/Hero/Navbar2.vue?vue&type=style&index=0&id=7684b350&lang=scss&scoped=true& ***!
  \************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Navbar2_vue_vue_type_style_index_0_id_7684b350_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader!../../../../node_modules/css-loader!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--7-2!../../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../../node_modules/vue-loader/lib??vue-loader-options!./Navbar2.vue?vue&type=style&index=0&id=7684b350&lang=scss&scoped=true& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Hero/Navbar2.vue?vue&type=style&index=0&id=7684b350&lang=scss&scoped=true&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Navbar2_vue_vue_type_style_index_0_id_7684b350_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Navbar2_vue_vue_type_style_index_0_id_7684b350_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Navbar2_vue_vue_type_style_index_0_id_7684b350_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Navbar2_vue_vue_type_style_index_0_id_7684b350_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./resources/js/components/Hero/Navbar2.vue?vue&type=template&id=7684b350&scoped=true&":
/*!*********************************************************************************************!*\
  !*** ./resources/js/components/Hero/Navbar2.vue?vue&type=template&id=7684b350&scoped=true& ***!
  \*********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Navbar2_vue_vue_type_template_id_7684b350_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Navbar2.vue?vue&type=template&id=7684b350&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Hero/Navbar2.vue?vue&type=template&id=7684b350&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Navbar2_vue_vue_type_template_id_7684b350_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Navbar2_vue_vue_type_template_id_7684b350_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);