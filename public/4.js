(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[4],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/CustomerLayout.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/CustomerLayout.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _components_Hero_Navbar2__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../components/Hero/Navbar2 */ "./resources/js/components/Hero/Navbar2.vue");
/* harmony import */ var _components_Dashboard_components_Activetable__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../components/Dashboard/components/Activetable */ "./resources/js/components/Dashboard/components/Activetable.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    Navbar: _components_Hero_Navbar2__WEBPACK_IMPORTED_MODULE_0__["default"],
    ATable: _components_Dashboard_components_Activetable__WEBPACK_IMPORTED_MODULE_1__["default"]
  },
  data: function data() {
    return {
      show: false,
      hideTable: true
    };
  },
  methods: {
    viewDetail: function viewDetail(item) {
      this.sData = "";
      this.sData = item;
      this.hideTable = false;
      console.log(this.sData);
    }
  },
  computed: {
    alert: function alert() {
      return this.$store.state.alert;
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/CustomerLayout.vue?vue&type=style&index=0&id=d8dd10f2&lang=scss&scoped=true&":
/*!*******************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/CustomerLayout.vue?vue&type=style&index=0&id=d8dd10f2&lang=scss&scoped=true& ***!
  \*******************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".estimate[data-v-d8dd10f2] {\n  margin-top: 40px;\n  font-family: \"Nunito\", sans-serif;\n}\n.estimate .book_service[data-v-d8dd10f2] {\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  flex-direction: column;\n}\n.estimate .book_service h6[data-v-d8dd10f2] {\n  font-size: 22px;\n  margin-bottom: 20px;\n}\n.estimate .d-none[data-v-d8dd10f2] {\n  opacity: 0;\n  pointer-events: none;\n  display: none;\n}\n.estimate .book_btn[data-v-d8dd10f2],\n.estimate .view_btn[data-v-d8dd10f2] {\n  background-color: #f27f0b !important;\n  font-style: normal;\n  font-weight: normal;\n  font-size: 17px;\n  color: #ffffff;\n  max-width: 150px;\n  height: 40px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  text-align: center;\n  width: 100%;\n  border: 1px solid #f27f0b;\n  box-sizing: border-box;\n  border-radius: 4px;\n  text-decoration: none !important;\n}\n.estimate a[data-v-d8dd10f2] {\n  text-decoration: none !important;\n}\n.estimate .view_btn[data-v-d8dd10f2] {\n  max-width: 100px;\n  height: 30px;\n  font-size: 0.875rem;\n}\n.estimate h1[data-v-d8dd10f2] {\n  text-align: center;\n}\n.estimate form[data-v-d8dd10f2] {\n  max-width: 700px;\n  margin: auto;\n}\n.customer[data-v-d8dd10f2] {\n  display: flex;\n  flex-wrap: wrap;\n}\n.customer .content[data-v-d8dd10f2] {\n  flex: 1 1 auto;\n}\n.customer .v-tabs[data-v-d8dd10f2] {\n  display: flex;\n  flex: 0 1 400px;\n}\n.theme--dark.v-sheet[data-v-d8dd10f2] {\n  background-color: #f27f0b;\n  border-color: #f27f0b;\n  color: #fff;\n}", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/CustomerLayout.vue?vue&type=style&index=0&id=d8dd10f2&lang=scss&scoped=true&":
/*!***********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/CustomerLayout.vue?vue&type=style&index=0&id=d8dd10f2&lang=scss&scoped=true& ***!
  \***********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../node_modules/css-loader!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--7-2!../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../node_modules/vue-loader/lib??vue-loader-options!./CustomerLayout.vue?vue&type=style&index=0&id=d8dd10f2&lang=scss&scoped=true& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/CustomerLayout.vue?vue&type=style&index=0&id=d8dd10f2&lang=scss&scoped=true&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/CustomerLayout.vue?vue&type=template&id=d8dd10f2&scoped=true&":
/*!************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/CustomerLayout.vue?vue&type=template&id=d8dd10f2&scoped=true& ***!
  \************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "home" },
    [
      _c("Navbar"),
      _vm._v(" "),
      _c(
        "section",
        { staticClass: "estimate", attrs: { id: "cdashboard" } },
        [
          _c(
            "v-container",
            [
              _c(
                "v-row",
                { staticClass: "customer" },
                [
                  _c(
                    "v-tabs",
                    { attrs: { vertical: "", "center-active": "" } },
                    [
                      _vm.show
                        ? _c("v-tab", { class: [_vm.show ? "d-none" : ""] }, [
                            _vm._v("\n            Services\n          ")
                          ])
                        : _vm._e(),
                      _vm._v(" "),
                      _c(
                        "v-tab",
                        { attrs: { to: "/dashboard/customer/my-reservation" } },
                        [_vm._v("My Reservation")]
                      ),
                      _vm._v(" "),
                      _c(
                        "v-tab",
                        { attrs: { to: "/dashboard/customer/profile" } },
                        [_vm._v("My Profile")]
                      ),
                      _vm._v(" "),
                      _c(
                        "v-tab",
                        { attrs: { to: "/dashboard/customer/payment-info" } },
                        [_vm._v(" Payment Info")]
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "div",
                    { staticClass: "content" },
                    [
                      _vm.alert.show
                        ? _c(
                            "v-alert",
                            {
                              staticClass: "animation",
                              attrs: {
                                dense: "",
                                border: "left",
                                type: "success"
                              }
                            },
                            [
                              _vm._v(
                                "\n          " +
                                  _vm._s(_vm.alert.message) +
                                  "\n          "
                              )
                            ]
                          )
                        : _vm._e(),
                      _vm._v(" "),
                      _c("router-view")
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/views/CustomerLayout.vue":
/*!***********************************************!*\
  !*** ./resources/js/views/CustomerLayout.vue ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _CustomerLayout_vue_vue_type_template_id_d8dd10f2_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./CustomerLayout.vue?vue&type=template&id=d8dd10f2&scoped=true& */ "./resources/js/views/CustomerLayout.vue?vue&type=template&id=d8dd10f2&scoped=true&");
/* harmony import */ var _CustomerLayout_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./CustomerLayout.vue?vue&type=script&lang=js& */ "./resources/js/views/CustomerLayout.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _CustomerLayout_vue_vue_type_style_index_0_id_d8dd10f2_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./CustomerLayout.vue?vue&type=style&index=0&id=d8dd10f2&lang=scss&scoped=true& */ "./resources/js/views/CustomerLayout.vue?vue&type=style&index=0&id=d8dd10f2&lang=scss&scoped=true&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _CustomerLayout_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _CustomerLayout_vue_vue_type_template_id_d8dd10f2_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _CustomerLayout_vue_vue_type_template_id_d8dd10f2_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "d8dd10f2",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/CustomerLayout.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/CustomerLayout.vue?vue&type=script&lang=js&":
/*!************************************************************************!*\
  !*** ./resources/js/views/CustomerLayout.vue?vue&type=script&lang=js& ***!
  \************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CustomerLayout_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./CustomerLayout.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/CustomerLayout.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CustomerLayout_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/CustomerLayout.vue?vue&type=style&index=0&id=d8dd10f2&lang=scss&scoped=true&":
/*!*********************************************************************************************************!*\
  !*** ./resources/js/views/CustomerLayout.vue?vue&type=style&index=0&id=d8dd10f2&lang=scss&scoped=true& ***!
  \*********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_CustomerLayout_vue_vue_type_style_index_0_id_d8dd10f2_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/style-loader!../../../node_modules/css-loader!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--7-2!../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../node_modules/vue-loader/lib??vue-loader-options!./CustomerLayout.vue?vue&type=style&index=0&id=d8dd10f2&lang=scss&scoped=true& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/CustomerLayout.vue?vue&type=style&index=0&id=d8dd10f2&lang=scss&scoped=true&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_CustomerLayout_vue_vue_type_style_index_0_id_d8dd10f2_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_CustomerLayout_vue_vue_type_style_index_0_id_d8dd10f2_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_CustomerLayout_vue_vue_type_style_index_0_id_d8dd10f2_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_CustomerLayout_vue_vue_type_style_index_0_id_d8dd10f2_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./resources/js/views/CustomerLayout.vue?vue&type=template&id=d8dd10f2&scoped=true&":
/*!******************************************************************************************!*\
  !*** ./resources/js/views/CustomerLayout.vue?vue&type=template&id=d8dd10f2&scoped=true& ***!
  \******************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CustomerLayout_vue_vue_type_template_id_d8dd10f2_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./CustomerLayout.vue?vue&type=template&id=d8dd10f2&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/CustomerLayout.vue?vue&type=template&id=d8dd10f2&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CustomerLayout_vue_vue_type_template_id_d8dd10f2_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CustomerLayout_vue_vue_type_template_id_d8dd10f2_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);