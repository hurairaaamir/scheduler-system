$('#demo-dt-basic').dataTable( {
    "responsive": true,
    "language": {
        "paginate": {
            "previous": '<i class="demo-psi-arrow-left"></i>',
            "next": '<i class="demo-psi-arrow-right"></i>'
        }
    }
} );

$('#luggers-table').dataTable( {
    "responsive": true,
    "language": {
        "paginate": {
            "previous": '<i class="demo-psi-arrow-left"></i>',
            "next": '<i class="demo-psi-arrow-right"></i>'
        }
    }
} );

