
function scheduleStatusSumit(event){
    event.preventDefault();
    document.querySelector('.preloader').classList.add('preloader-in');
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        type: "POST",
        url: '/admin/reservation/dropoff-schedule/store',
        data: {
            type:event.target.elements[0].value,
            reservation_schedule_id:event.target.elements[1].value,
            note:event.target.elements[2].value,
            status:event.target.elements[3].value,
            time:getTime()
        },

        success: function(response){
            console.log
            let type=response.type;
            let data=response.data;
            document.getElementById(type+"_id").innerHTML+=data;
            event.target.reset();
            document.querySelector('.preloader').classList.remove('preloader-in');
        },
        error:function(data){
            document.querySelector('.preloader').classList.remove('preloader-in');
        }
    });
}



function getTime(){
    let d=new Date();
    var ampm = (d.getHours() >= 12) ? "PM" : "AM";
   	var hours = (d.getHours() > 12) ? d.getHours()-12 : d.getHours();   
    return hours+':'+d.getMinutes()+' '+ampm;
}


let userForm={
    user_id:'',
    name:'',
    email:'',
    phone_number:'',
    password:'',
    confirm_password:''
}

function onUserFormChange(e){
    userForm[e.target.name]=e.target.value;
}

function edit(){
    let btns=document.querySelectorAll('.customer-btns');
    let divs=document.querySelectorAll('.edit-div');
    btns.forEach(element=>{
        element.classList.contains('off')?element.classList.remove('off'):element.classList.add('off');
    });
    divs.forEach(element=>{
        element.classList.contains('off')?element.classList.remove('off'):element.classList.add('off');
    })

}
function submitChanges(event,user_id){
    // event.preventDefault();
    userForm['user_id']=user_id;
    if(validate()){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    
        $.ajax({
            type: "POST",
            url: '/admin/reservation/customer/update',
            data: userForm,
    
            success: function(response){
                document.getElementById('name_id').innerHTML=userForm.name;
                document.getElementById('email_id').innerHTML=userForm.email;
                document.getElementById('phone_number_id').innerHTML=userForm.phone_number;
                edit()
            },
            error:function(data){
                
            }
        });   
    }
}
function setInput(input){
    userForm[input.name]=input.value;
}

function validate(){    
    
   var validateElements = document.getElementsByClassName("validate");
    
    
    var inputs = Array.prototype.filter.call(validateElements,         function(element){
        return element.nodeName === 'INPUT';
    });
    
    for(var i=0; i < inputs.length; i ++ ){
        var input = inputs[i];
        setInput(input);
        if(input.value.length == 0){
            input.placeholder = "kindly enter value";
            input.classList.add("error");
            return false;
        }else{
            input.classList.remove('error');
        }
        if(input.name=='password'){
            if(input.value!=inputs[4].value){
                document.querySelector('.password_error').classList.remove('off');
                document.querySelector('.password_error').innerHTML='The password confirmation don\'t match ';
                input.classList.add("error");
                return false;
            }else{
                document.querySelector('.password_error').classList.add('off');
                document.querySelector('.password_error').innerHTML='';
            }
        }
    }
    return true;
}
function getHost(){
   return (window.location.hostname=='127.0.0.1')?'http://127.0.0.1:8000/':'https://meniuu.dev.rigrex.com/'
}

function loginAsCustomer(reservation_id,user_id){
    document.querySelector('.preloader').classList.add('preloader-in');
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        type: "POST",
        url: '/admin/customers/impersonateAsCustomer',
        data: {
            reservation_id:reservation_id,
            user_id:user_id
        },
        success: function(response){
            localStorage.setItem('access_token', response.token);
            window.location.href=getHost()+'#/dashboard/customer/my-reservation/'+reservation_id;
            // location.reload();
            document.querySelector('.preloader').classList.remove('preloader-in');
        },
        error:function(data){
            document.querySelector('.preloader').classList.remove('preloader-in');
        }
    });
}
