/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import Vue from 'vue'
import router from './router'
import vuetify from './plugins/vuetify';

import Errors from './plugins/Errors';
window.Errors =Errors;
// import Vuetify from 'vuetify'
// Vue.use(Vuetify)


import * as VueGoogleMaps from 'vue2-google-maps'
import axios from 'axios'


Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyAhN31rb1mb4ejPcZiAqqjtrjOly3l960g',
    libraries: 'places',
  },
 
})

import store from './store/store'




const app = new Vue({
    router,
    axios,
    store,
    vuetify,
    el: '#app',
});
