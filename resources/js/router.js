import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from './views/Home.vue'
import Estimate from './components/Estimate.vue'
import Packages from './components/Packages'
import PackageCards from './components/PackageCards'
import FullPackageCard from './components/FullPackageCard.vue'
import Scedules from './components/Scedules'
import Final from "./components/Final"

import SignupForm from './components/Forms/SignupForm'
import StripeForm from './components/Forms/StripeForm'
import PersonalInfoForm from './components/Forms/PersonalInfoForm'
import ElectronicForm from './components/Forms/ElectronicForm'

import AdminContactForm from './components/Forms/AdminContactForm'
import AccountConfirmForm from './components/Forms/AccountConfirmForm'
import Driver from './components/Driver/Driver'
import DriverHome from './components/Driver/index'
import EditDriver from './components/Driver/EditDriver'
import DriverProfile from './components/Driver/Profile'

import Register from './components/Auth/Register'
import Logout from './components/Auth/Logout'
import Login from './components/Auth/Login'

import MyReservation from './components/Dashboard/MyReservation'
import Profile from './components/Dashboard/Profile'
import EditProfile from './components/Dashboard/components/EditProfile'
import EditUser from './components/Dashboard/components/EditUser'
import PaymentInfo from './components/Dashboard/PaymentInfo'

import ViewReservation from './components/Dashboard/ViewReservation'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/estimate',
    component: () => import('./views/layoutNavbar2.vue'),
    children: [
      {
          
        path: 'maps/:pickup/:dropoff/packages/:pickup_zone_id/:dropoff_zone_id/:category_id/schedule/:travel_data/:package_id/:type/rules',
        component: Scedules
      },
      {  
        path: 'maps/:pickup/:dropoff/packages/:pickup_zone_id/:dropoff_zone_id/:category_id/schedule/:travel_data/:package_id/:type/full',
        component: FullPackageCard
        
      },
      {   
        path: 'maps/:data',
        component: Scedules
      },
      {
          
          path: 'maps/:pickup/:dropoff/packages/:pickup_zone_id/:dropoff_zone_id/:category_id/packageCard',
          component: PackageCards
       },
       {
          
          path: 'maps/:pickup/:dropoff/data',
          component: Estimate,
        },
        {
          path: 'maps/:pickup/:dropoff/packages/:pickup_zone_id/:dropoff_zone_id/:category_id/:category_name',
          component: Packages
        },
      ]
  },
  {
    path: '/estimate',
  
    component: () => import('./views/layoutNavbar3.vue'),

     children: [
      {
        path: 'maps/:pickup/:dropoff/packages/:pickup_zone_id/:dropoff_zone_id/:category_id/schedule/:travel_data/:package_id/:type/reservation/:schedule_data/signup',
        component: SignupForm
      },
      {
        path: 'maps/:pickup/:dropoff/packages/:pickup_zone_id/:dropoff_zone_id/:category_id/schedule/:travel_data/:package_id/:type/reservation/:schedule_data/:note/stripeform',
        component: StripeForm
      },
      {
        path: 'maps/:pickup/:dropoff/packages/:pickup_zone_id/:dropoff_zone_id/:category_id/schedule/:travel_data/:package_id/:type/reservation/:schedule_data/personalInfoform',
        component: PersonalInfoForm
      },
      {
        path: 'maps/:pickup/:dropoff/packages/:pickup_zone_id/:dropoff_zone_id/:category_id/schedule/:travel_data/:package_id/:type/reservation/:schedule_data/admincontactform',
        component: AdminContactForm
      },
      {
        path: 'maps/:pickup/:dropoff/packages/:pickup_zone_id/:dropoff_zone_id/:category_id/schedule/:travel_data/:package_id/:type/reservation/:schedule_data/accountconfirmform',
        component: AccountConfirmForm
      },
    ]
  },
  {
    path: '/full',
  
    component: () => import('./views/layoutPlainNavbar3.vue'),

     children: [
      { 
        path: 'success/:reservation_id',
        component: Final
      }
    ]
  },
  {
    path: '/estimate',
  
    component: () => import('./views/layoutPlainNavbar3.vue'),

     children: [
      {
        path: 'maps/:pickup/:dropoff/packages/:pickup_zone_id/:dropoff_zone_id/:category_id/schedule/:travel_data/:package_id/:type/reservation/:schedule_data/:note/term-condition',
        component: ElectronicForm
      }
    ]
  },
  {
    path:'/dashboard',
    component:()=>import('./views/DriverLayout.vue'),
    children:[
      { 
        path: 'driver/reservation/:date',
        component: Driver
      },
      { 
        path: 'driver/index',
        component: DriverHome
      },
      { 
        path: 'driver/profile',
        component: DriverProfile
      },
      { 
        path: 'driver/edit-driver',
        component: EditDriver
      },
    ]
  },
  {
    path:'/dashboard/customer',
    component:()=>import('./views/CustomerLayout.vue'),
    children:[
      { 
        path: 'my-reservation',
        component: MyReservation
      },
      { 
        path: 'my-reservation/:reservation_id',
        component: ViewReservation
      },
      { 
        path: 'profile',
        component: Profile
      },
      { 
        path: 'edit-user',
        component: EditUser
      },
      { 
        path: 'edit-profile',
        component: EditProfile
      },
      { 
        path: 'payment-info',
        component: PaymentInfo
      },
    ]
  },
  {
    path:'/auth',
    component:()=>import('./views/AuthLayout.vue'),
    children:[
      { 
        path: 'login',
        component: Login
      },
      { 
        path: 'register',
        component: Register
      },
      { 
        path: 'logout',
        component: Logout
      }
    ]
  },

]

const router = new VueRouter({
  // mode: 'history',
  base: process.env.BASE_URL,
  routes,
  scrollBehavior (to, from, savedPosition) {
    return { x: 0, y: 0 }
  }
})

export default router
