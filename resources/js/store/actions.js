// import axios from "../axios";

import axios from 'axios'
const actions = {

    setSelectedPackage({commit},data){
      commit('SET_SELECTED_PACKAGE',data);
    },
    setDate({commit},date){
      commit('SET_DATE',date);
    },
    setFormComponent({commit},value){
      commit('SET_FORM_COMPONENT',value);
    },
    setRuleType({commit},value){
      commit('SET_RULE_TYPE',value);
    },
    setAlert({commit},value){
      commit('SET_ALERT',value);
    },
    setTimeBlock({commit},value){
      commit('SET_TIME_BLOCK',value);
    },
    setActivePackageId({commit},value){
      commit('SET_ACTIVE_PACKAGE_ID',value);
    },
    setCharge({commit},charge){
      commit("SET_CHARGE",charge);
    },
    fetchPackage(context,id){
      return new Promise((resolve, reject) => {
        axios.get('/api/package/get/'+id)
          .then((response) => {
            context.commit('SET_SELECTED_PACKAGE',response.data.data);
            resolve(response.data.data);
          })
          .catch((error) => { reject(error) })
      })
    },
    getTimeAndDistance(context, data){
        // axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');
      return new Promise((resolve, reject) => {
        axios.post('/api/reservation/getTimeAndDistance', data)
          .then((response) => {
            resolve(response.data.data);
          })
          .catch((error) => { reject(error) })
      })
    },    
    getCategories(context, data){
      // axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');
      return new Promise((resolve, reject) => {
        axios.post('/api/reservation/getCategories', data)
          .then((response) => {
            context.commit('ADD_CATEGORIES',response.data.data);
            resolve(response.data.data);
          })
          .catch((error) => { reject(error) })
      })
    },    
    confirmZipCode(context, data){
      // axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');
      return new Promise((resolve, reject) => {
        axios.post('/api/zone/confirmZipCode', data)
          .then((response) => {
            context.commit('ADD_ZONE',response.data.data);
            resolve(response.data.data);
          })
          .catch((error) => { reject(error) })
      })
    },
    getPackages(context, data){
      // axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');
      return new Promise((resolve, reject) => {
        axios.post('/api/package/fetch',data)
          .then((response) => {
            context.commit('ADD_PACKAGES',response.data.data.data);
            resolve(response.data.data.data);
          })
          .catch((error) => { reject(error) })
      })
    },
    fetchTimeBlock(context,data){
      return new Promise((resolve,reject)=>{
        axios.post('/api/units/fetchTimeBlock',data)
        .then((response) => {
          context.commit('ADD_TIME_BLOCK',response.data.data);
          resolve(response.data.data);
        })
        .catch((error) => { reject(error) })
      })
    },
    checkTimeBlockAvalibility(context,data){
      return new Promise((resolve,reject)=>{
        axios.post('/api/units/checkTimeBlockAvalibility',data)
        .then((response) => {
          resolve(response.data);
        })
        .catch((error) => { reject(error) })
      })
    },


    storeCustomerProfile(context,data){
      axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');
      return new Promise((resolve, reject) => {
        axios.post('/api/customer_profile/store',data).then((response)=>{
          context.commit('SET_PROFILE',response.data.data);
          resolve(response.data.data);
        })
        .catch((error)=>{
          reject(error);
        });
      })
    },

    
    storeReservation(context,data){
      axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');
      return new Promise((resolve, reject) => {
        axios.post('/api/reservation/store',data).then((response)=>{
          resolve(response.data.data);
        })
        .catch((error)=>{
          reject(error);
        });
      })
    },

    fetchReservations(context,data){
      axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');
      return new Promise((resolve, reject) => {
        axios.post('/api/reservation/index',data).then((response)=>{
          context.commit('ADD_RESERVATIONS',response.data.data);
          resolve(response.data.data);
        })
        .catch((error)=>{
          reject(error);
        });
      })
    },
    fetchCustomerReservations(context,data){
      axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');
      return new Promise((resolve, reject) => {
        axios.post('/api/reservation/customerReservations').then((response)=>{
          context.commit('ADD_CUSTOMER_RESERVATIONS',response.data.data);
          resolve(response.data.data);
        })
        .catch((error)=>{
          reject(error);
        });
      })
    },
    fetchCustomerReservation(context,data){
      axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');
      return new Promise((resolve, reject) => {
        axios.post('/api/reservation/customerReservation',data).then((response)=>{
          context.commit('ADD_CUSTOMER_RESERVATION',response.data.data);
          resolve(response.data.data);
        })
        .catch((error)=>{
          reject(error);
        });
      })
    },
    getReservation(context,data){
      axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');
      return new Promise((resolve, reject) => {
        axios.post('/api/reservation/getReservation',data).then((response)=>{
          context.commit('ADD_DATE_RESERVATIONS',response.data.data);
          resolve(response.data.data);
        })
        .catch((error)=>{
          reject(error);
        });
      })
    },
    changeStatus(context,data){
      axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');
      context.commit('CHANGE_STATUS',data);
      return new Promise((resolve, reject) => {
        axios.post('/api/reservation/change-status',data).then((response)=>{
          context.commit('ADD_STATUS',{id:data.id,status :response.data.data});
          context.commit('CHANGE_STATUS',data);

          resolve(response.data.data);
        })
        .catch((error)=>{
          reject(error);
        });
      })
    },


    fetchRules(context,data){
      axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');
      return new Promise((resolve, reject) => {
        axios.get('/api/rules/index/'+data.package_id+'/'+data.rule_type).then((response)=>{          
          context.commit('SET_RULES',response.data.data);
          resolve(response.data.data);
        })
        .catch((error)=>{
          reject(error);
        });
      })
    },
    fetchPayments(context){
      axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');
      return new Promise((resolve, reject) => {
        axios.post('/api/reservation/payments').then((response)=>{          
          context.commit('SET_PAYMENTS',response.data.data);
          resolve(response.data.data);
        })
        .catch((error)=>{
          reject(error);
        });
      })
    },

    updateReservation(context,data){
      axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');
      return new Promise((resolve, reject) => {
        axios.post('/api/reservation/update',data).then((response)=>{          
          resolve(response.data.data);
        })
        .catch((error)=>{
          reject(error);
        });
      })
    },





    // AUTHENTICATION


    login(context, data) {
      return new Promise((resolve, reject) => {
        axios.post('/api/login', data)
          .then(response =>{
            const token = response.data.access_token;
            localStorage.setItem('access_token', token);
            context.commit('retrieveToken', token);
            resolve(response);
          })
          .catch(error => {
            reject(error)
          })
        })
    },
    register(context, data) {
      return new Promise((resolve, reject) => {
        axios.post('/api/register', data)
          .then(response =>{
            context.commit('SET_USER',response.data.data);
            resolve(response);
          })
          .catch(error => {
            reject(error)
          })
        })
    },
    logout(context){
      axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');
      context.commit('RESET_USER');
      localStorage.removeItem('access_token');
      context.commit('DESTROY_TOKEN');
      return new Promise((resolve,reject)=>{  
        axios.post('/api/logout')
          .then((response)=>{
            resolve(response);
          }).catch((error)=>{
            reject(error);
          })
      })
    },
    getUser(context){
      axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');
      
      return new Promise((resolve, reject) => {
        if(localStorage.getItem('access_token')){
          axios.post('/api/auth/user').then((response)=>{
            context.commit('SET_USER',response.data.data);
            context.commit('SET_PROFILE',response.data.data.customer_profile);
            // context.commit('set',response.data.data.restaurant);
            resolve(response.data.data);
          })
          .catch((error)=>{
            reject(error);
          });
        }else{
          reject("no token");
        }
      })
    },
    updateProfile(context,data){
      axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');
      
      return new Promise((resolve, reject) => {
        if(localStorage.getItem('access_token')){
          axios.post('/api/auth/updateProfile/'+data.profile_id,data.profile).then((response)=>{
            context.commit('SET_PROFILE',response.data.data);
            resolve(response.data.data);
          })
          .catch((error)=>{
            reject(error);
          });
        }else{
          reject("no token");
        }
      })
    },

    updateUser(context,data){
      axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');
      
      return new Promise((resolve, reject) => {
        if(localStorage.getItem('access_token')){
          axios.post('/api/auth/updateUser/'+data.user_id,data.user).then((response)=>{
            context.commit('SET_USER',response.data.data);
            resolve(response.data.data);
          })
          .catch((error)=>{
            reject(error);
          });
        }else{
          reject("no token");
        }
      })
    },
}

export default actions;