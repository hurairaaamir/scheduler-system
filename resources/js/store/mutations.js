import state from "./state";

const mutations = {
    ADD_CATEGORIES(state,data){
        state.categories=data;
    },
    ADD_ZONE(state,data){
        if(data.pickup_confirm&&data.dropoff_confirm){
            state.zone.dropoff_zone_id=data.dropoff_zone_id;
            state.zone.pickup_zone_id=data.pickup_zone_id;
            state.zoneConfirm=true;
        }
    },
    ADD_PACKAGES(state,data){
        state.packages=data;
        console.log(data);
        data.forEach(element=>{
            console.log(element);
            state.activePackageId=element.id;    
            // return false;
        });
        // console.log('active_id'+state.activePackageId);

    },
    SET_ACTIVE_PACKAGE_ID(state,id){
        state.activePackageId=id
    },
    ADD_TIME_BLOCK(state,time_blocks){
        state.time_blocks=time_blocks;
    },
    SET_TIME_BLOCK(state,time_block){
        state.time_block=time_block;
    },
    SET_SELECTED_PACKAGE(state,pack){
        state.selected_package=pack;
    },
    SET_CHARGE(state,charge){
        state.charge=charge;
    },
    SET_DATE(state,date){
        state.time_block_date=date;
    },
    SET_FORM_COMPONENT(state,value){
        state.form_component=value;
    },
    SET_USER(state,user){
        state.user=user;
    },
    SET_PROFILE(state,profile){
        state.profile=profile;
    },
    ADD_RESERVATIONS(state,reservations){
        state.reservations=reservations;
    },
    CHANGE_STATUS(state,data){
        let index=state.date_reservation.findIndex(r=> r.id == data.id);
        state.date_reservation[index].status=data.status;
    },
    ADD_STATUS(status,data){
        let index=state.date_reservation.findIndex(r=> r.id == data.id);
        state.date_reservation[index].schedule_status.push(data.status);
    },
    RESET_USER(state){
        state.user=""
        console.log('here');
        console.log(state.user);
    },
    DESTROY_TOKEN(state){
        state.token=null;
    },
    SET_RULES(state,rules){
        state.rules=rules;
    },
    ADD_DATE_RESERVATIONS(state,date_reservation){
        state.date_reservation=date_reservation;
    },
    ADD_CUSTOMER_RESERVATIONS(state,reservations){
        state.customer_reservations=reservations;
    },
    ADD_CUSTOMER_RESERVATION(state,reservation){
        state.customer_reservation=reservation;
    },
    SET_PAYMENTS(state,payments){
        state.payments=payments;
    },
    SET_RULE_TYPE(state,type){
        state.rule_type=type;
    },
    SET_ALERT(state,message){
        state.alert.show=true;
        state.alert.message=message;
        setTimeout(() => {
            state.alert.show=false;
        }, 2000);
    }
    
}

export default mutations;