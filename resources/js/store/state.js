
const state ={

    categories:[],
    estimate:{
        distance:'',
        price:''
    },
    zone:{
        pickup_zone_id:'',
        dropoff_zone_id:''
    },
    zoneConfirm:false,
    packages:[],
    selected_package:[],
    time_blocks:[],
    time_block:'',
    time_block_date:"",
    loggedIn:false,
    form_component:"",
    activePackageId:'',
    token:localStorage.getItem('access_token')||null,
    user:'',
    profile:'',
    rules:[],
    rule_type:'delivery',
    reservations:[],
    date_reservation:[],
    customer_reservations:[],
    customer_reservation:[],
    payments:[],
    alert:{
        show:false,
        message:''
    },
    charge:0
}

export default state;