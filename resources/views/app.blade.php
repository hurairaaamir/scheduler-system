<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>seredo</title>

    <link rel="icon" type="image/png" href="/images/general/seredo.png"/>

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@mdi/font@latest/css/materialdesignicons.min.css">    
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <style>
        .animation{
            animation: anim 0.4s ease-in;
        }
        @keyframes anim {
        0%   {
            transform: translateX(100%);
            }
        100% {
            transform: translateX(0%);
            }
        }
        .v-data-table-header tr th{
            min-width: 130px!important;
        }
        .gmnoprint{
            display:none!important;
        }
        .gm-style-cc{
            display:none!important;
        }
    </style>
</head>
<body>
    <div id="app">
        <router-view></router-view>
    </div>
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="https://js.stripe.com/v3/"></script>
</body>
</html>
