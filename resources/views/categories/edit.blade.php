@extends('layouts.master')
@section('title')
    Categories
@endsection
@section('page-head')
    <div id="page-title">
        <h1 class="page-header text-overflow">Categories</h1>
    </div>
    <ol class="breadcrumb">
        <li><a href="#"><i class="demo-pli-home"></i></a></li>

        <li class="active">Categories</li>
    </ol>
@endsection
@section('content')
    <hr class="new-section-sm bord-no">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title">Edit Categories</h3>
                </div>
                <form method="post" action="{{url('admin/categories/edit' , $category->id)}}" enctype="multipart/form-data">
                    @csrf
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Name</label>
                                    <input value="{{$category->name}}" type="text" name="name" class="form-control">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Icon</label>
                                    <input  type="file" name="icon" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Descrption</label>
                                    <textarea name="description" class="form-control">{{$category->description}}</textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <img src="{{$category->icon}}" width="30%">
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer text-right">
                        <button class="btn btn-success" type="submit">Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection