@extends('layouts.master')
@section('title')
    Customers
@endsection
@section('page-head')
    <div id="page-title">
        <h1 class="page-header text-overflow">Customers</h1>
    </div>
    <ol class="breadcrumb">
        <li><a href="#"><i class="demo-pli-home"></i></a></li>

        <li class="active">Customers</li>
    </ol>
@endsection
@section('content')
    <hr class="new-section-sm bord-no">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title">All Customers</h3>
                </div>
                <div class="panel-body">
                    <table id="luggers-table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th class="min-tablet">Email</th>
                            <th class="min-tablet">Phone #</th>
                            <th class="min-desktop">Mailing Address</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($customers as $value)
                        <tr>
                            <td>{{$value->name}}</td>
                            <td>{{$value->email}}</td>
                            <td>{{$value->customer_profile->phone_number??""}}</td>
                            <td>{{$value->customer_profile->mailing_address??""}}</td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection