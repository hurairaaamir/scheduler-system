@extends('layouts.master')
@section('title')
    Dashboard
@endsection
@section('page-head')
    <div id="page-title">
        <h1 class="page-header text-overflow">Dashboard</h1>
    </div>
    <ol class="breadcrumb">
        <li><a href="#"><i class="demo-pli-home"></i></a></li>

        <li class="active">Dashboard</li>
    </ol>
@endsection
@section('content')
    <hr class="new-section-sm bord-no">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-body text-center">
                <div class="panel-heading">
                    <h3>Your content here...</h3>
                </div>
                <div class="panel-body">
                    <p>Start putting content on grids or panels, you can also use different combinations of grids.
                        <br>Please check out the dashboard and other pages.</p>
                </div>
            </div>
        </div>
    </div>
@endsection