@extends('layouts.master')
@section('title')
Drivers
@endsection
@section('page-head')
    <div id="page-title">
        <h1 class="page-header text-overflow">Drivers</h1>
    </div>
    <ol class="breadcrumb">
        <li><a href="#"><i class="demo-pli-home"></i></a></li>
        <li class="active">Drivers</li>
    </ol>
@endsection
@section('content')
    <hr class="new-section-sm bord-no">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title">Add Drivers</h3>
                </div>
                <form method="post" action="{{url('admin/drivers/store')}}" enctype="multipart/form-data">
                    @csrf
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label" for='name'>Driver Name</label>
                                    <input type="text" name="name" class="form-control" id="name" value="{{old('name')}}">
                                    @if($errors->has('name'))
                                        <p style="color:red">
                                            {{ $errors->first('name') }}
                                        </p>
                                    @endif
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label" for="email">Driver Email</label>
                                    <input name="email" id="email" class="form-control" type="text" value="{{old('email')}}">
                                    @if($errors->has('email'))
                                        <p style="color:red">
                                            {{ $errors->first('email') }}
                                        </p>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label" for="password">Password</label>
                                    <input type="password" name="password" class="form-control" id="password" >
                                    @if($errors->has('password'))
                                        <p style="color:red">
                                            {{ $errors->first('password') }}
                                        </p>
                                    @endif
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label" for="password_confirmation">Password Confirmation</label>
                                    <input type="password" name="password_confirmation" class="form-control" id="password_confirmation" >
                                    @if($errors->has('password_confirmation'))
                                        <p style="color:red">
                                            {{ $errors->first('password_confirmation') }}
                                        </p>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label" for="status">Status</label>
                                    <select name="status" class="form-control" id="status" >
                                        <option value="active">active</option>
                                        <option value="inactive">inactive</option>
                                    </select>
                                    @if($errors->has('status'))
                                        <p style="color:red">
                                            {{ $errors->first('status') }}
                                        </p>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <button class="btn btn-success" type="submit">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection