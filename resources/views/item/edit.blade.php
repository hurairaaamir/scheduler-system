@extends('layouts.master')
@section('title')
    Package Item
@endsection
@section('page-head')
    <div id="page-title">
        <h1 class="page-header text-overflow">Package Item</h1>
    </div>
    <ol class="breadcrumb">
        <li><a href="#"><i class="demo-pli-home"></i></a></li>

        <li class="active">Package Item</li>
    </ol>
@endsection
@section('content')
    <hr class="new-section-sm bord-no">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title">Edit Package Item</h3>
                </div>
                <form method="post" action="{{url('admin/item/edit' , $item->id)}}" enctype="multipart/form-data">
                    @csrf
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Name</label>
                                    <input type="text" value="{{$item->name}}" name="name" class="form-control">
                                </div>
                            </div>
                        </div>
                        {{-- <div class="col-sm-6">
                            <div class="form-group">
                                <label class="control-label" value="{{$item->value}}">Value</label>
                                <input type="text" name="value" class="form-control">
                            </div>
                        </div> --}}
                    </div>
                    <div class="panel-footer text-right">
                        <button class="btn btn-success" type="submit">Edit Item</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection