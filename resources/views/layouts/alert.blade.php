@if(\Session::has('success'))
    <div class="alert alert-success">
      <strong><i class="fa fa-check-circle"></i> Great!</strong>  {{ \Session::get('success') }}
    </div>
@endif
@if(\Session::has('info'))
    <div class="alert alert-info">
        <strong><i class="fa fa-times"></i> Alert!</strong> {{ \Session::get('info') }}
    </div>
@endif