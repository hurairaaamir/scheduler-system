<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="icon" type="image/png" href="/images/general/seredo.png"/>

<title>@yield('title') | Seredo</title>
{{--<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>--}}
<link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">
<link href="{{asset('assets/css/nifty.min.css')}}" rel="stylesheet">
<link href="{{asset('assets/css/demo/nifty-demo-icons.min.css')}}" rel="stylesheet">
<link href="{{asset('assets/plugins/datatables/media/css/dataTables.bootstrap.css')}}" rel="stylesheet">
<link href="{{asset('assets/plugins/datatables/extensions/Responsive/css/responsive.dataTables.min.css')}}" rel="stylesheet">
<link href="{{asset('assets/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
<link href="{{asset('assets/plugins/pace/pace.min.css')}}" rel="stylesheet">
<script src="{{asset('assets/plugins/pace/pace.min.js')}}"></script>
<link href="{{asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css')}}" rel="stylesheet">
<link href="{{asset('assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.css')}}" rel="stylesheet">
<link href="{{asset('assets/plugins/select2/css/select2.min.css')}}" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>
@yield('style')
<style>
    .is-invalid{
        box-shadow: 0px 0px 8px rgb(250, 94, 94)!important;
    }
    .center_out{
        display: flex;
        justify-content: center;
        align-items: center;
        flex-direction: column;
    }
</style>

