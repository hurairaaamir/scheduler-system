<script src="{{asset('assets/js/jquery.min.js')}}"></script>
<script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/js/nifty.min.js')}}"></script>
<script src="{{asset('assets/plugins/datatables/media/js/jquery.dataTables.js')}}"></script>
<script src="{{asset('assets/plugins/datatables/media/js/dataTables.bootstrap.js')}}"></script>
<script src="{{asset('assets/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
<script src="{{asset('assets/custom-js/data-tables.js')}}"></script>
<script src="{{asset('assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js')}}"></script>
<script src="{{asset('assets/plugins/select2/js/select2.min.js')}}"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB8Ew5yi_m4X9Xrj0SSZMI2VhM8N8lYmcw&libraries=places"></script>


@yield('scripts')
<script>

    $('.input-group.date').datepicker({autoclose:true});
    $("#days").select2();
</script>

