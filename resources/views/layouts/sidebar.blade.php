<nav id="mainnav-container">
    <div id="mainnav">
        <div id="mainnav-menu-wrap">
            <div class="nano">
                <div class="nano-content">
                    <div id="mainnav-profile" class="mainnav-profile">
                        <div class="profile-wrap text-center">
                            <div class="pad-btm">
                                <img class="img-circle img-md" src="{{asset('assets/img/profile-photos/1.png')}}" alt="Profile Picture">
                            </div>
                                <p class="mnp-name">{{Auth::user()->name}}</p>
                                <span class="mnp-desc">{{Auth::user()->email}}</span>
                        </div>
                    </div>
                    <div id="mainnav-shortcut" class="hidden">
                        <ul class="list-unstyled shortcut-wrap">
                            <li class="col-xs-3" data-content="My Profile">
                                <a class="shortcut-grid" href="#">
                                    <div class="icon-wrap icon-wrap-sm icon-circle bg-mint">
                                        <i class="demo-pli-male"></i>
                                    </div>
                                </a>
                            </li>
                            <li class="col-xs-3" data-content="Messages">
                                <a class="shortcut-grid" href="#">
                                    <div class="icon-wrap icon-wrap-sm icon-circle bg-warning">
                                        <i class="demo-pli-speech-bubble-3"></i>
                                    </div>
                                </a>
                            </li>
                            <li class="col-xs-3" data-content="Activity">
                                <a class="shortcut-grid" href="#">
                                    <div class="icon-wrap icon-wrap-sm icon-circle bg-success">
                                        <i class="demo-pli-thunder"></i>
                                    </div>
                                </a>
                            </li>
                            <li class="col-xs-3" data-content="Lock Screen">
                                <a class="shortcut-grid" href="#">
                                    <div class="icon-wrap icon-wrap-sm icon-circle bg-purple">
                                        <i class="demo-pli-lock-2"></i>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <ul id="mainnav-menu" class="list-group">
                        <li class="{{Request::is('admin') ? 'active-sub': ''}}">
                            <a href="{{url('admin')}}">
                                <i class="demo-pli-home"></i>
                                <span class="menu-title">Dashboard</span>
                            </a>
                        </li>
                        <li class="list-divider"></li>

                        <li class="{{Request::is('admin/customers') ? 'active-sub': ''}}">
                            <a href="{{url('admin/customers')}}">
                                <i class="demo-pli-checked-user"></i>
                                <span class="menu-title">Customers</span>
                            </a>
                        </li>
                        <li class="{{Request::is('admin/categories') || Request::is('admin/categories/*') ? 'active-sub': ''}}">
                            <a href="{{url('admin/categories')}}">
                                <i class="fa fa-list"></i>
                                <span class="menu-title">Categories</span>
                            </a>
                        </li>

                        <li class="{{Request::is('admin/zone') || Request::is('admin/zone/*') ? 'active-sub': ''}}">
                            <a href="{{url('admin/zone')}}">
                                <i class="fa fa-map-marker"></i>
                                <span class="menu-title">Zone</span>
                            </a>
                        </li>
                        <li class="{{Request::is('admin/rule') || Request::is('admin/rule/*') ? 'active-link': ''}}">
                            <a href="{{url('admin/rule')}}">
                                <i class="fa fa-gavel"></i>
                                <span class="menu-title">Rules</span>
                            </a>
                        </li>
                        @php  
                            $expend=Request::is('admin/package') || Request::is('admin/package/*') ||Request::is('admin/item') || Request::is('admin/item/*') ? 'true': 'false';
                            $class= Request::is('admin/package') || Request::is('admin/package/*') ||Request::is('admin/item') || Request::is('admin/item/*') ? 'active active-sub': '';   
                        @endphp
                        <li class="{{$class}}">
                            <a href="#" aria-expanded="{{$expend}}">
                                <i class="fa fa-folder-open"></i>
                                <span class="menu-title">Packages</span>
                                <i class="arrow"></i>
                            </a>
                            <ul class="collapse" aria-expanded="{{$expend}}" >
                                <li class="{{Request::is('admin/package') || Request::is('admin/package/*') ? 'active-link': ''}}">
                                    <a href="{{url('admin/package')}}">
                                        <span class="menu-title">Packages</span>
                                    </a>
                                </li>
                                
                                <li class="{{Request::is('admin/item')||Request::is('admin/item/*') ? 'active-link': ''}}">
                                    <a href="{{url('admin/item')}}">
                                        <span class="menu-title">Package Items</span>
                                    </a>
                                </li>                            
                            </ul>
                        </li>
                        

                        

                        

                        <li class="{{Request::is('admin/office-location') || Request::is('admin/office-location/*') ? 'active-sub': ''}}">
                            <a href="{{url('admin/office-location')}}">
                                <i class="fa fa-map-marker"></i>
                                <span class="menu-title">Office Location</span>
                            </a>
                        </li>
                        <li class="{{Request::is('admin/unit-truck') || Request::is('admin/unit-truck/*') ? 'active-sub': ''}}">
                            <a href="{{url('admin/unit-truck')}}">
                                <i class="fa fa-truck"></i>
                                <span class="menu-title">Unit Trucks</span>
                            </a>
                        </li>
                        <li class="{{Request::is('admin/schedule') || Request::is('admin/schedule/*') ? 'active-sub': ''}}">
                            <a href="{{url('admin/schedule')}}">
                                <i class="fa fa-file"></i>
                                <span class="menu-title">Schedules</span>
                            </a>
                        </li>
                        <li class="{{Request::is('admin/drivers') || Request::is('admin/drivers/*') ? 'active-sub': ''}}">
                            <a href="{{url('admin/drivers')}}">
                                <i class="fa fa-users"></i>
                                <span class="menu-title">Drivers</span>
                            </a>
                        </li>
                        <li class="{{Request::is('admin/reservation') || Request::is('admin/reservation/*') ? 'active-link': ''}}">
                            <a href="{{url('admin/reservation')}}">
                                <i class="fa fa-server"></i>
                                <span class="menu-title">Reservations</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</nav>



{{-- @php  
                            $expend=Request::is('rule') || Request::is('rule/*') ||Request::is('rule-type')||Request::is('rule-type/*') ? 'true': 'false';
                            $class= Request::is('rule') || Request::is('rule/*') ||Request::is('rule-type')||Request::is('rule-type/*') ? 'active active-sub': '';   
                        @endphp
                        <li class="{{$class}}">
                            <a href="#" aria-expanded="{{$expend}}">
                                <i class="fa fa-gavel"></i>
                                <span class="menu-title">Rules</span>
                                <i class="arrow"></i>
                            </a>
                            <ul class="collapse" aria-expanded="{{$expend}}" >
                                <li class="{{Request::is('rule') || Request::is('rule/*') ? 'active-link': ''}}">
                                    <a href="{{url('rule')}}">
                                        <span class="menu-title">Rules</span>
                                    </a>
                                </li>
                                
                                <li class="{{Request::is('rule-type')||Request::is('rule-type/*') ? 'active-link': ''}}">
                                    <a href="{{url('rule-type')}}">
                                        <span class="menu-title">Rule Type</span>
                                    </a>
                                </li>                            
                            </ul>
                        </li> --}}