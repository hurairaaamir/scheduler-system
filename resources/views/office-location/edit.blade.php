@extends('layouts.master')
@section('title')
    Offices Locations
@endsection
@section('page-head')
    <div id="page-title">
        <h1 class="page-header text-overflow">Offices Locations</h1>
    </div>
    <ol class="breadcrumb">
        <li><a href="#"><i class="demo-pli-home"></i></a></li>

        <li class="active">Offices Locations</li>
    </ol>
@endsection
@section('content')
    <hr class="new-section-sm bord-no">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title">Edit Offices Locations</h3>
                </div>
                <form  method="post" action="{{url('admin/office-location/update',$data->id)}}" enctype="multipart/form-data" id="OfficeForm">
                    @csrf
                    <input type="hidden" name="address" id="address" value="{{$data->address}}">
                    <input type="hidden" name="lat" id="office_lat" value="{{$data->lat}}">
                    <input type="hidden" name="lng" id="office_lng" value="{{$data->lng}}">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label" for='name'>Name</label>
                                    <input type="text" name="name" class="form-control {{$errors->first('name')? 'is-invalid':''}}" value="{{$data->name}}" id="name" placeholder="name">
                                    @if($errors->has('name'))
                                        <p style="color:red">
                                            {{ $errors->first('name') }}
                                        </p>
                                    @endif
                                </div>
                            </div>
                            @php
                                $selected_ids=[];
                            @endphp
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Zones</label>
                                    <select class="form-control zone_selector" name="zones[]" multiple="multiple">
                                        <option value="">Select Zones</option>
                                        @foreach($data->zones as $zone)
                                            <option value="{{$zone->id}}" selected>{{$zone->name}}</option>
                                            @php
                                                array_push($selected_ids,$zone->id);   
                                            @endphp
                                        @endforeach
                                        @foreach($zones as $zone)
                                                @if(!in_array($zone->id,$selected_ids))
                                                    <option value="{{$zone->id}}">{{$zone->name}}</option>
                                                @endif
                                        @endforeach
                                    </select>
                                    @if($errors->has('zones'))
                                        <p style="color:red">
                                            {{ $errors->first('zone') }}
                                        </p>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="control-label" for="officeLocation-input">Address</label>
                                <input type="text" name="address" class="form-control {{$errors->first('address') ? 'is-invalid':''}}" id="officeLocation-input" value="{{$data->address}}">
                            </div>
                            @if($errors->has('address'))
                                <p style="color:red">
                                    {{ $errors->first('address') }}
                                </p>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <button class="btn btn-success" onclick="document.getElementById('OfficeForm').submit()" type="submit">Submit</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAhN31rb1mb4ejPcZiAqqjtrjOly3l960g&libraries=places"></script>
<script>
    $(document).ready(function() {
        $('.zone_selector').select2({
            placeholder: 'Select Zones',
        });
    });
    const officeInput = document.getElementById('officeLocation-input');
    const officeLat= document.getElementById('office_lat');
    const officeLng= document.getElementById('office_lng');
    const address=document.getElementById('address');
    
    const officeBox = new google.maps.places.SearchBox(officeInput);
    officeBox.addListener('places_changed', () => {
      const places = officeBox.getPlaces();
      address.value=places[0].formatted_address;
      officeLng.value=places[0].geometry.location.lng();
      officeLat.value=places[0].geometry.location.lat();
    });
    geometry.location.lat();
</script>


@endsection