@extends('layouts.master')
@section('title')
    Package
@endsection
@section('page-head')
    <div id="page-title">
        <h1 class="page-header text-overflow">Package</h1>
    </div>
    <ol class="breadcrumb">
        <li><a href="#"><i class="demo-pli-home"></i></a></li>

        <li class="active">Package</li>
    </ol>
@endsection
@section('content')
    <hr class="new-section-sm bord-no">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title">Add Package</h3>
                </div>
                <form id="package-form" method="post" action="{{url('admin/package/edit',$package->id)}}" class="form-horizontal" enctype="multipart/form-data">
                    @csrf
                    <div class="panel-body">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Package Name</label>
                            <div class="col-sm-6">
                                <input type="text" name="name" placeholder="Package Name" class="form-control" value="{{$package->name}}">
                                @if($errors->has('name'))
                                    <p style="color:red">
                                        {{ $errors->first('name') }}
                                    </p>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="demo-hor-inputpass">Category</label>
                            <div class="col-sm-6">
                                <select class="form-control" name="category">

                                    <option value="">Select Category</option>
                                    @foreach($categories as $value)
                                        <option value="{{$value->id}}" {{($package->category->name==$value->name)?'selected':''}}>{{$value->name}}</option>
                                    @endforeach
                                </select>
                                @if($errors->has('category'))
                                    <p style="color:red">
                                        {{ $errors->first('category') }}
                                    </p>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="demo-hor-inputpass">Image</label>
                            <div class="col-sm-6">
                                <input type="file" class="form-control" name="image">
                                @if($errors->has('image'))
                                    <p style="color:red">
                                        {{ $errors->first('image') }}
                                    </p>
                                @endif
                            </div>
                        </div>
                        @php
                            $selected_ids=[];
                        @endphp
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Rules</label>
                            <div class="col-sm-6">
                                <select class="form-control rules_selector" name="rules[]" multiple="multiple">
                                    <option value="">Select Rule</option>
                                    @foreach($package->rules as $rule)
                                        <option value="{{$rule->id}}" selected>{{$rule->name}} --rule type {{$rule->rule_type}}</option>
                                        @php
                                            array_push($selected_ids,$rule->id);   
                                        @endphp
                                    @endforeach
                                    @foreach($rules as $rule)
                                            @if(!in_array($rule->id,$selected_ids))
                                                <option value="{{$rule->id}}">{{$rule->name}} --rule type {{$rule->rule_type}}</option>
                                            @endif
                                    @endforeach
                                </select>
                                @if($errors->has('rule'))
                                    <p style="color:red">
                                        {{ $errors->first('rule') }}
                                    </p>
                                @endif
                            </div>
                        </div>
                        {{-- <br>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Zones</label>
                            <div class="col-sm-6">
                                <select class="form-control rules_selector" name="zones[]" multiple="multiple">
                                    <option value="">Select Zones</option>
                                    @foreach($package->zones as $zone)
                                        <option value="{{$zone->id}}" selected>{{$zone->name}}</option>
                                        @php
                                            array_push($selected_ids,$zone->id);   
                                        @endphp
                                    @endforeach
                                    @foreach($zones as $zone)
                                            @if(!in_array($zone->id,$selected_ids))
                                                <option value="{{$zone->id}}">{{$zone->name}}</option>
                                            @endif
                                    @endforeach
                                </select>
                                @if($errors->has('zone'))
                                    <p style="color:red">
                                        {{ $errors->first('zone') }}
                                    </p>
                                @endif
                            </div>
                        </div> --}}
                        <br>
                        <div class="form-group">
                            <div class=" col-sm-2 col-sm-offset-2">
                                <label for="deposit">Deposits</label>
                            </div>
                            <div class="col-sm-3">
                            <input type="text" min="1" id="deposit"  name="deposit" class="form-control" placeholder="Enter Value" value="{{$package->deposit}}">
                            @if($errors->has('deposit'))
                                <p style="color:red">
                                    {{ $errors->first('deposit') }}
                                </p>
                            @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-2 col-sm-offset-2">
                                <label for="distance_charge">Distance Charge</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" min="1" id="distance_charge"  name="distance_charge" class="form-control" placeholder="Enter Value" value="{{$package->distance_charge}}">
                                @if($errors->has('distance_charge'))
                                    <p style="color:red">
                                        {{ $errors->first('distance_charge') }}
                                    </p>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class=" col-sm-2 col-sm-offset-2">
                                <label for="origin_fee">Origin Fee</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" min="1" id="origin_fee"  name="origin_fee" value="{{$package->origin_fee}}" class="form-control" placeholder="Enter Value">
                            </div>
                            <div class="col-sm-2 ">
                                <input type="text" min="1" id="origin_fee_limit"  name="origin_fee_limit" value="{{$package->origin_fee_limit}}" class="form-control" placeholder="Limit in Miles">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-2 col-sm-offset-2">
                                <label for="return_fee">Long Distance Return Fee</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" min="1" id="return_fee" value="{{$package->return_fee}}"  name="return_fee" class="form-control" placeholder="Enter Value">
                            </div>
                            <div class="col-sm-2 ">
                                <input type="text" min="1" id="return_fee_limit" value="{{$package->return_fee_limit}}"  name="return_fee_limit" class="form-control" placeholder="Limit in Miles">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-2 col-sm-offset-2">
                                <label for="moving_charge">Moving items Base Charge</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" min="1" id="moving_charge" value="{{$package->moving_charge}}"  name="moving_charge" class="form-control" placeholder="Enter Value">
                            </div>
                            <div class="col-sm-2 ">
                                <input type="text" min="1" id="moving_item" value="{{$package->moving_item}}"  name="moving_item" class="form-control" placeholder="No of Max Moving Item">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-2 col-sm-offset-2">
                                <label for="service_time">Padding Time</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" min="1" id="service_time" value="{{$package->service_time}}" name="service_time" class="form-control" placeholder="Enter time in minutes">
                            </div>
                        </div>
                        <h6>Extras</h6>

                        @php
                            $selected_ids=[];
                        @endphp

                        @foreach($items as $key => $value)
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-2">
                                    @foreach($package->items as $item)
                                        @if($value->id==$item->id)
                                            @php                                                
                                                array_push($selected_ids,$value->id);
                                            @endphp
                                            <input name="item_ids[]" onchange="selectItem({{$value}} , this , {{$key}})" id="{{$value->name}}" class="magic-checkbox" type="checkbox" value="{{$item->id}}" checked>
                                            <label for="{{$value->name}}">{{$value->name}}</label>
                                        @endif
                                    @endforeach
                                </div>
                                <div class="col-sm-3">
                                    @foreach($package->items as $item)
                                        @if($value->id==$item->id)
                                            <input type="text" id="{{$value->id}}" oninput="inpupValue({{$value}} , this , {{$key}})" name="value[]" class="form-control" placeholder="Enter Value" value="{{$item->pivot->value}}">
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        @endforeach
                        @foreach($items as $key => $value)
                            @if(!in_array($value->id,$selected_ids))
                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-2">
                                        <input name="item_ids[]" onchange="selectItem({{$value}} , this , {{$key}})" id="{{$value->name}}" class="magic-checkbox" type="checkbox" value="{{$value->id}}">
                                        <label for="{{$value->name}}">{{$value->name}}</label>
                                    </div>
                                    <div class="col-sm-3">
                                        <input type="text" id="{{$value->id}}" oninput="inpupValue({{$value}} , this , {{$key}})" name="value[]" class="form-control" placeholder="Enter Value">
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    </div>
                    <div class="panel-footer text-right">
                        <button class="btn btn-success" type="submit">Add Package</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

    <script type="text/javascript">
        $(document).ready(function() {
            $('.rules_selector').select2({
                placeholder: 'Select Rules',
            });
            $('.zone_selector').select2({
                placeholder: 'Select Zones',
            });
        });

    </script>
    <script type="text/javascript">
        var items = [];
    
    
        function selectItem(value , event , i) {
    
            if (event.checked){
                var a = items.find(function (element) {
                    if (element.index == i){
                        return true;
                    } else {
                        return false;
                    }
                });
    
    
                if (a){
                    a.id = value.id;
                } else {
                    const obj = {
                        id: value.id,
                        index: i
                    };
                    items.push(obj);
                }
            console.log(items);
            } else {
                return false;
            }
        }
    
        function inpupValue(value , event , i) {
            if (event.checked){
                var a = items.find(function (element) {
                    if (element.index == i){
                        return true;
                    } else {
                        return false;
                    }
                });
                if (a){
                    a.value = event.value;
                }
                else {
                    const obj = {
                        index: i,
                        value: event.value
                    };
                    items.push(obj);
                }
                console.log(items);
            }else{
                return false;
            }
    //            items.forEach(function(element) {
    //                console.log(element);
    //            });
    
    //            var a = items.find(i);
    //            console.log(a);
        }
    </script>
@endsection