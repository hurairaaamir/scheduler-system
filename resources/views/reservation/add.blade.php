@extends('layouts.master')
@section('title')
Reservation
@endsection
@section('page-head')
    <div id="page-title">
        <h1 class="page-header text-overflow">Reservation</h1>
    </div>
    <ol class="breadcrumb">
        <li><a href="#"><i class="demo-pli-home"></i></a></li>
        <li class="active">Reservation</li>
    </ol>
@endsection
@section('content')
    <hr class="new-section-sm bord-no">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title">Add Reservation</h3>
                </div>
                <form method="post" action="{{url('admin/reservation/store')}}" enctype="multipart/form-data">
                    @csrf
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label" for='name'>Customer Name</label>
                                    <input type="text" name="name" class="form-control" id="name" value="{{old('name')}}">
                                    @if($errors->has('name'))
                                        <p style="color:red">
                                            {{ $errors->first('name') }}
                                        </p>
                                    @endif
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label" for="email">Customer Email</label>
                                    <input name="email" id="email" class="form-control" type="text" value="{{old('email')}}">
                                    @if($errors->has('email'))
                                        <p style="color:red">
                                            {{ $errors->first('email') }}
                                        </p>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label" for="password">Password</label>
                                    <input type="password" name="password" class="form-control" id="password" >
                                    @if($errors->has('password'))
                                        <p style="color:red">
                                            {{ $errors->first('password') }}
                                        </p>
                                    @endif
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label" for="password_confirmation">Password Confirmation</label>
                                    <input type="password" name="password_confirmation" class="form-control" id="password_confirmation" >
                                    @if($errors->has('password_confirmation'))
                                        <p style="color:red">
                                            {{ $errors->first('password_confirmation') }}
                                        </p>
                                    @endif
                                </div>  
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label" for="phone_number">Phone Number</label>
                                    <input type="number" min="1" name="phone_number" class="form-control" id="phone_number" value="{{old('phone_number')}}">
                                    @if($errors->has('phone_number'))
                                        <p style="color:red">
                                            {{ $errors->first('phone_number') }}
                                        </p>
                                    @endif
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label" for="package">Select Package</label>
                                    <select name="package" class="form-control" id="package" value="{{old('package')}}">
                                        @foreach($packages as $package)
                                          <option value="{{$package->id}}">{{$package->name}}</option>
                                        @endforeach
                                    </select>
                                    @if($errors->has('package'))
                                        <p style="color:red">
                                            {{ $errors->first('package') }}
                                        </p>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label" for="bulky_items">Bulky Items</label>
                                    <input type="number" min="1" name="bulky_items" class="form-control" id="bulky_items" value="{{old('bulky_items')}}">
                                    @if($errors->has('bulky_items'))
                                        <p style="color:red">
                                            {{ $errors->first('bulky_items') }}
                                        </p>
                                    @endif
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label" for="unit">Select Unit (or auto-select)</label>
                                    <select name="unit" class="form-control" id="unit" value="{{old('unit')}}">
                                        @foreach($trucks as $truck)
                                          <option value="{{$truck->id}}">{{$truck->name}}</option>
                                        @endforeach
                                    </select>
                                    @if($errors->has('unit'))
                                        <p style="color:red">
                                            {{ $errors->first('unit') }}
                                        </p>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label" for="note">Note</label>
                                    <textarea name="note" class="form-control" id="note" value="{{old('note')}}" rows="5">{{old('note')}}</textarea>
                                    @if($errors->has('note'))
                                        <p style="color:red">
                                            {{ $errors->first('note') }}
                                        </p>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <h4>PickUP Schedule</h4>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label" for="pickup_address">Pickup Address</label>
                                    <input type="text" name="pickup_address" class="form-control" id="pickup_address" value="{{old('pickup_address')}}">
                                    @if($errors->has('pickup_address'))
                                        <p style="color:red">
                                            {{ $errors->first('pickup_address') }}
                                        </p>
                                    @endif
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label" for="pickup_time_block">PickUp Time Block</label>
                                    <select name="pickup_time_block" class="form-control" id="pickup_time_block" value="{{old('pickup_time_block')}}">
                                        <option value="6AM-9AM">6AM-9AM</option>
                                        <option value="9AM-12PM">9AM-12PM</option>
                                        <option value="3PM-6PM">3PM-6PM</option>
                                    </select>
                                    @if($errors->has('pickup_time_block'))
                                        <p style="color:red">
                                            {{ $errors->first('pickup_time_block') }}
                                        </p>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <H4>DropOff Schedule</h4>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label" for="Dropoff_address">Dropoff Address</label>
                                    <input type="text" name="Dropoff_address" class="form-control" id="Dropoff_address" value="{{old('Dropoff_address')}}">
                                    @if($errors->has('Dropoff_address'))
                                        <p style="color:red">
                                            {{ $errors->first('Dropoff_address') }}
                                        </p>
                                    @endif
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label" for="Dropoff_time_block">Dropoff Time Block</label>
                                    <select name="Dropoff_time_block" class="form-control" id="Dropoff_time_block" value="{{old('Dropoff_time_block')}}">
                                        <option value="6AM-9AM">6AM-9AM</option>
                                        <option value="9AM-12PM">9AM-12PM</option>
                                        <option value="3PM-6PM">3PM-6PM</option>
                                    </select>
                                    @if($errors->has('Dropoff_time_block'))
                                        <p style="color:red">
                                            {{ $errors->first('Dropoff_time_block') }}
                                        </p>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <button class="btn btn-success" type="submit">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection