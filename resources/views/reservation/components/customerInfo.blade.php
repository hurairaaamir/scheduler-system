
<div class="panel">
    <div class="panel-heading">
        <h4 class="panel-title" id="CustomerModalLabel"><img src="/images/general/customer_info.svg" style="width:20px;" alt=""> Customer Info</h4>
    </div>
    <div class="panel-body">
        <form onsubmit="submitChanges(event)" class="modal-body center_out" id="customer-form">
            <div class="info-tab">
                <span >Name</span>
                <span class="tab edit-div" id="name_id">{{$reservation->user->name}}</span>
                <input type="text" class="off edit-div info-input validate" name="name" placeholder="name" onchange="onUserFormChange(event)" value="{{$reservation->user->name}}">
                <span class="name_error off" style="color:red"></span>
            </div>
            <div class="info-tab">
                <span>Email</span>
                <span class="tab edit-div" id="email_id">{{$reservation->user->email}}</span>
                <input type="text" class="off edit-div info-input validate" name="email" placeholder="email" onchange="onUserFormChange(event)" value="{{$reservation->user->email}}">
                <span class="email_error off" style="color:red"></span>
            </div>
            <div class="info-tab">
                <span>Phone Number</span>
                <span class="tab edit-div" id="phone_number_id">{{$reservation->user->customer_profile->phone_number}}</span>
                <input type="text" class="off edit-div info-input validate" name="phone_number" placeholder="phone number" onchange="onUserFormChange(event)" value="{{$reservation->user->customer_profile->phone_number}}">
                <span class="phone_number_error off" style="color:red"></span>
            </div>
            <div class="info-tab off edit-div">
                <span>Password</span>
                <input type="password" class="off edit-div info-input validate" name="password" placeholder="password" onchange="onUserFormChange(event)">
            </div>
            <div class="info-tab off edit-div">
                <span>Confirm Password</span>
                <input type="password" class="info-input validate" name="confirm_password" placeholder="confirm password" onchange="onUserFormChange(event)">
            </div>
            <div class="info-tab off edit-div">
                <div></div>
                <div class="password_error off" style="color:red"></div>
            </div>
        </form>
        
        <div class="modal-footer">
            <button class="btn btn-sm btn-primary off customer-btns" onclick="submitChanges(event,{{$reservation->user->id}})">save changes</button>   
            <button class="btn btn-sm btn-default off customer-btns" onclick="edit()">cancel</button>   
            <button class="btn btn-sm btn-primary customer-btns" onclick="edit()">Edit Account</button>
        </div>
    </div>
</div>

            
            







            {{-- <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title">Pickup Schedule</h3>
                </div>
                <div class="panel-body">
                </div>
            </div> --}}