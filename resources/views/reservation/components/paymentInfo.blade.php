<div class="panel">
    <div class="panel-heading">
        <h4 class="panel-title" id="paymentModalLabel"><img src="/images/general/payment.svg" style="width:20px" alt=""> Payment Info</h4>
    </div>
    <div class="panel-body">
        <div class="modal-body center_out">
            <div class="info-tab">
                <span >Stripe Id</span>
                <span class="tab">{{$reservation->payment->stripe_id}}</span>
            </div>
            <div class="info-tab">
                <span>Amount</span>
                <span class="tab">{{$reservation->payment->amount}}</span>
            </div>
        </div>
    </div>
</div>