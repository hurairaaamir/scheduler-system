<div class="panel">
    <div class="panel-heading">
        <h3 class="panel-title"><img src="/images/general/loading_truck.svg" style="width:20px" alt=""> Pickup Schedule
            
            <button class="btn btn-sm btn-primary" style="float:right;margin-top:5px;" onclick="loginAsCustomer({{$reservation->id}},{{$reservation->user->id}})">
                Schedule Pickup 
            </button>
        </h3>
        @php
            $reservation_sechedule=$reservation->reservation_schedule->where('type','pickup')->first();
        @endphp
    </div>
    <div class="panel-body">
        <div class="info-tab">
            <span >Pickup Address</span>
            <span class="tab">{{$reservation_sechedule->address??''}}</span>
        </div>
        <div class="info-tab">
            <span>Pickup Date</span>
            <span class="tab">{{$reservation_sechedule->date??""}}</span>
        </div>
        <div class="info-tab">
            <span>Pickup Time Block</span>
            <span class="tab">{{$reservation_sechedule->time_block??""}}</span>
        </div>
        <div class="info-tab">
            <span>Unit</span>
            <span class="tab">{{$reservation_sechedule->unit_truck->name??""}}</span>
        </div>
        <form onsubmit="scheduleStatusSumit(event)" style="margin-top:20px;display:flex;flex-direction:column;" method="POST">
            <div class="row">
                <input type="hidden" value="pickup" name="type">
                <input type="hidden" value="{{$reservation_sechedule->id??''}}" name="reservation_schedule_id">
                <div class="col-md-offset-1 col-md-6">
                    <h4>Notes</h4>
                    <textarea name="" id="" class="form-control" cols="30" rows="10"></textarea>
                </div>
                <div class="col-md-4">
                    <h4>Status</h4>
                    <select name="status" class="form-control" style="width:100%;">
                        <option value="pending">pending</option>
                        <option value="onRoute">on Route</option>
                        <option value="schedule">Schedule</option>
                        <option value="arrived">Arrived</option>
                        <option value="issue">Issue</option>
                        <option value="canceled">canceled</option>
                        <option value="complete">complete</option>
                    </select>
                </div>
            </div>
            <div class="btn-container">
                <button class="btn btn-sm btn-primary" style="width:100px;">save</button>
            </div>
        </form>
    </div>
    <table class="table">
        <tbody id="pickup_id">
            <tr>
                <th>User</th>
                <th>Date</th>
                <th>Status</th>
                <th>Time</th>
                <th>Note</th>
            </tr>  
            @if($reservation_sechedule)
                @foreach($reservation_sechedule->reservation_schedule_status as $status)
                    <tr>
                        <td>{{$status->driver_name}}</td>
                        <td>{{$status->date}}</td>
                        <td>{{$status->status}}</td>
                        <td>{{$status->time}}</td>
                        <td>{{$status->note}}</td>
                    </tr>
                @endforeach
            @endif
        </tbody>
    </table>
</div>