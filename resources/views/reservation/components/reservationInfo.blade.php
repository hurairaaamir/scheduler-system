
<div class="panel">
    <div class="panel-heading">
        <h4 class="panel-title" id="reservationModalLabel"><img src="/images/general/schedule.svg" style="width:20px" alt=""> Reservation Info</h4>
    </div>
    <div class="panel-body">
        <div class="info-tab">
            <span >Date Booked</span>
            <span class="tab">{{$reservation->date}}</span>
        </div>
        <div class="info-tab">
            <span>package Selected</span>
            <span class="tab">{{$reservation->package->name}}</span>
        </div>
        <div class="info-tab">
            <span>Package Bulky Items</span>
            <span class="tab">{{$reservation->bulky_items}}</span>
        </div>
        @php
            $bulky_items_list=unserialize($reservation->bulky_items_list)
        @endphp
        <div class="info-tab">
            <span>Bulky Items</span>
            <span class="tab">
                @if($bulky_items_list)
                    @foreach($bulky_items_list as $key => $item)
                        {{$item}},
                    @endforeach
                @endif
            </span>
        </div>
        <div class="info-tab">
            <span>Quote Amount</span>
            <span class="tab">{{$reservation->quote_amount}} $</span>
        </div>
        <div class="info-tab">
            <span>Deposit Amount</span>
            <span class="tab">{{$reservation->deposit_amount}} $</span>
        </div>
        <div class="info-tab">
            <span>Note</span>
            <span class="tab">{{$reservation->note}}</span>
        </div>
    </div>
</div>
            
                