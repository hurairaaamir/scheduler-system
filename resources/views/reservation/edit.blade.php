@extends('layouts.master')
@section('title')
    Reservation
@endsection
@section('page-head')
    <div id="page-title">
        <h1 class="page-header text-overflow">Reservation</h1>
    </div>
    <ol class="breadcrumb">
        <li><a href="#"><i class="demo-pli-home"></i></a></li>

        <li class="active">Reservation</li>
    </ol>
@endsection
@section('content')
    <hr class="new-section-sm bord-no">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title">Edit Reservation</h3>
                </div>
                <form method="post" action="{{url('admin/reservation/update' , $driver->id)}}" enctype="multipart/form-data">
                    @csrf
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label" for='name'>Driver Name</label>
                                    <input type="text" name="name" class="form-control" id="name" value="{{$driver->name}}">
                                    @if($errors->has('name'))
                                        <p style="color:red">
                                            {{ $errors->first('name') }}
                                        </p>
                                    @endif
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label" for="email">Driver Email</label>
                                    <input name="email" id="email" class="form-control" type="text" value="{{$driver->email}}">
                                </div>
                                @if($errors->has('office_location'))
                                    <p style="color:red">
                                        {{ $errors->first('office_location') }}
                                    </p>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label" for="password">Password</label>
                                    <input type="password" name="password" class="form-control" id="password" >
                                </div>
                                @if($errors->has('password'))
                                    <p style="color:red">
                                        {{ $errors->first('password') }}
                                    </p>
                                @endif
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label" for="password_confirmation">Password Confirmation</label>
                                    <input type="password" name="password_confirmation" class="form-control" id="password_confirmation" >
                                </div>
                                @if($errors->has('password_confirmation'))
                                    <p style="color:red">
                                        {{ $errors->first('password_confirmation') }}
                                    </p>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label" for="status">Status</label>
                                    <select name="status" class="form-control" id="status" >
                                        <option value="active" {{$driver->status =='active'?? 'selected'}}>active</option>
                                        <option value="inactive" {{$driver->status =='inactive'?? 'selected'}}>inactive</option>
                                    </select>
                                    @if($errors->has('status'))
                                        <p style="color:red">
                                            {{ $errors->first('status') }}
                                        </p>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <button class="btn btn-success" type="submit">Edit Item</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection