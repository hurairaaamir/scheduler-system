@extends('layouts.master')
@section('title')
    Reservation
@endsection
@section('page-head')
    <div id="page-title">
        <h1 class="page-header text-overflow">Reservation</h1>
    </div>
    <ol class="breadcrumb">
        <li><a href="#"><i class="demo-pli-home"></i></a></li>

        <li class="active">Reservation</li>
    </ol>
@endsection
@section('content')
    <hr class="new-section-sm bord-no">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title">All Reservations</h3>
                </div>
                <div class="panel-body">
                    <div class="pad-btm form-inline">
                        <div class="row">
                            <div class="col-sm-6 table-toolbar-left">
                                <a href="{{url('admin/reservation/create')}}" id="demo-btn-addrow" class="btn btn-purple"><i class="demo-pli-add"></i> Add</a>
                            </div>
                        </div>
                    </div>
                    
                    <table id="luggers-table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>Status</th>
                            <th>Reservation #</th>
                            <th>Home Office</th>
                            <th>Customer Name</th>
                            <th>Customer Phone #</th>
                            <th>Customer Email</th>
                            <th class="min-tablet">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($reservations as $reservation)
                            <tr>
                                <td>
                                    <span class="badge {{$reservation->status=='active' ? 'badge-info' : 'badge-danger'}} ">{{$reservation->status}}</span>
                                </td>
                                <td>{{ 'Gpq'.Helper::styleId($reservation->id)}}</td>
                                <td>{{$reservation->reservation_schedule->first()->address}}</td>
                                <td>{{$reservation->user->name}}</td>
                                <td>{{$reservation->user->customer_profile->phone_number??""}}</td>
                                <td>{{$reservation->user->email}}</td>
                                <td>
                                    <a class="btn btn-primary" href="{{url('admin/reservation/show' , $reservation->id)}}">View More</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection