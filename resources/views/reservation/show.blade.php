@extends('layouts.master')
@section('title')
    Reservation
@endsection
@section('style')
<style>
    .panel-card{
        display:flex;
        padding:10px;
        border-radius: 20px;
        cursor: pointer;
        margin:15px 0px;
    }
    .panel-card .child1{
        flex: 1 1 150px;
    }
    .panel-card .child1 img{
        width:80px;
        color:white;
        height: auto;
    }

    .panel-card .child2{
        flex: 1 1 300px;
    }
    .panel-card .child2 h4{
        color:white;
    }
    .center_out{
        display:flex;
        justify-content: center;
        align-content: center;
        flex-direction: column;
        text-align: center;
    }
    .center_out form{
        display: flex;
        justify-content: center;
        align-content: center;
        text-align: center;
        margin-top:10px;
        margin-bottom: 50px;
    }
    .center_out form span{
        margin:6px; 
    }
    .center_out form select{
        width:100px;
        margin:0px 10px;

    }
    .info-tab{
        display: flex;
        justify-content: space-between;
        margin: 5px 50px;
        align-items: center;
        font-size:15px;
        min-width: 365px;
    }
    .info-tab .tab{
        text-align: center;
        min-width:252px;
        min-height: 29px;
        background-color: #f1f1f1;
        border: 1px solid rgb(212, 212, 212);
    }
    .btn-container{
        display:flex;
        margin:0px 73px!important;
        margin-top:27px!important;
    }
    @media(max-width:760px){
        .info-tab{
            
            margin: 5px 0px;
        }
        .btn-container{
            margin:10px 10px!important;
        }
    }
    .info-input{
        min-width: 252px;
        padding: 4px 9px;
        background-color: hsl(0, 0%, 100%);
        border: 1px solid rgb(216, 211, 211);
    }
    .preloader{
        position: fixed;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        width:100%;
        height: 100vh;
        display:none;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        background-color: #000;
        opacity: 0.5;
        z-index: 1000000;
    }
    .off{
        display:none!important;
    }
    .preloader-in{
        display:flex!important;
    }
    .preloader img{
        width:100px;  
        opacity: 1; 
        z-index: 1000001;
    }
    .error{
        border: 1px solid rgba(255, 0, 0, 0.842);
        box-shadow: 0px 0px 8px #fa5e5e !important;
    }
    .panel-body {
        padding: 13px 0px 25px!important;
    }
</style>
@endsection
@section('page-head')
    <div id="page-title">
        <h1 class="page-header text-overflow">Reservation</h1>
    </div>
    <ol class="breadcrumb">
        <li><a href="#"><i class="demo-pli-home"></i></a></li>
        <li class="active">Reservation</li>
    </ol>
@endsection
@section('content')
    <hr class="new-section-sm bord-no">
    <div class="preloader"><img src="/images/preloader/blue-circle.svg" alt=""></div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title">View Reservation</h3>
                </div>
                <div class="panel-body">
                    <div class="row demo-nifty-panel" >
                        <div class="center_out">
                            <h3> Reservation :{{ 'Gpq'.Helper::styleId($reservation->id)}} <span class="badge {{$reservation->status=='active' ? 'badge-info' : 'badge-danger'}} ">{{$reservation->status}}</span></h3>
                            <form action="{{url('admin/reservation/update/status',$reservation->id)}}" method="post">
                                @csrf
                                <span>Reservation Status</span>
                                <select name="status" class="form-control">
                                    <option value="active" {{($reservation->status=='active')?'selected':''}}>Active</option>
                                    <option value="close" {{($reservation->status=='close')?'selected':''}}>Close</option>
                                </select>
                                <button class="btn btn-sm btn-primary">save</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row demo-nifty-panel">
        <div class="col-lg-6">
            <div class="col-md-12">
                @include('reservation.components.reservationInfo')
            </div>
            <div class="col-md-12">
                @include('reservation.components.customerInfo')
            </div>
            <div class="col-md-12">
                @include('reservation.components.paymentInfo')
            </div>
            <div class="col-md-12">
                @include('reservation.components.dropoffSchedule')
            </div>
        </div>
        <div class="col-lg-6">
            <div class="col-md-12">
                @include('reservation.components.pickupSchedule')
            </div>
            <div class="col-md-12">
                @include('reservation.components.deliverySchedule')
            </div>
        </div>
    </div>              
@endsection

@section('script')
@endsection