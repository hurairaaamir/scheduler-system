@extends('layouts.master')
@section('title')
    Rule Type
@endsection
@section('page-head')
    <div id="page-title">
        <h1 class="page-header text-overflow">Rule Type</h1>
    </div>
    <ol class="breadcrumb">
        <li><a href="#"><i class="demo-pli-home"></i></a></li>

        <li class="active">Rule Type</li>
    </ol>
@endsection
@section('content')
    <hr class="new-section-sm bord-no">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title">Edit Rule Type</h3>
                </div>
                <form method="post" action="{{url('admin/rule-type/edit', $rule_type->id)}}" enctype="multipart/form-data">
                    @csrf
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Name</label>
                                    <input type="text" value="{{$rule_type->name}}" name="name" class="form-control" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Type</label>
                                    <select class="form-control" name="type" required>
                                        <option value="">Select Type</option>
                                        <option @if($rule_type->type == 'Delievery') selected @endif value="Delievery">Delievery</option>
                                        <option @if($rule_type->type == 'Pickup') selected @endif value="Pickup">Pickup</option>
                                        <option @if($rule_type->type == 'Dropoff') selected @endif value="Dropoff">Dropoff</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer text-right">
                        <button class="btn btn-success" type="submit">Edit Rule Type</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection