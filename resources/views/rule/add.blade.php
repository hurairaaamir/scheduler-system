@extends('layouts.master')
@section('title')
    Rule
@endsection
@section('page-head')
    <div id="page-title">
        <h1 class="page-header text-overflow">Rule</h1>
    </div>
    <ol class="breadcrumb">
        <li><a href="#"><i class="demo-pli-home"></i></a></li>

        <li class="active">Rule</li>
    </ol>
@endsection
@section('content')
    <hr class="new-section-sm bord-no">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel">
                @if (\Session::has('message'))
                    <div class="alert alert-danger">
                        {!! \Session::get('message') !!}
                    </div>
                @endif
                <div class="panel-heading">
                    <h3 class="panel-title">Add Rule</h3>
                </div>
                <form id="rule-form" method="post" action="{{url('admin/rule/add')}}" enctype="multipart/form-data">
                    @csrf
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Rule Name</label>
                                    <input type="text" name="name" class="form-control">
                                    @if($errors->has('name'))
                                        <p style="color:red">
                                            {{ $errors->first('name') }}
                                        </p>
                                    @endif
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Rule Type</label>
                                    <select class="form-control" name="rule_type">
                                        <option value="">Select Rule Type</option>
                                        <option value="delivery">Delivery</option>
                                        <option value="pickup">Pickup</option>
                                    </select>
                                    @if($errors->has('rule_type'))
                                        <p style="color:red">
                                            {{ $errors->first('rule_type') }}
                                        </p>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Zone</label>
                                    <select id="zone" multiple="multiple" class="form-control"  name="zones[]">
                                        @foreach($zones as $value)
                                            <option value="{{$value->id}}">{{$value->name}}</option>
                                        @endforeach
                                    </select>
                                    @if($errors->has('zones'))
                                        <p style="color:red">
                                            {{ $errors->first('zones') }}
                                        </p>
                                    @endif
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Days</label>
                                    <select id="days" multiple="multiple" class="form-control" name="days[]">
                                        <option value="Monday">Monday</option>
                                        <option value="Tuesday">Tuesday</option>
                                        <option value="Wednesday">Wednesday</option>
                                        <option value="Thursday">Thursday</option>
                                        <option value="Friday">Friday</option>
                                        <option value="Saturday">Saturday</option>
                                        <option value="Sunday">Sunday</option>
                                    </select>
                                    @if($errors->has('days'))
                                        <p style="color:red">
                                            {{ $errors->first('days') }}
                                        </p>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer text-right">
                        <button class="btn btn-success" type="submit">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

    <script type="text/javascript">
        $("#zone").select2();
        $('#rule-form').submit(function (e) {
            var day = $('#days').select2().val();
            $("select[name='days']").val(day);
        });

        $('#rule-form').submit(function (e) {
            var zoneId = $('#zone').select2().val();
            $("select[name='zone_id']").val(zoneId);
        });
    </script>
@endsection
