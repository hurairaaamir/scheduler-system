@extends('layouts.master')
@section('title')
    Rule
@endsection
@section('page-head')
    <div id="page-title">
        <h1 class="page-header text-overflow">Rule</h1>
    </div>
    <ol class="breadcrumb">
        <li><a href="#"><i class="demo-pli-home"></i></a></li>

        <li class="active">Rule</li>
    </ol>
@endsection
@section('content')
    <hr class="new-section-sm bord-no">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel">
                @if (\Session::has('message'))
                    <div class="alert alert-danger">
                        {!! \Session::get('message') !!}
                    </div>
                @endif
                <div class="panel-heading">
                    <h3 class="panel-title">Edit Rule</h3>
                </div>
                <form id="rule-form" method="post" action="{{url('admin/rule/edit',$rule->id)}}" enctype="multipart/form-data">
                    @csrf
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Rule Name</label>
                                    <input type="text" name="name" class="form-control" value="{{$rule->name}}">
                                    @if($errors->has('name'))
                                        <p style="color:red">
                                            {{ $errors->first('name') }}
                                        </p>
                                    @endif
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Rule Type</label>
                                    {{$rule->rule_type=='pickup'}}
                                    <select class="form-control" name="rule_type" >
                                        <option value="" >Select Rule Type</option>
                                        <option value="delivery" {{$rule->rule_type=="delivery"?'selected':''}}>Delivery</option>
                                        <option value="pickup" {{$rule->rule_type=='pickup'?'selected':''}}>PickUp</option>
                                    </select>
                                    @if($errors->has('rule_type'))
                                        <p style="color:red">
                                            {{ $errors->first('rule_type') }}
                                        </p>
                                    @endif
                                </div>
                            </div>
                        </div>
                        @php
                            $selected_ids=[];
                        @endphp
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Zones</label>
                                    <select class="form-control rules_selector" name="zones[]" multiple="multiple" id="zone">
                                        @foreach($rule->zones as $zone)
                                            <option value="{{$zone->id}}" selected>{{$zone->name}}</option>
                                            @php
                                                array_push($selected_ids,$zone->id);   
                                            @endphp
                                        @endforeach
                                        @foreach($zones as $zone)
                                            @if(!in_array($zone->id,$selected_ids))
                                                <option value="{{$zone->id}}">{{$zone->name}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                    @if($errors->has('zone'))
                                        <p style="color:red">
                                            {{ $errors->first('zone') }}
                                        </p>
                                    @endif
                                </div>
                            </div>
                            @php
                                $days=explode(',',$rule->days);
                            @endphp
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Days</label>
                                    <select id="days" multiple="multiple" class="form-control" name="days[]" >
                                        <option value="Monday" {{in_array('Monday',$days)?'selected':''}}>Monday</option>
                                        <option value="Tuesday" {{in_array('Tuesday',$days)?'selected':''}}>Tuesday</option>
                                        <option value="Wednesday" {{in_array('Wednesday',$days)?'selected':''}}>Wednesday</option>
                                        <option value="Thursday" {{in_array('Thursday',$days)?'selected':''}}>Thursday</option>
                                        <option value="Friday" {{in_array('Friday',$days)?'selected':''}}>Friday</option>
                                        <option value="Saturday" {{in_array('Saturday',$days)?'selected':''}}>Saturday</option>
                                        <option value="Sunday" {{in_array('Sunday',$days)?'selected':''}}>Sunday</option>
                                    </select>
                                    @if($errors->has('days'))
                                        <p style="color:red">
                                            {{ $errors->first('days') }}
                                        </p>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer text-right">
                        <button class="btn btn-success" type="submit">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

    <script type="text/javascript">
        $("#zone").select2();
        $('#rule-form').submit(function (e) {
            var day = $('#days').select2().val();
            $("select[name='days']").val(day);
        });

        $('#rule-form').submit(function (e) {
            var zoneId = $('#zone').select2().val();
            $("select[name='zone_id']").val(zoneId);
        });

    </script>
@endsection
