@extends('layouts.master')
@section('title')
    Schedule
@endsection
@section('page-head')
    <div id="page-title">
        <h1 class="page-header text-overflow">Schedule</h1>
    </div>
    <ol class="breadcrumb">
        <li><a href="#"><i class="demo-pli-home"></i></a></li>

        <li class="active">Schedule</li>
    </ol>
@endsection
@section('content')
    <hr class="new-section-sm bord-no">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title">Assign Driver</h3>
                </div>
                <div class="center_out">
                    <h2>Assign Driver</h2>
                    <div style="display:flex">
                        <h5 style="margin:5px;">Unit # {{Helper::styleId($schedule->id)}}</h5>
                        <h5 style="margin:5px;">{{$schedule->unit_truck->name}}</h5>
                    </div>
                    <div>
                        <h5>{{$schedule->date}}</h5>
                    </div>
                    <div style="display:flex;">
                        <h5 style="margin:5px;">Start Time: {{$schedule->start_time}}</h5>
                        <h5 style="margin:5px;">End Time: {{$schedule->end_time}}</h5>
                    </div>
                    
                </div>
                <form method="post" action="{{url('admin/schedule/assignDriver')}}" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="schedule_id" value="{{$schedule->id}}">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Drviers</label>
                                    <select class="form-control drivers_selector" name="users[]" multiple="multiple">
                                        @foreach($drivers as $user)
                                            <option value="{{$user->id}}">{{$user->name}}</option>
                                        @endforeach
                                    </select>
                                    @if($errors->has('users'))
                                        <p style="color:red">
                                            {{ $errors->first('users') }}
                                        </p>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <button class="btn btn-success" type="submit">Upload</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('scripts')

    <script type="text/javascript">
        
        $(document).ready(function() {
            $('.drivers_selector').select2({
                placeholder: 'Select Rules',
            });
        });

    </script>
@endsection