@extends('layouts.master')
@section('title')
    Schedule
@endsection
@section('page-head')
    <div id="page-title">
        <h1 class="page-header text-overflow">Schedule</h1>
    </div>
    <ol class="breadcrumb">
        <li><a href="#"><i class="demo-pli-home"></i></a></li>

        <li class="active">Schedule</li>
    </ol>
@endsection
@section('content')
    <hr class="new-section-sm bord-no">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title">Edit Schedule</h3>
                </div>
                <form method="post" action="{{url('admin/schedule/csv-upload')}}" enctype="multipart/form-data">
                    @csrf
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label" for='csv'>upload Csv</label>
                                    <input type="file" name="csv" class="form-control" id="csv" >
                                    @if($errors->has('name'))
                                        <p style="color:red">
                                            {{ $errors->first('csv') }}
                                        </p>
                                    @endif
                                </div>
                            </div>
                        </div>
                        

                    </div>
                    <div class="panel-footer">
                        <button class="btn btn-success" type="submit">Upload</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection