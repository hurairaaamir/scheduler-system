@extends('layouts.master')
@section('title')
    Schedule
@endsection
@section('page-head')
    <div id="page-title">
        <h1 class="page-header text-overflow">Schedule</h1>
    </div>
    <ol class="breadcrumb">
        <li><a href="#"><i class="demo-pli-home"></i></a></li>

        <li class="active">Schedule</li>
    </ol>
@endsection
@section('content')
    <hr class="new-section-sm bord-no">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title">Add Schedule</h3>
                </div>
                <form method="post" action="{{url('admin/schedule/update',$schedule->id)}}" enctype="multipart/form-data">
                    @csrf
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label" for='name'>Select Units</label>
                                    <select name="unit[]" class="js-example-basic-multiple form-control" multiple="multiple">
                                        @foreach($data as $value)
                                            @if($schedule->unit_truck->id==$value->id)
                                                <option value="{{$value->id}}" selected>{{$value->name}}</option>  
                                            @else
                                                <option value="{{$value->id}}">{{$value->name}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                    @if($errors->has('unit'))
                                        <p style="color:red">
                                            {{ $errors->first('starting_service_zone') }}
                                        </p>
                                    @endif
                                </div>
                            </div>
                            @php
                                $date=explode('/',$schedule->date);
                                $date=$date[1].'/'.$date[0].'/'.$date[2];
                            @endphp
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label"></label>
                                    <input type="text" value="{{$date}}" name="date" class="form-control" style="width:100%"/>
                                </div>
                                @if($errors->has('date'))
                                    <p style="color:red">
                                        {{ $errors->first('date') }}
                                    </p>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label" for='name'>Start Time</label>
                                    <input type="text"  name="start_time" class="form-control" placeholder="ex. 10AM" value="{{$schedule->start_time}}"/>
                                    @if($errors->has('start_time'))
                                        <p style="color:red">
                                            {{ $errors->first('start_time') }}
                                        </p>
                                    @endif
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label" for='name'>End Time</label>
                                    <input type="text"  name="end_time" class="form-control" placeholder="ex. 12AM" value="{{$schedule->end_time}}"/>
                                    @if($errors->has('end_time'))
                                        <p style="color:red">
                                            {{ $errors->first('end_time') }}
                                        </p>
                                    @endif
                                </div>
                            </div>
                        </div>
                        @php
                            $driver_ids=[];
                        @endphp
                        <div class="row">
                            <div class="col-sm-6">
                                <label class="control-label" for='name'>Drivers</label>
                                <select class="form-control rules_selector" name="users[]" multiple="multiple">
                                    @foreach($schedule->users as $driver)
                                        <option value="{{$driver->id}}" selected>{{$driver->name}}</option>
                                        @php
                                            array_push($driver_ids,$driver->id);   
                                        @endphp
                                    @endforeach
                                    @foreach($users as $user)
                                            @if(!in_array($user->id,$driver_ids))
                                                <option value="{{$user->id}}">{{$user->name}}</option>
                                            @endif
                                    @endforeach
                                </select>
                                @if($errors->has('user'))
                                    <p style="color:red">
                                        {{ $errors->first('user') }}
                                    </p>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <button class="btn btn-success" type="submit">Edit Item</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('scripts')

<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<script>
    $(function() {
      $('input[name="date"]').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        minYear: 2020,
      });
    });
</script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.16/dist/summernote-bs4.min.js"></script>
<script>
    $(document).ready(function() {
        $('.js-example-basic-multiple').select2({
            placeholder: 'Select Units',
        });
        $('.rules_selector').select2({
                placeholder: 'Select Rules',
            });
    });
</script>
@endsection