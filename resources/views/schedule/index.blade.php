@extends('layouts.master')
@section('title')
    Schedule
@endsection
@section('page-head')
    <div id="page-title">
        <h1 class="page-header text-overflow">Schedule</h1>
    </div>
    <ol class="breadcrumb">
        <li><a href="#"><i class="demo-pli-home"></i></a></li>

        <li class="active">Schedule</li>
    </ol>
@endsection
@section('content')
    <hr class="new-section-sm bord-no">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title">All Schedule</h3>
                </div>
                <div class="panel-body">
                    <div class="pad-btm form-inline">
                        <div class="row">
                            <div class="table-toolbar-left">
                                <a href="{{url('admin/schedule/create')}}" id="demo-btn-addrow" class="btn btn-purple"><i class="demo-pli-add"></i> Add</a>
                            </div>
                            <div class="table-toolbar-left" style="margin-left:20px;">
                                <a href="{{url('admin/schedule/csv')}}" id="demo-btn-addrow" class="btn btn-primary"><i class="fa fa-upload"></i> upload CSV</a>
                            </div>
                        </div>
                    </div>
                    <table id="luggers-table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>Unit #</th>
                            <th>Unit Name</th>
                            <th>Start Time</th>
                            <th>End Time</th>
                            <th>Date</th>
                            <th>Drivers</th>
                            <th class="min-tablet">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($data as $value)
                            <tr>
                                <td>{{Helper::styleId($value->unit_truck_id)}}</td>
                                <td>{{$value->unit_truck->name}}</td>
                                <td>{{$value->start_time}}</td>
                                <td>{{$value->end_time}}</td>
                                <td>{{$value->date}}</td>
                                <td>
                                    @foreach($value->users as $user)
                                        {{$user->name}},
                                    @endforeach
                                </td>

                                <td>
                                    <a class="btn btn-warning" href="{{url('admin/schedule/assignDriver/'.$value->id.'/'.$value->unit_truck_id)}}"> <i class="fa fa-user"></i> Assign Driver</a>
                                    <a class="btn btn-primary" href="{{url('admin/schedule/edit' , $value->id)}}"><i class="fa fa-edit"></i></a>
                                    <form method="post" action="{{url('admin/schedule/destroy', $value->id)}}" style="display: inline;">
                                        @csrf
                                        <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i></button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection