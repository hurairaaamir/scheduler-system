@extends('layouts.master')
@section('title')
    Trucks
@endsection
@section('page-head')
    <div id="page-title">
        <h1 class="page-header text-overflow">Trucks</h1>
    </div>
    <ol class="breadcrumb">
        <li><a href="#"><i class="demo-pli-home"></i></a></li>

        <li class="active">Trucks</li>
    </ol>
@endsection
@section('content')
    <hr class="new-section-sm bord-no">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title">Edit Trucks</h3>
                </div>
                <form method="post" action="{{url('admin/trucks/edit' , $truck->id)}}" enctype="multipart/form-data">
                    @csrf
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Name</label>
                                    <input value="{{$truck->name}}" type="text" name="name" class="form-control">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Luggers</label>
                                    <input value="{{$truck->luggers}}" type="number" name="luggers" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Per K/M Price</label>
                                    <input value="{{$truck->per_kilomoeter_price}}" type="number" name="per_kilomoeter_price" class="form-control">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Icon</label>
                                    <input type="file" name="icon" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label class="control-label">Description</label>
                                <textarea class="form-control" name="description">{{$truck->description}}</textarea>
                            </div>
                            <div class="col-md-6">
                                <img src="{{$truck->icon}}" style="width: 30%">
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer text-right">
                        <button class="btn btn-success" type="submit">Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection