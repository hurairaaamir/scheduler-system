@extends('layouts.master')
@section('title')
Units
@endsection
@section('page-head')
    <div id="page-title">
        <h1 class="page-header text-overflow">Units</h1>
    </div>
    <ol class="breadcrumb">
        <li><a href="#"><i class="demo-pli-home"></i></a></li>

        <li class="active">Units</li>
    </ol>
@endsection
@section('content')
    <hr class="new-section-sm bord-no">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title">Add Units</h3>
                </div>
                <form method="post" action="{{url('admin/unit-truck/store')}}" enctype="multipart/form-data">
                    @csrf
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label" for='name'>Unit Name</label>
                                    <input type="text" name="name" class="form-control" id="name">
                                    @if($errors->has('name'))
                                        <p style="color:red">
                                            {{ $errors->first('name') }}
                                        </p>
                                    @endif
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label" for="office_location">Office Location</label>
                                    <select name="office_location" id="office_location" class="form-control">
                                        @foreach($locations as $location)
                                            <option value="{{$location->id}}">{{$location->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                @if($errors->has('office_location'))
                                    <p style="color:red">
                                        {{ $errors->first('office_location') }}
                                    </p>
                                @endif
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Service Type</label>
                                    <br>
                                    <input type="checkbox" name="service_type_delivery" value="delivery"><label style="margin:10px">Delivery</label>
                                    <input type="checkbox" name="service_type_pickUP"  value="pickUp"><label style="margin:10px">PickUP</label>
                                </div>
                                @if($errors->has('service_type'))
                                    <p style="color:red">
                                        {{ $errors->first('service_type') }}
                                    </p>
                                @endif
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label" for="max_bulky_items">Max Bulky Items</label>
                                    <input type="number" min="1" name="max_bulky_items" class="form-control" id="max_bulky_items" >
                                </div>
                                @if($errors->has('max_bulky_items'))
                                    <p style="color:red">
                                        {{ $errors->first('max_bulky_items') }}
                                    </p>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <button class="btn btn-success" type="submit">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection