@extends('layouts.master')
@section('title')
    Units
@endsection
@section('page-head')
    <div id="page-title">
        <h1 class="page-header text-overflow">Units</h1>
    </div>
    <ol class="breadcrumb">
        <li><a href="#"><i class="demo-pli-home"></i></a></li>

        <li class="active">Units</li>
    </ol>
@endsection
@section('content')
    <hr class="new-section-sm bord-no">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title">All Units</h3>
                </div>
                <div class="panel-body">
                    <div class="pad-btm form-inline">
                        <div class="row">
                            <div class="col-sm-6 table-toolbar-left">
                                <a href="{{url('admin/unit-truck/create')}}" id="demo-btn-addrow" class="btn btn-purple"><i class="demo-pli-add"></i> Add</a>
                            </div>
                        </div>
                    </div>
                    
                    <table id="luggers-table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>Unit #</th>
                            <th>Name</th>
                            <th>Office Location</th>
                            <th>Max Bulk Items</th>
                            <th>Service Type</th>
                            <th class="min-tablet">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($data as $value)
                            <tr>
                                <td>{{ Helper::styleId($value->id)}}</td>
                                <td>{{$value->name}}</td>
                                <td>{{$value->office_location->address}}</td>
                                <td>{{$value->max_bulky_items}}</td>
                                <td>
                                    @if($value->service_type_delivery)
                                        <span class="badge">{{$value->service_type_delivery}}</span> 
                                    @endif
                                    @if($value->service_type_pickUP)
                                        <span class="badge badge-info">{{$value->service_type_pickUP}}</span>
                                    @endif
                                </td>

                                <td>
                                    {{-- <a href="" class="btn btn-warning" href="{{url('admin/unit-truck/assignDriver' $value->id)}}"> <i class="fa fa-user"></i> Assign Driver</a> --}}
                                    
                                    <a class="btn btn-primary" href="{{url('admin/unit-truck/edit' , $value->id)}}"><i class="fa fa-edit"></i></a>
                                    
                                    <form method="post" action="{{url('admin/unit-truck/destroy', $value->id)}}" style="display: inline;">
                                        @csrf
                                        <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i></button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection