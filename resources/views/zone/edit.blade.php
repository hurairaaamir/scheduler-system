@extends('layouts.master')
@section('title')
    Zone
@endsection
@section('page-head')
    <div id="page-title">
        <h1 class="page-header text-overflow">Zone</h1>
    </div>
    <ol class="breadcrumb">
        <li><a href="#"><i class="demo-pli-home"></i></a></li>

        <li class="active">Zone</li>
    </ol>
@endsection
@section('content')
    <hr class="new-section-sm bord-no">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel">
                @if (\Session::has('message'))
                    <div class="alert alert-danger">
                        {!! \Session::get('message') !!}
                    </div>
                @endif
                <div class="panel-heading">
                    <h3 class="panel-title">Edit Zone</h3>

                </div>
                <form method="post" action="{{url('admin/zone/edit', $zone->id)}}" enctype="multipart/form-data">
                    @csrf
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Name</label>
                                    <input type="text" value="{{$zone->name}}" name="name" class="form-control" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Zip Codes</label>
                                    <input type="text" name="zip_codes" class="form-control" value="{{$zone_zipcode}}" placeholder="Type to add a Zone" data-role="tagsinput">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer text-right">
                        <button class="btn btn-success" type="submit">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection