@extends('layouts.master')
@section('title')
    Zones
@endsection
@section('page-head')
    <div id="page-title">
        <h1 class="page-header text-overflow">Zones</h1>
    </div>
    <ol class="breadcrumb">
        <li><a href="#"><i class="demo-pli-home"></i></a></li>

        <li class="active">Zones</li>
    </ol>
@endsection
@section('content')
    <hr class="new-section-sm bord-no">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title">All Zones</h3>
                </div>
                <div class="panel-body">
                    <div class="pad-btm form-inline">
                        <div class="row">
                            <div class="col-sm-6 table-toolbar-left">
                                <a href="{{url('admin/zone/add')}}" id="demo-btn-addrow" class="btn btn-purple"><i class="demo-pli-add"></i> Add</a>
                            </div>
                        </div>
                    </div>
                    <table id="luggers-table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Zip Code</th>
                            <th class="min-tablet">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($zones as $value)
                            <tr>
                                <td>{{$value->name}}</td>
                                <td>{{implode(',' , $value->zip_code)}}</td>
                                <td>
                                    <a class="btn btn-primary" href="{{url('admin/zone/edit' , $value->id)}}"><i class="fa fa-edit"></i></a>
                                    <form method="post" action="{{url('admin/zone/delete', $value->id)}}" style="display: inline;">
                                        @csrf

                                        <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i></button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection