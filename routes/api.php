<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group([ 'prefix' => 'auth'], function (){
    Route::group(['middleware' => ['guest:api']], function () {
        // Route::post('login', 'API\AuthController@login');
        // Route::post('signup', 'API\AuthController@signup');
        // ROute::post('request-password' , 'API\AuthController@requestPassword');
        // ROute::post('reset-password' , 'API\AuthController@resetPassword');

        // Route::post('register-user' , 'API\AuthController@registerUser');
        // Route::post('verify-code' , 'API\AuthController@verifyCode');
    });
    Route::group(['middleware' => 'auth:api'], function() {
        Route::get('logout', 'API\AuthController@logout');
        Route::get('getuser', 'API\AuthController@getUser');
        Route::post('/updateProfile/{id}', 'API\AuthenController@updateProfile');
        Route::post('/updateUser/{id}', 'API\AuthenController@updateUser');
    });




});

Route::middleware(['auth:api'])->group(function(){
    Route::post('/auth/user','API\AuthenController@authUser');
    Route::post('/logout', 'API\AuthenController@logout');

    Route::group([ 'prefix' => 'customer_profile'], function (){
        Route::post('/store','API\AuthenController@storeCustomerProfile');
    });
    Route::group([ 'prefix' => 'reservation'], function (){
        Route::post('/index','API\ReservationController@index');
        Route::post('/getReservation','API\ReservationController@getReservation');
        Route::post('/change-status','API\ReservationController@changeStatus');
        Route::post('/store','API\ReservationController@storeReservation');
        Route::post('/customerReservations','API\ReservationController@customerReservations');
        Route::post('/customerReservation','API\ReservationController@customerReservation');
        Route::post('/payments','API\ReservationController@fetchPayments');
        Route::post('/update','API\ReservationController@update');
    });
});
Route::post('/login', 'API\AuthenController@login')->name('login');
Route::post('/register', 'API\AuthenController@register');

// Route::post('/zone',"API\ZoneController@send");


Route::get('get-categories' , 'API\CategoriesController@get_categories');
Route::get('get-trucks' , 'API\TruckController@get_trucks');
Route::get('get-packages' , 'API\PackageController@get_packages');
Route::get('get-package-by-id' , 'API\PackageController@get_package_by_id');
Route::get('get-zone' , 'API\ZoneController@get_zone');
Route::post('book'  , 'API\BookingController@reserve_booking');


Route::group([ 'prefix' => 'reservation'], function (){
    Route::post('/getTimeAndDistance','API\ReservationController@getTimeAndDistance');
    Route::post('/getCategories','API\ReservationController@getCategories');

    
});
Route::group([ 'prefix' => 'zone'], function (){
    Route::post('/confirmZipCode','API\ZoneController@confirmZipCode');
});
Route::group([ 'prefix' => 'package'], function (){
    Route::post('/fetch','API\PackageController@fetchPackages');
    Route::get('/get/{id}','API\PackageController@show');
});
Route::group(['prefix'=>"rules"],function (){
    Route::get('/index/{package_id}/{type}',"API\PackageController@fetchRules");
});

Route::group([ 'prefix' => 'units'], function (){
    Route::post('/fetchTimeBlock','API\UnitTruckController@fetchTimeBlock');
    Route::post('/checkTimeBlockAvalibility','API\UnitTruckController@checkTimeBlockAvalibility');
});

