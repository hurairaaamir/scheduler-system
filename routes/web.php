<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();
Route::get('/',function(){
    return view('app');
});

Route::group(['prefix' => 'admin', 'middleware' => ['auth']], function () {
    Route::get('/' , 'Admin\DashboardController@index');


    Route::prefix('customers')->group(function () {
        Route::get('' , 'Admin\CustomerController@index');
        Route::post('impersonateAsCustomer','Admin\CustomerController@impersonateAsCustomer');
    });


    Route::prefix('categories')->group(function () {
        Route::get('' , 'Admin\CategoriesController@index');
        Route::get('add' , 'Admin\CategoriesController@add');
        Route::post('add' , 'Admin\CategoriesController@store');
        Route::get('edit/{id}' , 'Admin\CategoriesController@edit');
        Route::post('edit/{id}' , 'Admin\CategoriesController@update');
        Route::post('delete/{id}' , 'Admin\CategoriesController@delete');
    });

    Route::prefix('trucks')->group(function () {
        Route::get('' , 'Admin\TruckController@index');
        Route::get('add' , 'Admin\TruckController@add');
        Route::post('add' , 'Admin\TruckController@store');
        Route::get('edit/{id}' , 'Admin\TruckController@edit');
        Route::post('edit/{id}' , 'Admin\TruckController@update');
        Route::get('delete/{id}' , 'Admin\TruckController@delete');

    });

    Route::prefix('zone')->group(function () {
        Route::get('' , 'Admin\ZoneController@index');
        Route::get('add' , 'Admin\ZoneController@add');
        Route::post('add' , 'Admin\ZoneController@store');
        Route::get('edit/{id}' , 'Admin\ZoneController@edit');
        Route::post('edit/{id}' , 'Admin\ZoneController@update');
        Route::post('delete/{id}' , 'Admin\ZoneController@delete');

    });

    Route::prefix('rule-type')->group(function () {
        Route::get('' , 'Admin\RuleTypeController@index');
        Route::get('add' , 'Admin\RuleTypeController@add');
        Route::post('add' , 'Admin\RuleTypeController@store');
        Route::get('edit/{id}' , 'Admin\RuleTypeController@edit');
        Route::post('edit/{id}' , 'Admin\RuleTypeController@update');
        Route::post('delete/{id}' , 'Admin\RuleTypeController@delete');

    });

    Route::prefix('rule')->group(function () {
        Route::get('' , 'Admin\RuleController@index');
        Route::get('add' , 'Admin\RuleController@add');
        Route::post('add' , 'Admin\RuleController@store');
        Route::get('edit/{id}' , 'Admin\RuleController@edit');
        Route::post('edit/{id}' , 'Admin\RuleController@update');
        Route::post('delete/{id}' , 'Admin\RuleController@delete');

    });

    Route::prefix('package')->group(function () {
        Route::get('' , 'Admin\PackageController@index');
        Route::get('add' , 'Admin\PackageController@add');
        Route::post('add' , 'Admin\PackageController@store');
        Route::get('edit/{id}' , 'Admin\PackageController@edit');
        Route::post('edit/{id}' , 'Admin\PackageController@update');
        Route::post('delete/{id}' , 'Admin\PackageController@delete');

    });

    Route::prefix('item')->group(function () {
        Route::get('' , 'Admin\ItemController@index');
        Route::get('add' , 'Admin\ItemController@add');
        Route::post('add' , 'Admin\ItemController@store');
        Route::get('edit/{id}' , 'Admin\ItemController@edit');
        Route::post('edit/{id}' , 'Admin\ItemController@update');
        Route::post('delete/{id}' , 'Admin\ItemController@delete');

    });

    Route::prefix('office-location')->group(function () {
        Route::get('' , 'Admin\OfficeLocationController@index');
        Route::get('create' , 'Admin\OfficeLocationController@create');
        Route::post('store' , 'Admin\OfficeLocationController@store');
        Route::get('show/{id}' , 'Admin\OfficeLocationController@show');
        Route::get('edit/{id}' , 'Admin\OfficeLocationController@edit');
        Route::post('update/{id}' , 'Admin\OfficeLocationController@update');
        Route::post('destroy/{id}' , 'Admin\OfficeLocationController@destroy');
    });

    Route::prefix('unit-truck')->group(function () {
        Route::get('' , 'Admin\UnitTruckController@index');
        Route::get('create' , 'Admin\UnitTruckController@create');
        Route::post('store' , 'Admin\UnitTruckController@store');
        Route::get('show/{id}' , 'Admin\UnitTruckController@show');
        Route::get('edit/{id}' , 'Admin\UnitTruckController@edit');
        Route::post('update/{id}' , 'Admin\UnitTruckController@update');
        Route::post('destroy/{id}' , 'Admin\UnitTruckController@destroy');
    });

    Route::prefix('schedule')->group(function () {
        Route::get('' , 'Admin\ScheduleController@index');
        Route::get('create' , 'Admin\ScheduleController@create');
        Route::post('store' , 'Admin\ScheduleController@store');
        Route::get('show/{id}' , 'Admin\ScheduleController@show');
        Route::get('edit/{id}' , 'Admin\ScheduleController@edit');
        Route::post('update/{id}' , 'Admin\ScheduleController@update');
        Route::post('destroy/{id}' , 'Admin\ScheduleController@destroy');
        Route::post('csv-upload','Admin\ScheduleController@csvUpload');
        Route::get('csv','Admin\ScheduleController@csv');
        Route::get('assignDriver/{schedule_id}/{unit_truck_id}' , 'Admin\ScheduleController@assignDriverView');
        Route::post('assignDriver' , 'Admin\ScheduleController@assignDriver');
    });

    Route::prefix('drivers')->group(function () {
        Route::get('' , 'Admin\DriverController@index');
        Route::get('create' , 'Admin\DriverController@create');
        Route::post('store' , 'Admin\DriverController@store');
        Route::get('show/{id}' , 'Admin\DriverController@show');
        Route::get('edit/{id}' , 'Admin\DriverController@edit');
        Route::post('update/{id}' , 'Admin\DriverController@update');
        Route::post('destroy/{id}' , 'Admin\DriverController@destroy');
    });

    Route::prefix('reservation')->group(function () {
        Route::get('' , 'Admin\ReservationController@index');
        Route::get('create' , 'Admin\ReservationController@create');
        Route::post('store' , 'Admin\ReservationController@store');
        Route::post('update/status/{id}','Admin\ReservationController@updateStatus');
        Route::get('show/{id}' , 'Admin\ReservationController@show');
        Route::get('edit/{id}' , 'Admin\ReservationController@edit');
        Route::post('update/{id}' , 'Admin\ReservationController@update');
        Route::post('destroy/{id}' , 'Admin\ReservationController@destroy');
        Route::post('dropoff-schedule/store' , 'Admin\ReservationController@dropoffSchedule');
        Route::post('customer/update','Admin\ReservationController@customerUpdate');
    });
});
Route::get('/test','Admin\ReservationController@test');
